@echo off

rem convert path to backslash format
set MQXROOTDIR=%1
set MQXROOTDIR=%MQXROOTDIR:/=\%
set OUTPUTDIR=%2
set OUTPUTDIR=%OUTPUTDIR:/=\%
set TOOL=%3


rem copy common files
IF NOT EXIST %OUTPUTDIR% mkdir %OUTPUTDIR%
copy %MQXROOTDIR%\adk-stack\ADK.h %OUTPUTDIR%\ADK.h /Y
copy %MQXROOTDIR%\adk-stack\HCI.h %OUTPUTDIR%\HCI.h /Y
copy %MQXROOTDIR%\adk-stack\btL2CAP.h %OUTPUTDIR%\btL2CAP.h /Y
copy %MQXROOTDIR%\adk-stack\btSDP.h %OUTPUTDIR%\btSDP.h /Y
copy %MQXROOTDIR%\adk-stack\btRFCOMM.h %OUTPUTDIR%\btRFCOMM.h /Y
copy %MQXROOTDIR%\adk-stack\adapter\fwk.h %OUTPUTDIR%\fwk.h /Y
copy %MQXROOTDIR%\adk-stack\sgBuf.h %OUTPUTDIR%\sgBuf.h /Y
copy %MQXROOTDIR%\adk-stack\BT.h %OUTPUTDIR%\BT.h /Y
copy %MQXROOTDIR%\adk-stack\dbg.h %OUTPUTDIR%\dbg.h /Y
rem copy %MQXROOTDIR%\adk-stack\adapter\os_utils.h %OUTPUTDIR%\os_utils.h /Y
copy %MQXROOTDIR%\adk-stack\transport\hci_transport.h %OUTPUTDIR%\hci_transport.h /Y

goto tool_%TOOL%

rem cw10 files
:tool_cw10
goto copy_end

rem iar files
:tool_iar
goto copy_end

rem cw10gcc files
:tool_cw10gcc
goto copy_end

rem uv4 files
:tool_uv4
goto copy_end

:copy_end

