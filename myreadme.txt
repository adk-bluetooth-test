for K70f120m
1.adk bluetooth test app :
   examples\build\cw10gcc\bt_app_twrk70f120m\.project 

2. adk bluetooth stack lib:
   build\cw10gcc\adkbt_twrk70f120m\.project

3. mqx usb host lib for bt usb transport:
   adk-stack\transport\usb\usb_host\host\build\cw10gcc\usbh_twrk70f120m\.project


for K60d100m:
1.adk bluetooth test app :
   examples\build\cw10gcc\bt_app_twrk60d100m\.project 

2. adk bluetooth stack lib:
   build\cw10gcc\adkbt_twrk60d100m\.project

3. mqx usb host lib for bt usb transport:
   adk-stack\transport\usb\usb_host\host\build\cw10gcc\usbh_twrk60d100m\.project

K60 project pcm output to dac

build with : cw10.5
MQX :        4.01