/**HEADER*******************************************************************
* 
* Copyright (c) 2008 Freescale Semiconductor;
* All Rights Reserved
*
* Copyright (c) 1989-2008 ARC International;
* All Rights Reserved
*
**************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************
*
* Comments:
*
*   This file contains main initialization for your application
*   and infinite loop
*
*END************************************************************************/

#include "main.h"
#if MQX_KERNEL_LOGGING
#include <klog.h>
#endif
#if MQX_USE_LOGS
#include <log.h>
#endif

#define BT_ADK2012_TEST    // run BT on the adk2012 apk

extern _mqx_uint _lwmsgq_deinit(pointer location);
extern _mqx_int I2C_Init(void);
void USB_task(uint_32 param);

TASK_TEMPLATE_STRUCT MQX_template_list[] =
{
/*  Task number, Entry point, Stack,                 Pri,   String, Auto? */
   {USB_TASK,    USB_task,    USB_TASK_STACK_SIZE,     10 ,   "USB",  MQX_AUTO_START_TASK}, /* this priority must same as BT stack coop task */
   {0,           0,           0,     0,   0,      0,                 }
};



// #include "main.h"
#include <lwmsgq.h>

#define ADK_INTERNAL

/* #include "fwk.h" */
#include "ADK.h"
#include "HCI.h"
#include "fio.h"
#include "msi.h"
//#include "btRFCOMM.h"


//#include "os_utils.h"
typedef int BOOL;
#include "hci_transport.h"

#define dbgPrintf  printf
//android app needs to match this
#define BT_ADK_UUID	0x1d, 0xd3, 0x50, 0x50, 0xa4, 0x37, 0x11, 0xe1, 0xb3, 0xdd, 0x08, 0x00, 0x20, 0x0c, 0x9a, 0x66

#ifdef BT_ADK2012_TEST
// adk-2012
/*
static const uint8_t gzippedLicences[] = {
	0x1F, 0x8B, 0x08, 0x00, 0x36, 0xB6, 0xDF, 0x4F, 0x02, 0x03, 0xCD, 0x58, 0x5D, 0x73, 0xDA, 0x38, 0x14, 0x7D, 0xD7, 0xAF, 0xB8, 0x93, 0x97, 0x26, 0x19, 0x07, 0xB2, 0x79, 0xDA, 0x49, 0x9F, 0x0C,
	0x18, 0xD0, 0x2C, 0xB1, 0x59, 0xDB, 0x24, 0xE5, 0xAD, 0xC2, 0x16, 0xE0, 0x1D, 0x63, 0x79, 0x2D, 0x93, 0x34, 0xFF, 0x7E, 0xEF, 0x95, 0x6D, 0x3E, 0x12, 0x92, 0xA6, 0x09, 0xDD, 0x96, 0xE9, 0x34,
	0x06, 0x49, 0x57, 0xE7, 0x9E, 0xFB, 0x71, 0x2C, 0x31, 0x38, 0xC6, 0x87, 0xFD, 0x02, 0x2B, 0x2C, 0x5C, 0x26, 0x1A, 0xF2, 0x42, 0xC5, 0xEB, 0xA8, 0x84, 0x24, 0x8B, 0xD2, 0x75, 0x2C, 0x35, 0x68,
	0x35, 0x2F, 0x1F, 0x44, 0x21, 0x61, 0x5E, 0xA8, 0x15, 0x74, 0x97, 0xC2, 0xBD, 0x66, 0xE7, 0x17, 0xC7, 0xFB, 0xB4, 0x59, 0x1B, 0xA0, 0x2F, 0xCA, 0xBE, 0x86, 0x0B, 0xE8, 0xDB, 0x21, 0xCC, 0x93,
	0x54, 0x82, 0x7E, 0xD4, 0xA5, 0x5C, 0xC1, 0x0A, 0xC1, 0xE0, 0xB7, 0x1A, 0x4C, 0x35, 0x04, 0xFE, 0x65, 0xEB, 0xF2, 0xCF, 0x19, 0x41, 0x3E, 0xED, 0x9E, 0x11, 0x20, 0x0B, 0xAE, 0x2E, 0xFF, 0xF8,
	0x83, 0xB5, 0x2F, 0x8E, 0x0C, 0xAB, 0x42, 0xD5, 0x60, 0xD0, 0x20, 0x60, 0x21, 0x33, 0x59, 0x24, 0xD1, 0x4B, 0x38, 0xE7, 0xAA, 0x00, 0xBD, 0x12, 0x69, 0x0A, 0x72, 0x35, 0x93, 0x71, 0x2C, 0xE3,
	0x7A, 0x82, 0x6E, 0xA1, 0x39, 0xC3, 0xAF, 0x31, 0x33, 0x2F, 0xA4, 0xDC, 0x12, 0x5B, 0x2E, 0x45, 0x09, 0x2A, 0x47, 0xD3, 0xB1, 0xB1, 0x20, 0x31, 0x00, 0xA2, 0x4C, 0x54, 0x66, 0x41, 0x21, 0xB5,
	0x14, 0x45, 0xB4, 0x04, 0x91, 0xC5, 0x10, 0xA9, 0xD5, 0x4A, 0x16, 0x51, 0x22, 0x52, 0x34, 0x16, 0xCB, 0x7B, 0x99, 0xAA, 0x7C, 0x25, 0xB3, 0x52, 0xC3, 0x3A, 0x8B, 0x65, 0x01, 0x69, 0x12, 0xC9,
	0x4C, 0x4B, 0xC8, 0x15, 0x3E, 0x3D, 0x82, 0x9A, 0xA3, 0xB5, 0x34, 0x55, 0x0F, 0x49, 0xB6, 0x80, 0xB2, 0xA8, 0x40, 0x10, 0xD9, 0x5D, 0x95, 0x3F, 0x16, 0xC9, 0x62, 0x59, 0x12, 0x7F, 0x86, 0x3A,
	0x0B, 0x2A, 0x1A, 0x09, 0x79, 0x35, 0x42, 0x1B, 0x17, 0xF7, 0x32, 0xAE, 0x96, 0x9C, 0x23, 0x76, 0x79, 0x80, 0x8E, 0x7D, 0x3F, 0x08, 0x63, 0xB9, 0x94, 0x85, 0x19, 0x74, 0x3D, 0xB8, 0xB3, 0x7D,
	0xDF, 0x76, 0xC3, 0x69, 0xCB, 0x58, 0x70, 0x15, 0x19, 0x2D, 0x91, 0x3D, 0x72, 0x0D, 0xF0, 0xDF, 0x5A, 0xCB, 0x16, 0x4C, 0xD5, 0x1A, 0x22, 0x61, 0xBE, 0x58, 0x64, 0x3B, 0x99, 0x3F, 0x1A, 0x43,
	0x85, 0x8C, 0x13, 0x9A, 0x3D, 0x5B, 0x97, 0x68, 0xAF, 0x24, 0x62, 0x08, 0x3B, 0xE4, 0xB2, 0xD0, 0x2A, 0x13, 0xA9, 0x05, 0x99, 0xCA, 0x2E, 0x30, 0x5B, 0xE7, 0x38, 0x88, 0xA4, 0x6D, 0xC9, 0xD9,
	0xA4, 0xF0, 0xC4, 0xED, 0x39, 0x3E, 0x4C, 0xBD, 0x89, 0x0F, 0xBE, 0x13, 0x8C, 0x3D, 0x37, 0xE0, 0x1D, 0x3E, 0xE2, 0x0D, 0x20, 0x7F, 0xBB, 0x03, 0x02, 0xD2, 0xC4, 0x97, 0x56, 0xEB, 0x22, 0x92,
	0x68, 0x0B, 0x73, 0x6D, 0xB5, 0xD6, 0x44, 0x43, 0x29, 0x92, 0x8C, 0xBC, 0x02, 0x31, 0x53, 0xF7, 0x34, 0xD4, 0x70, 0x97, 0xA9, 0x12, 0xF9, 0x36, 0xFC, 0x1C, 0x33, 0xEF, 0xCE, 0xDB, 0x8C, 0xBD,
	0xA9, 0x12, 0x7B, 0xAB, 0xA4, 0x2C, 0x1E, 0x61, 0x50, 0x24, 0xD9, 0x4C, 0x16, 0x8B, 0x6B, 0xD6, 0x3E, 0x67, 0x3B, 0x91, 0x8D, 0x9A, 0xC8, 0x3E, 0x99, 0x07, 0xA7, 0x02, 0x4D, 0xAF, 0x67, 0x69,
	0xA2, 0x97, 0x75, 0xBE, 0xF5, 0x06, 0x5E, 0x40, 0xF1, 0x58, 0x96, 0x65, 0x7E, 0xDD, 0x6E, 0xC7, 0x0B, 0xA5, 0x67, 0xA9, 0x5A, 0xB4, 0xE8, 0x3F, 0x9D, 0xAB, 0xB2, 0x85, 0xDC, 0x9E, 0x31, 0xBB,
	0xC9, 0x0D, 0xBD, 0x93, 0x1C, 0x6C, 0x9F, 0x44, 0x13, 0x39, 0x8C, 0x24, 0x62, 0x6E, 0xB8, 0xA4, 0x5F, 0x66, 0x49, 0x26, 0x10, 0x02, 0xEE, 0xB5, 0xD2, 0x16, 0x3C, 0x24, 0xE5, 0x92, 0x22, 0x46,
	0x7F, 0xD5, 0xBA, 0x64, 0x26, 0xE8, 0x49, 0x93, 0xF1, 0xE4, 0x1F, 0xC6, 0x18, 0x41, 0x97, 0x08, 0x0F, 0x39, 0xB8, 0x4F, 0xA8, 0x8C, 0x4C, 0x8D, 0x50, 0x18, 0xB6, 0x29, 0x1D, 0xA9, 0x2C, 0x4E,
	0xCC, 0xAE, 0x48, 0xD6, 0x4A, 0x96, 0xD7, 0xC0, 0xD8, 0x91, 0xA2, 0x6A, 0xE1, 0x58, 0xA2, 0xA9, 0x7D, 0x22, 0x4D, 0x25, 0xD9, 0xD8, 0x6C, 0xA6, 0x9B, 0x34, 0xDF, 0x41, 0x82, 0x3B, 0x46, 0xA9,
	0x48, 0x30, 0xFF, 0x5A, 0x88, 0x21, 0x1C, 0xF2, 0x00, 0x02, 0xAF, 0x1F, 0x62, 0xFE, 0x3B, 0x80, 0xCF, 0x63, 0xDF, 0xBB, 0xE5, 0x3D, 0xA7, 0x07, 0x9D, 0x29, 0x84, 0x43, 0x07, 0xBA, 0xDE, 0x78,
	0xEA, 0xF3, 0xC1, 0x30, 0x84, 0xA1, 0x37, 0xC2, 0x14, 0x0D, 0xC0, 0x76, 0x7B, 0xF8, 0xAB, 0x1B, 0xFA, 0xBC, 0x33, 0x09, 0x3D, 0xFC, 0xE1, 0xC4, 0x0E, 0x70, 0xE5, 0x09, 0x0D, 0x30, 0xDB, 0x9D,
	0x82, 0xF3, 0x65, 0x8C, 0x29, 0x1C, 0x80, 0xE7, 0x03, 0xBF, 0x19, 0x8F, 0x38, 0x1A, 0xAB, 0xAB, 0x8B, 0x3B, 0x81, 0x05, 0xDC, 0xED, 0x8E, 0x26, 0x3D, 0xEE, 0x0E, 0x2C, 0x40, 0x03, 0x58, 0x7B,
	0x21, 0x8C, 0xF8, 0x0D, 0x0F, 0x71, 0x5A, 0xE8, 0x59, 0x66, 0xD3, 0x7A, 0x19, 0xDB, 0x2E, 0x03, 0xAF, 0x0F, 0x37, 0x8E, 0xDF, 0x1D, 0xE2, 0x57, 0xBB, 0x2A, 0x0C, 0x03, 0xA4, 0xCF, 0x43, 0x97,
	0xF6, 0xEA, 0xE3, 0x66, 0x36, 0x8C, 0x6D, 0x3F, 0xE4, 0xDD, 0xC9, 0xC8, 0xF6, 0x61, 0x3C, 0xF1, 0xC7, 0x5E, 0xE0, 0x00, 0xBA, 0xC5, 0x7A, 0x3C, 0xE8, 0x8E, 0x6C, 0x7E, 0xE3, 0xF4, 0x5A, 0xB8,
	0x3B, 0x55, 0xBB, 0x73, 0xEB, 0xB8, 0x21, 0x04, 0x43, 0x7B, 0x34, 0x7A, 0xE2, 0xA5, 0x77, 0xE7, 0x62, 0x1D, 0xA2, 0xB5, 0x3D, 0x17, 0x3B, 0x0E, 0x62, 0xB4, 0x3B, 0x23, 0x87, 0x36, 0x32, 0x4E,
	0xF6, 0xB8, 0xEF, 0x74, 0x43, 0xF2, 0x66, 0xFB, 0xD4, 0x45, 0xE2, 0x10, 0xDE, 0xC8, 0x82, 0x60, 0xEC, 0x74, 0x39, 0x3D, 0x38, 0x5F, 0x1C, 0xF4, 0xC5, 0xF6, 0xA7, 0x56, 0x6D, 0x33, 0x70, 0xFE,
	0x9E, 0xE0, 0x24, 0x1C, 0x84, 0x9E, 0x7D, 0x63, 0x0F, 0x9C, 0x80, 0x9D, 0x7E, 0x87, 0x11, 0x0C, 0x49, 0x77, 0xE2, 0x3B, 0x37, 0x04, 0x19, 0x69, 0x08, 0x26, 0x9D, 0x20, 0xE4, 0xE1, 0x24, 0x74,
	0x60, 0xE0, 0x79, 0x3D, 0xC3, 0x73, 0xE0, 0xF8, 0xB7, 0xBC, 0xEB, 0x04, 0x9F, 0xD9, 0xC8, 0x0B, 0x0C, 0x59, 0x93, 0xC0, 0xC1, 0x72, 0xB2, 0x43, 0xDB, 0x6C, 0x8C, 0x26, 0x90, 0xA9, 0xE0, 0x33,
	0x3D, 0x77, 0x26, 0x01, 0x37, 0x9C, 0x71, 0x37, 0x74, 0x7C, 0x7F, 0x32, 0x0E, 0xB9, 0xE7, 0x9E, 0x61, 0x78, 0xEF, 0x90, 0x15, 0xC4, 0x68, 0xE3, 0xD2, 0x9E, 0x09, 0xA6, 0xE7, 0x02, 0xB9, 0x8A,
	0x04, 0x79, 0xFE, 0x94, 0x8C, 0x12, 0x07, 0x86, 0x7B, 0x0B, 0xEE, 0x86, 0x0E, 0xFE, 0xEE, 0x13, 0x9F, 0x86, 0x29, 0x9B, 0x28, 0x08, 0x90, 0xB1, 0x6E, 0xB8, 0x3B, 0x0D, 0xF7, 0x43, 0x02, 0xC3,
	0x1D, 0x1F, 0xC1, 0x75, 0x06, 0x23, 0x3E, 0x70, 0xDC, 0xAE, 0x43, 0xA3, 0x1E, 0x59, 0xB9, 0xE3, 0x81, 0x73, 0x86, 0xA1, 0xE2, 0x01, 0x4D, 0xE0, 0xD5, 0xB6, 0x77, 0x36, 0xEE, 0x39, 0x31, 0x2E,
	0x53, 0x88, 0x10, 0x55, 0xF5, 0xC8, 0x03, 0xD6, 0x24, 0xAC, 0x65, 0x02, 0x09, 0xBC, 0x0F, 0x76, 0xEF, 0x96, 0x13, 0xEC, 0x7A, 0x32, 0x86, 0xBE, 0xE9, 0x9F, 0x15, 0x65, 0xDD, 0x61, 0x4D, 0x77,
	0x8B, 0xBD, 0xB9, 0x69, 0xD9, 0xE5, 0x4A, 0xA6, 0x28, 0x40, 0x45, 0xAE, 0x0A, 0x53, 0xF0, 0xD4, 0xB6, 0xE0, 0x50, 0xDB, 0x7A, 0x36, 0xF3, 0xAD, 0xAF, 0x31, 0x88, 0x05, 0x4D, 0x1E, 0xF5, 0x53,
	0x99, 0x3C, 0xD4, 0x00, 0x3F, 0x68, 0xF2, 0x27, 0xA0, 0xFC, 0x78, 0x43, 0x7E, 0x66, 0xF2, 0x78, 0x0D, 0xFA, 0x67, 0x3A, 0x7E, 0x71, 0xAC, 0xD6, 0xBF, 0x35, 0x49, 0x22, 0xF0, 0x9A, 0x02, 0x6C,
	0xFB, 0x3E, 0xCC, 0xF0, 0x65, 0xEC, 0xA1, 0xF5, 0x6B, 0xF2, 0x92, 0x2A, 0xE5, 0x93, 0x86, 0x4C, 0xAC, 0xD0, 0x49, 0xF1, 0x48, 0x8E, 0x20, 0x1E, 0x0A, 0x3B, 0xC2, 0x54, 0x20, 0xB3, 0x58, 0x15,
	0x98, 0x02, 0x18, 0x61, 0x8C, 0xD6, 0x4A, 0xE1, 0xFB, 0x54, 0x5D, 0xA5, 0x1A, 0xDF, 0x22, 0x8B, 0xE4, 0x9E, 0x5E, 0x03, 0xA8, 0x38, 0x9F, 0x38, 0xBE, 0xA9, 0xDC, 0x26, 0x2F, 0x74, 0x2E, 0x23,
	0x4A, 0x04, 0x5C, 0x9E, 0x50, 0xBA, 0x14, 0x94, 0x02, 0x59, 0x95, 0x0C, 0x5A, 0x23, 0x33, 0xAD, 0xFF, 0xD7, 0xF1, 0x8D, 0x06, 0xF9, 0x98, 0x59, 0xAF, 0xAA, 0xAE, 0x1D, 0xDE, 0x38, 0xA3, 0x5D,
	0x51, 0x85, 0x27, 0xA2, 0xDA, 0x98, 0xFC, 0x98, 0xB8, 0xC2, 0xBE, 0xB8, 0x56, 0x26, 0x9F, 0x48, 0xAC, 0xF5, 0x06, 0x7D, 0x45, 0x7C, 0xAE, 0xE7, 0x5E, 0x70, 0xB7, 0xEF, 0xE3, 0xB6, 0x95, 0x4A,
	0x91, 0x57, 0xCF, 0x1C, 0x3F, 0x28, 0xBE, 0x95, 0xB3, 0x7B, 0xF2, 0x0A, 0x87, 0xE5, 0x75, 0x87, 0xCB, 0xF7, 0x2A, 0x2D, 0x1C, 0x52, 0xDA, 0xCA, 0xE4, 0x7B, 0xF5, 0x16, 0x0E, 0xE8, 0x6D, 0x6D,
	0xF2, 0xBD, 0xB2, 0x0B, 0xCF, 0x64, 0x77, 0xE3, 0xF8, 0xBB, 0xF5, 0x77, 0xC7, 0xF3, 0x27, 0x79, 0xF9, 0x71, 0x35, 0x86, 0xAD, 0x1A, 0x57, 0x26, 0x7F, 0x5C, 0x93, 0x5F, 0xAF, 0x9E, 0x97, 0xD4,
	0x3A, 0xC4, 0x18, 0x21, 0xE6, 0x7D, 0xD1, 0xA6, 0x56, 0xF7, 0x25, 0xC9, 0x97, 0x2D, 0x85, 0xC7, 0x87, 0xBE, 0xC2, 0xC3, 0x66, 0x2D, 0xDD, 0x4F, 0x75, 0xFB, 0xF2, 0xCA, 0x3A, 0x34, 0xF1, 0xF7,
	0x38, 0x25, 0x68, 0x46, 0x8B, 0x48, 0x86, 0x18, 0xFB, 0xA8, 0x56, 0xB0, 0xDD, 0x63, 0xC2, 0x0F, 0x9F, 0x11, 0x0E, 0xED, 0x8F, 0xFB, 0xEC, 0xF8, 0xDF, 0xEC, 0x5F, 0xC5, 0x47, 0x1E, 0x1D, 0x02,
	0x54, 0x6E, 0xB1, 0x58, 0x45, 0x6B, 0xBA, 0x41, 0x10, 0x4D, 0x58, 0xDA, 0xC8, 0xB8, 0xA2, 0x33, 0x3C, 0x0A, 0x49, 0x89, 0xDA, 0x20, 0x52, 0xBD, 0x65, 0xD7, 0x84, 0xA4, 0xD6, 0xBD, 0x0D, 0x74,
	0xE3, 0x8D, 0x2B, 0x13, 0xB3, 0x88, 0x06, 0x8D, 0x0C, 0x21, 0x94, 0x17, 0xB2, 0x06, 0xD5, 0x69, 0x3B, 0xCF, 0xF0, 0x9E, 0x94, 0x9A, 0x21, 0xEE, 0xCA, 0x22, 0x2A, 0x95, 0xD1, 0xB0, 0x77, 0xE8,
	0x17, 0x7B, 0xAF, 0x6A, 0x1D, 0xE5, 0xC8, 0xC6, 0xBE, 0x7E, 0x35, 0xF2, 0xF2, 0xE9, 0xD3, 0x21, 0x7D, 0x79, 0x9B, 0xAE, 0xB0, 0xB7, 0xE9, 0xCA, 0x77, 0x0E, 0x6D, 0xEC, 0xA5, 0x43, 0xDB, 0x9E,
	0x6E, 0xBC, 0x70, 0x6A, 0xEB, 0x7B, 0x13, 0x17, 0x5B, 0x2E, 0xF6, 0x51, 0xF6, 0xEA, 0x81, 0x0D, 0xBE, 0x7B, 0x60, 0x63, 0x1F, 0x95, 0x11, 0x76, 0x14, 0x01, 0x61, 0x1F, 0x3B, 0xB0, 0xD5, 0xCA,
	0xC1, 0x7E, 0x9B, 0x03, 0x1B, 0x7B, 0x2E, 0x11, 0xEF, 0x38, 0xB0, 0xBD, 0xED, 0xB4, 0x46, 0x65, 0x6A, 0x67, 0x71, 0xA1, 0x92, 0x18, 0xBC, 0x5C, 0x66, 0x17, 0x41, 0xD5, 0x22, 0xC7, 0x85, 0xFA,
	0x47, 0x46, 0xA5, 0xB9, 0x72, 0x82, 0xF3, 0xE7, 0xF7, 0x89, 0x57, 0xE6, 0xBA, 0x70, 0x77, 0x25, 0xEC, 0xAF, 0xC4, 0x55, 0xB4, 0x70, 0x54, 0x5D, 0x58, 0xC6, 0xF5, 0xFD, 0xA5, 0xD9, 0x2E, 0x17,
	0x11, 0xFE, 0xA9, 0x47, 0x2C, 0xB8, 0x95, 0x05, 0xD5, 0x28, 0x5C, 0xB5, 0x2E, 0xE1, 0x94, 0x26, 0x9C, 0xD4, 0x43, 0x27, 0x67, 0x9F, 0xC9, 0xC4, 0xA3, 0x5A, 0x6F, 0xDE, 0x79, 0x49, 0x52, 0x4C,
	0x0F, 0x30, 0x37, 0xB3, 0xF2, 0x5B, 0x24, 0x73, 0x72, 0x8D, 0xAE, 0x06, 0xF3, 0x34, 0x11, 0x59, 0x24, 0xB7, 0x2D, 0xAC, 0xB6, 0xD2, 0x22, 0x1B, 0xD3, 0xDA, 0x86, 0x9A, 0x99, 0x7E, 0x2F, 0x4C,
	0x93, 0x6D, 0x5A, 0x58, 0x3D, 0x11, 0x44, 0x03, 0xDA, 0x7C, 0xEA, 0xAB, 0xB2, 0x87, 0x87, 0x87, 0x96, 0x30, 0x88, 0xA9, 0xCB, 0xB5, 0xEB, 0x0B, 0x58, 0xDD, 0x1E, 0x61, 0x1E, 0x62, 0x96, 0x5F,
	0x20, 0xEA, 0x7A, 0xD5, 0x24, 0x4B, 0xA5, 0xA6, 0x83, 0xE3, 0xBF, 0xEB, 0xA4, 0x40, 0x8F, 0x67, 0x8F, 0x20, 0x72, 0x44, 0x15, 0x89, 0x19, 0x62, 0x4D, 0xC5, 0x03, 0xB5, 0x37, 0xB1, 0x28, 0x64,
	0xD5, 0xF3, 0x10, 0x06, 0x35, 0x2A, 0xEC, 0xDA, 0xD6, 0x26, 0x26, 0x64, 0x66, 0x7B, 0x27, 0xBA, 0x4B, 0x5A, 0x83, 0x11, 0x5D, 0xDF, 0x9D, 0x60, 0x5A, 0xFA, 0xE6, 0xAD, 0xB7, 0x63, 0x07, 0x3C,
	0xB0, 0xC8, 0xC8, 0x1D, 0x0F, 0x87, 0x94, 0x54, 0xBB, 0x5D, 0xC5, 0xD4, 0x65, 0x8F, 0x53, 0x19, 0x98, 0xD2, 0xA1, 0xE4, 0xFB, 0x0B, 0xEB, 0xDA, 0x82, 0xBA, 0xB5, 0xCB, 0x6F, 0x79, 0x41, 0x1E,
	0x20, 0xCC, 0x84, 0xE8, 0xA4, 0xDB, 0x3F, 0xB4, 0x15, 0x48, 0xB9, 0x07, 0x61, 0x5E, 0x77, 0xF7, 0x4D, 0xD7, 0x4D, 0x45, 0xB6, 0x58, 0x8B, 0x85, 0x84, 0x05, 0xCA, 0x57, 0x91, 0x91, 0x0E, 0x6D,
	0x5B, 0xAF, 0x51, 0x29, 0x32, 0x93, 0x26, 0x28, 0xE5, 0xA2, 0x12, 0xAE, 0x67, 0x7E, 0xD1, 0x46, 0xF8, 0xCA, 0xF2, 0x1F, 0xF8, 0xB9, 0x5A, 0xB2, 0x5A, 0x19, 0x00, 0x00
};
*/

enum AdkDisplayMode{

  AdkShowAnimation,
  AdkShowAccel,
  AdkShowMag,
  AdkShowTemp,
  AdkShowHygro,
  AdkShowBaro,
  AdkShowProx,
  AdkShowColor,

  AdkShowLast	//always last
};

#define SETTINGS_NAME    "/AdkSettings.bin"   //   not use sdcard
#define SETTINGS_MAGIX   0xAF
typedef struct AdkSettings{
  
  uint_8 magix;
  uint_8 ver;

//v1 settings:

  uint_8 R, G, B, bri, vol, almH, almM, almOn;
  char btName[16]; //null terminated
  char btPIN[17];  //null terminated

  uint_16 almSnooze;
  char almTune[32]; // null terminated
  
  uint_8 speed, displayMode;
  
//later settings
  
}AdkSettings;


AdkSettings settings;

/* dummy sensor */
volatile static int_32 hTemp=60, hHum=60, bPress=60, bTemp=60;
volatile static uint_16 prox[7] ={60,60,60,60,60,60,60}; //prox,  clear, IR ,R, G, B, temp pProx, pClear, pR, pG, pB, pIR, pTemp;
volatile static uint_16 proxNormalized[3] = {0,0,0};
volatile static int_16 accel[3]={60,60,60}, mag[3]={60,60,60};
volatile static char locked = 0;
#endif // BT_ADK2012_TEST


#if 0
typedef struct bluetooth_device_struct {
   /* This must be the first member of this structure, because sometimes we can use it
   ** as pointer to CLASS_CALL_STRUCT, other time as a pointer to BLUETOOTH_DEVICE_STRUCT
   */
   CLASS_CALL_STRUCT                ccs;
} BLUETOOTH_DEVICE_STRUCT,  _PTR_ BLUETOOTH_DEVICE_STRUCT_PTR;
#endif

//typedef struct {
//    CLASS_CALL_STRUCT_PTR ccs;     /* class call struct of MSD instance */
//    uint_8                body;    /* message body one of USB_EVENT_xxx as defined above */
//} usb_msg_t;

/* include in hci_transport.h
#define USB_EVENT_ATTACH    (1)
#define USB_EVENT_DETACH    (2)
#define USB_EVENT_INTF      (3)
*/


LWSEM_STRUCT   USB_Stick;

#define MAX_PACKET_SZ                       260  //256b payload + header

#ifdef BT_ADK2012_TEST
// command header
// u8 cmd opcode
// u8 sequence
// u16 size

// data formats:
//   timespec = (year,month,day,hour,min,sec) (u16,u8,u8,u8,u8,u8)

#define CMD_MASK_REPLY                      0x80
#define BT_CMD_GET_PROTO_VERSION            1    // () -> (u8 protocolVersion)
#define BT_CMD_GET_SENSORS                  2    // () -> (sensors: i32,i32,i32,i32,u16,u16,u16,u16,u16,u16,u16,i16,i16,i16,i16,i16,i16) 
#define BT_CMD_FILE_LIST                    3    // FIRST: (char name[]) -> (fileinfo or single zero byte)   OR   NONLATER: () -> (fileinfo or empty or single zero byte)
#define BT_CMD_FILE_DELETE                  4    // (char name[0-255)) -> (char success)
#define BT_CMD_FILE_OPEN                    5    // (char name[0-255]) -> (char success)
#define BT_CMD_FILE_WRITE                   6    // (u8 data[]) -> (char success)
#define BT_CMD_FILE_CLOSE                   7    // () -> (char success)
#define BT_CMD_GET_UNIQ_ID                  8    // () -> (u8 uniq[16])
#define BT_CMD_BT_NAME                      9    // (char name[]) -> () OR () -> (char name[])
#define BT_CMD_BT_PIN                      10    // (char PIN[]) -> () OR () -> (char PIN[])
#define BT_CMD_TIME                        11    // (timespec) -> (char success)) OR () > (timespec)
#define BT_CMD_SETTINGS                    12    // () -> (alarm:u8,u8,u8,brightness:u8,color:u8,u8,u8:volume:u8) or (alarm:u8,u8,u8,brightness:u8,color:u8,u8,u8:volume:u8) > (char success)
#define BT_CMD_ALARM_FILE                  13    // () -> (char file[0-255]) OR (char file[0-255]) > (char success)
#define BT_CMD_GET_LICENSE                 14    // () -> (u8 licensechunk[]) OR () if last sent
#define BT_CMD_DISPLAY_MODE                15    // () -> (u8) OR (u8) -> ()
#define BT_CMD_LOCK                        16    // () -> (u8) OR (u8) -> ()

#define BT_PROTO_VERSION_1                  1    //this line marks the end of v1.0 API, all things after this are the next version


//constants
#define BT_PROTO_VERSION_CURRENT            BT_PROTO_VERSION_1

static uint8_t cmdBuf[MAX_PACKET_SZ];
static uint32_t bufPos = 0;

// start adk app
static void putLE32(uint_8* buf, uint_16* idx, uint_32 val){

  buf[(*idx)++] = val;
  buf[(*idx)++] = val >> 8;
  buf[(*idx)++] = val >> 16;
  buf[(*idx)++] = val >> 24;
}

static void putLE16(uint_8* buf, uint_16* idx, uint_32 val){

  buf[(*idx)++] = val;
  buf[(*idx)++] = val >> 8;
}


void readSettings(){
 
   //uint_32 read;
   char r;
   AdkSettings ts;
   
   //apply defaults
   strcpy(settings.btName, "ADK 2012");
   strcpy(settings.btPIN, "1337");
   settings.magix = SETTINGS_MAGIX;
   settings.ver = 1;
#if 0
   settings.R = 0;
   settings.G = 0;
   settings.B = 255;
   settings.bri = 255;
   settings.vol = 255;
   settings.almH = 6;
   settings.almM = 0;
   settings.almOn = 0;
#endif    
   settings.speed = 1;
   settings.displayMode = AdkShowAnimation;
   settings.almSnooze = 10 * 60;	//10-minute alarm
   strcpy(settings.almTune, "Alarm_Rooster_02.ogg");
#ifdef ACCESSORY_PRINTF   
   printf("ADK: settings: read dummy\n");
#endif
}


LWGPIO_STRUCT led[4], btn1;
const uint_32 led_mux_gpio[4] ={BSP_LED1_MUX_GPIO,BSP_LED2_MUX_GPIO,BSP_LED3_MUX_GPIO,BSP_LED4_MUX_GPIO};
const uint_32 led_pin_id[4]  = {BSP_LED1,BSP_LED2,BSP_LED3,BSP_LED4}; 

uint_8 led_stage [5] = {0,63,127,191,255};

static void test_leds(uint_8 i){
	for(int j=0 ; j< 4; j++){
		lwgpio_set_value(&led[j],LWGPIO_VALUE_HIGH ); /* set pin to 1 */
	}
		
	 lwgpio_set_value(&led[i], LWGPIO_VALUE_LOW); /* set pin to 1 */
}

void writeSettings(){
	int i;
 	//printf("ADK: settings: write dummy\n");
#ifdef ACCESSORY_PRINTF
 	printf(" R=%d,G=%d,B=%d\n",  settings.R , settings.G,  settings.B);
#endif
      // for demo
      i = 0;
      while(i  <  4) {
	  	if((led_stage[i] <= settings.R) && (settings.R < led_stage[i+1] )){
			test_leds(i);
			break;
	  	}
		i++;
      	}
}

void setVolume(uint_8 vol){
#ifdef ACCESSORY_PRINTF	
    printf("setVolume %d\n",vol);
#endif
}

void rtcSet(uint_16 year, uint_8 month, uint_8 day, uint_8 hour, uint_8 minute, uint_8 second){

    printf("year=%d, month=%d, day=%d, hour=%d, minute=%d, second=%d,",year, month, day, hour, minute, second);
	
}

static uint_16 adkProcessCommand(uint_8 cmd, const uint_8* dataIn, uint_16 sz, char fromBT, uint_8* reply, uint_16 maxReplySz){  //returns num bytes to reply with (or 0 for no reply)

  uint_16 sendSz = 0;
    static uint16_t btLicPos = 0, usbLicPos = 0;
  uint16_t* licPos = fromBT ? &btLicPos : &usbLicPos;
  

#ifdef ACCESSORY_PRINTF 
  printf("ADK: BT: have cmd 0x%x with %db of data\n", cmd, sz);
#endif
  
  //NOTE: this code was written in a hurry and features little error checking. yes, I know it's bad. -DG
  
  //process packet
  switch(cmd){
      
    case BT_CMD_GET_PROTO_VERSION:
       // dbgPrintf("BT_CMD_GET_PROTO_VERSION\n");
      {
        reply[sendSz++] = BT_PROTO_VERSION_CURRENT;
      }
      break;
      
    case BT_CMD_GET_SENSORS:
        //dbgPrintf("BT_CMD_GET_SENSORS\n");
      {   
        putLE32(reply, &sendSz, hTemp);
        putLE32(reply, &sendSz, hHum);
        putLE32(reply, &sendSz, bPress);
        putLE32(reply, &sendSz, bTemp);
        putLE16(reply, &sendSz, prox[0]);
        putLE16(reply, &sendSz, prox[1]);
        putLE16(reply, &sendSz, prox[3]);
        putLE16(reply, &sendSz, prox[4]);
        putLE16(reply, &sendSz, prox[5]);
        putLE16(reply, &sendSz, prox[2]);
        putLE16(reply, &sendSz, prox[6]);
        putLE16(reply, &sendSz, accel[0]);
        putLE16(reply, &sendSz, accel[1]);
        putLE16(reply, &sendSz, accel[2]);
        putLE16(reply, &sendSz, mag[0]);
        putLE16(reply, &sendSz, mag[1]);
        putLE16(reply, &sendSz, mag[2]);
      }
      break;

    case BT_CMD_FILE_LIST:
        //dbgPrintf("BT_CMD_FILE_LIST\n");
      {
      
        reply[sendSz++] = 0;
      }
      break;
       
    case BT_CMD_FILE_DELETE:
        //dbgPrintf("BT_CMD_FILE_DELETE\n");
      {
        reply[sendSz++] = 0;
      }
      break;
       
    case BT_CMD_FILE_OPEN:
        //dbgPrintf("BT_CMD_FILE_OPEN\n");
      {
       reply[sendSz++] = 0;
      }
      break;
       
    case BT_CMD_FILE_WRITE:
        //dbgPrintf("BT_CMD_FILE_WRITE\n");
      {reply[sendSz++] = 0;}
      break;
       
    case BT_CMD_FILE_CLOSE:
         //dbgPrintf("BT_CMD_FILE_CLOSE\n");
      {
        reply[sendSz++] = 0;
      }
      break;
       
    case BT_CMD_GET_UNIQ_ID:
        //dbgPrintf("BT_CMD_GET_UNIQ_ID\n");
      {
        putLE32(reply, &sendSz, 0);
        putLE32(reply, &sendSz, 1);
        putLE32(reply, &sendSz, 2);
        putLE32(reply, &sendSz, 3);
      }
      break;
      
    case BT_CMD_BT_NAME:
        //dbgPrintf("BT_CMD_BT_NAME\n");
      {
        if(sz){  //set
          strcpy(settings.btName, (char*)dataIn);
          reply[sendSz++] = 1;
          writeSettings();
        }
        else{
          strcpy((char*)reply, settings.btName);
          sendSz = strlen((char*)reply) + 1;
        }
      }
      break;
      
    case BT_CMD_BT_PIN:
         //dbgPrintf("BT_CMD_BT_PIN\n");
      {
        if(sz){  //set
          strcpy(settings.btPIN, (char*)dataIn);
          reply[sendSz++] = 1;
          writeSettings();
        }
        else{
          strcpy((char*)reply, settings.btPIN);
          sendSz = strlen((char*)reply) + 1;
        }
      }
      break;
    case BT_CMD_TIME:
        //dbgPrintf("BT_CMD_TIME\n");
      {
        if (sz >= 7) {  //set
          rtcSet(dataIn[1] << 8 | dataIn[0], dataIn[2], dataIn[3], dataIn[4], dataIn[5], dataIn[6]);
          reply[sendSz++] = 1;
        } else if (sz == 0) {
          reply[0] = 0;reply[2]=0;reply[3]=0; reply[4]=0;reply[5]=0; reply[6]=0;
          sendSz += 7;
        }
      }
      break;
    case BT_CMD_SETTINGS:
        //dbgPrintf("BT_CMD_SETTINGS sz %d\n",sz);
      {
        if (sz >= 8) {  //set
          settings.almH = dataIn[0];
          settings.almM = dataIn[1];
          settings.almOn = dataIn[2];
          settings.bri = dataIn[3];
          settings.R = dataIn[4];
          settings.G = dataIn[5];
          settings.B = dataIn[6];
          settings.vol = dataIn[7];
          setVolume(settings.vol);
          writeSettings();
          reply[sendSz++] = 1;
        } else {  /*  get setting */
          reply[sendSz++] = settings.almH;
          reply[sendSz++] = settings.almM;
          reply[sendSz++] = settings.almOn;
          reply[sendSz++] = settings.bri;
          reply[sendSz++] = settings.R;
          reply[sendSz++] = settings.G;
          reply[sendSz++] = settings.B;
          reply[sendSz++] = settings.vol;
        }
      }
      break;
    case BT_CMD_ALARM_FILE:
        //dbgPrintf("BT_CMD_ALARM_FILE\n");
      {
        if(sz){  //set
          strcpy(settings.almTune, (char*)dataIn);
          reply[sendSz++] = 1;
          writeSettings();
        } else{
          strcpy((char*)reply, settings.almTune);
          sendSz = strlen((char*)reply) + 1;
        }
      }
      break;
    case BT_CMD_GET_LICENSE:
        //dbgPrintf("BT_CMD_GET_LICENSE\n");
#if 1        
      {
          reply[sendSz++] = 0;
      }
#else
      {
        static const uint32_t maxPacket = MAX_PACKET_SZ - 10;	//seems reasonable

        if(*licPos >= sizeof(gzippedLicences)){	//send terminator
          reply[sendSz++] = 0;
          *licPos = 0;
        }
        else{

          uint32_t left = sizeof(gzippedLicences) - *licPos;
          if(left > maxPacket) left = maxPacket;
          reply[sendSz++] = 1;
          while(left--) reply[sendSz++] = gzippedLicences[(*licPos)++];
        }
      }
#endif
      break;
    case BT_CMD_DISPLAY_MODE:
        //dbgPrintf("BT_CMD_DISPLAY_MODE\n");
      {
        if (sz) {  //set
          settings.displayMode = dataIn[0];
          reply[sendSz++] = 1;
        } else if (sz == 0) {
          reply[sendSz++] = settings.displayMode;
        }
      }
      break;
    case BT_CMD_LOCK:
        //dbgPrintf("BT_CMD_LOCK\n");
      {
        if (sz) {  //set
          locked = dataIn[0] ? 2 : 0;
          reply[sendSz++] = 1;
        } else if (sz == 0) {
          reply[sendSz++] = locked;
        }
      }
      break;
  }
  return sendSz;
}



void int_service_routine(void *pin)
{
    lwgpio_int_clear_flag((LWGPIO_STRUCT_PTR) pin);
	settings.vol += 5;
	if(settings.vol >= 255)
		settings.vol =0;
    //printf("btn1 press\n");
}

static void BSP_LED_BTN_INIT(uint_8 val)
{
    int i;
    for(i=0;i<4;i++){
	    if (!lwgpio_init(&led[i], led_pin_id[i], LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE))
	    {
	        printf("Initializing LED%d GPIO as output failed.\n",i);
	        //_task_block();
	        continue;
	    }
	    /* swich pin functionality (MUX) to GPIO mode */
	    lwgpio_set_functionality(&led[i], led_mux_gpio[i]);

	    /* write logical 1 to the pin */
	    lwgpio_set_value(&led[i], /*LWGPIO_VALUE_HIGH*/val); /* set pin to 1 */
    	}


    if (!lwgpio_init(&btn1, BSP_BUTTON1, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE))
    {
        printf("Initializing button 1 GPIO as input failed.\n");
        //_task_block();
    }


    lwgpio_set_functionality(&btn1, BSP_BUTTON1_MUX_IRQ);
    lwgpio_set_attribute(&btn1, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);

    /* enable gpio functionality for given pin, react on falling edge */
    if (!lwgpio_int_init(&btn1, LWGPIO_INT_MODE_FALLING))
    {
        printf("Initializing button GPIO for interrupt failed.\n");
        _task_block();
    }

    /* install gpio interrupt service routine */
    _int_install_isr(lwgpio_int_get_vector(&btn1), int_service_routine, (void *) &btn1);
    /* set the interrupt level, and unmask the interrupt in interrupt controller*/
    _bsp_int_init(lwgpio_int_get_vector(&btn1), 3, 0, TRUE);
    /* enable interrupt on GPIO peripheral module*/
    lwgpio_int_enable(&btn1, TRUE);

}
#endif  // BT_ADK2012_TEST


static void btAdkPortOpen(void* port, uint8_t dlci){
#ifdef BT_ADK2012_TEST
    bufPos = 0;
#endif    
}

static void btAdkPortClose(void* port, uint8_t dlci){

    //nothing here [yet?]
}

/* todo, move cmd handle from btTask callback  to  to a new task  */
static void btAdkPortRx(void* port, uint8_t dlci, const uint8_t* data, uint16_t sz)
{
#ifdef BT_ADK2012_TEST
        // adk2012
    uint8_t reply[MAX_PACKET_SZ];
    uint32_t i;
    uint8_t seq, cmd;
    uint16_t cmdSz;
    uint8_t* ptr;
    
    //printf("btAdkPortRx + %d\n",sz);
      
    while(sz || bufPos){
      
      uint16_t sendSz = 0;
      
      //copy to buffer as much as we can
      while(bufPos < MAX_PACKET_SZ && sz){
        cmdBuf[bufPos++] = *data++;
        sz--;
      }
      
      //see if a packet exists
      if(bufPos < 4) {
        printf("too small to be a packet-> discard %d\n",bufPos);
        //-bufPos = 0;
        return; // too small to be a packet -> discard
        }
      cmd = cmdBuf[0];
      seq = cmdBuf[1];
      cmdSz = cmdBuf[3];
      cmdSz <<= 8;
      cmdSz += cmdBuf[2];

      //printf("cmdSz + %d\n",cmdSz);
            
      if(bufPos - 4 < cmdSz) {
        printf("not entire command received yet %d\n",bufPos);
        //-bufPos = 0;
        return; //not entire command received yet
      }
      
      sendSz = adkProcessCommand(cmd, cmdBuf + 4, cmdSz, 1, reply + 4, MAX_PACKET_SZ - 4);  /* l2cap call service rx  ,到这里处理命令*/
      if(sendSz){
                
        reply[0] = cmd | CMD_MASK_REPLY;
        reply[1] = seq;
        reply[2] = sendSz;
        reply[3] = sendSz >> 8;
        sendSz += 4;
      
        ADK_btRfcommPortTx(port, dlci, reply, sendSz);
      }
      
      //adjust buffer as needed
      for(i = 0; i < bufPos - cmdSz - 4; i++){
        cmdBuf[i] =  cmdBuf[i + cmdSz + 4];
      }
      bufPos = i;
    }
    
    //printf("btAdkPortRx -\n");
#endif // BT_ADK2012_TEST        
}


//////////// bt support (boring)

volatile static uint32_t btSSP = ADK_BT_SSP_DONE_VAL;
const char* btPIN = 0;
char btName[64];

#define  maxPairedDevices 4
static uint8_t numPairedDevices = 0;
static uint8_t savedMac[maxPairedDevices][BLUETOOTH_MAC_SIZE];
static uint8_t savedKey[maxPairedDevices][BLUETOOTH_LINK_KEY_SIZE];

static void adkBtSspF(const uint8_t* mac, uint32_t val)
{

  btSSP = val;
  printf("ssp with val %u\n", val);
}

static char adkBtConnectionRequest(const uint8_t* mac, uint32_t devClass, uint8_t linkType){	//return 1 to accept

    printf("Accepting connection from %02X:%02X:%02X:%02X:%02X:%02X\n", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
    return 1;
}

static char adkBtLinkKeyRequest(const uint8_t* mac, uint8_t* buf){ //link key create

  uint8_t i, j;
  
  printf("Key request from %02X:%02X:%02X:%02X:%02X:%02X\n  -> ", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);

  for(i = 0; i < numPairedDevices; i++){
 
    for(j = 0; j < BLUETOOTH_MAC_SIZE && savedMac[i][j] == mac[j]; j++);
    if(j == BLUETOOTH_MAC_SIZE){ //match
    
       printf("{");
        for(j = 0; j < BLUETOOTH_LINK_KEY_SIZE; j++){
         
          printf(" %02X", savedKey[i][j]);
          buf[j] = savedKey[i][j];
        }
        printf(" }");
        return 1;
    }
  }
  printf("FAIL\n");
  return 0;
}

static void adkBtLinkKeyCreated(const uint8_t* mac, const uint8_t* buf){ 	//link key was just created, save it if you want it later

   uint8_t j;
   
   printf("Key created for %02X:%02X:%02X:%02X:%02X:%02X <- {", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
   
   for(j = 0; j < BLUETOOTH_LINK_KEY_SIZE; j++){
         
     printf(" %02X", buf[j]);
   }
   printf(" }\n");
    
   if(numPairedDevices < maxPairedDevices){
      
      for(j = 0; j < BLUETOOTH_LINK_KEY_SIZE; j++) savedKey[numPairedDevices][j] = buf[j];
      for(j = 0; j < BLUETOOTH_MAC_SIZE; j++) savedMac[numPairedDevices][j] = mac[j];
      numPairedDevices++;
      printf("saved to slot %d/%d\n", numPairedDevices, maxPairedDevices);
   }
   else{
      printf("out of slots...discaring\n");
   }
}

static char adkBtPinRequest(const uint8_t* mac, uint8_t* buf){		//fill buff with PIN code, return num bytes used (16 max) return 0 to decline

   uint8_t v, i = 0;

   printf("PIN request from %02X:%02X:%02X:%02X:%02X:%02X\n  -> ", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
   
   if(btPIN){
     printf(" -> using pin '%s'\n", btPIN);
     for(i = 0; btPIN[i]; i++) buf[i] = btPIN[i];
     return i;
   }
   else dbgPrintf(" no PIN set. rejecting\n");
   return 0;
}

#define MAGIX	0xFA

int magix_offset = 0;

#if 1
static uint8_t sdpDescrADK[] =
{
        //service class ID list
        SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), 0x00, 0x01, SDP_ITEM_DESC(SDP_TYPE_ARRAY, SDP_SZ_u8), 17,
            SDP_ITEM_DESC(SDP_TYPE_UUID, SDP_SZ_16), BT_ADK_UUID,
        //ServiceId
        SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), 0x00, 0x03, SDP_ITEM_DESC(SDP_TYPE_UUID, SDP_SZ_2), 0x11, 0x01,
        //ProtocolDescriptorList
        SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), 0x00, 0x04, SDP_ITEM_DESC(SDP_TYPE_ARRAY, SDP_SZ_u8), 15,
            SDP_ITEM_DESC(SDP_TYPE_ARRAY, SDP_SZ_u8), 6,
                SDP_ITEM_DESC(SDP_TYPE_UUID, SDP_SZ_2), 0x01, 0x00, // L2CAP
                SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), L2CAP_PSM_RFCOMM >> 8, L2CAP_PSM_RFCOMM & 0xFF, // L2CAP PSM
            SDP_ITEM_DESC(SDP_TYPE_ARRAY, SDP_SZ_u8), 5,
                SDP_ITEM_DESC(SDP_TYPE_UUID, SDP_SZ_2), 0x00, 0x03, // RFCOMM
                SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_1), MAGIX, // port ###
        //browse group list
        SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), 0x00, 0x05, SDP_ITEM_DESC(SDP_TYPE_ARRAY, SDP_SZ_u8), 3,
            SDP_ITEM_DESC(SDP_TYPE_UUID, SDP_SZ_2), 0x10, 0x02, // Public Browse Group
        //name
        SDP_ITEM_DESC(SDP_TYPE_UINT, SDP_SZ_2), 0x01, 0x00, SDP_ITEM_DESC(SDP_TYPE_TEXT, SDP_SZ_u8), 12, 'A', 'D', 'K', ' ', 'B', 'T', ' ', 'C', 'O', 'M', 'M', 'S'
};
#endif

static void btStart(){  /* called after ADK_adkInit in main => setup */
    uint8_t i, dlci;
    int f;
  
    ADK_btEnable(adkBtConnectionRequest, adkBtLinkKeyRequest, adkBtLinkKeyCreated, adkBtPinRequest, NULL);
#ifdef BT_ADK2012_TEST
    dlci = ADK_btRfcommReserveDlci(RFCOMM_DLCI_NEED_EVEN);

    if(!dlci) dbgPrintf("BTADK: failed to allocate DLCI\n");
    else{

        //change descriptor to be valid...
        for(i = 0, f = -1; i < sizeof(sdpDescrADK); i++){

            if(sdpDescrADK[i] == MAGIX){
                if(f == -1) f = i;
                else break;
            }
        }

        if(i != sizeof(sdpDescrADK) || f == -1){

            dbgPrintf("BTADK: failed to find a single marker in descriptor\n");
            ADK_btRfcommReleaseDlci(dlci);
            return;
        }

        sdpDescrADK[f] = dlci >> 1; 
        magix_offset = f;
        
        dbgPrintf("BTADK has DLCI %u\n", dlci);

        ADK_btRfcommRegisterPort(dlci, btAdkPortOpen, btAdkPortClose, btAdkPortRx);
        ADK_btSdpServiceDescriptorAdd(sdpDescrADK, sizeof(sdpDescrADK));
    }
#endif    

      btPIN = "0000";

          if(!ADK_btSetDeviceClass(DEVICE_CLASS_SERVICE_AUDIO | DEVICE_CLASS_SERVICE_RENDERING |
		DEVICE_CLASS_SERVICE_INFORMATION | (DEVICE_CLASS_MAJOR_AV << DEVICE_CLASS_MAJOR_SHIFT) |
		(DEVICE_CLASS_MINOR_AV_PORTBL_AUDIO << DEVICE_CLASS_MINOR_AV_SHIFT))) dbgPrintf("ADK: Failed to set device class\n");
  if(!ADK_btSetLocalName(btName)) dbgPrintf("ADK: failed to set BT name\n");
  if(!ADK_btDiscoverable(1)) dbgPrintf("ADK: Failed to set discoverable\n");      /*  Before the loop , enable the inqure and  page !!! */
  if(!ADK_btConnectable(1)) dbgPrintf("ADK: Failed to set connectable\n");
  ADK_btSetSspCallback(adkBtSspF);
}

/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : USB_task
* Returned Value : None
* Comments       :
*     First function called. This rouine just transfers control to host main
*END*--------------------------------------------------------------------*/

void USB_task(uint_32 param)
{ 
    //_usb_host_handle     host_handle;
    USB_STATUS           error;
    //pointer              usb_fs_handle = NULL;
    usb_msg_t            msg;
    int usb_task_state = 0;
   
#if 0
#if DEMOCFG_USE_POOLS && defined(DEMOCFG_MFS_POOL_ADDR) && defined(DEMOCFG_MFS_POOL_SIZE)
    _MFS_pool_id = _mem_create_pool((pointer)DEMOCFG_MFS_POOL_ADDR, DEMOCFG_MFS_POOL_SIZE);
#endif
#endif

    /* This event will inform other tasks that the filesystem on USB was successfully installed */
    //_lwsem_create(&USB_Stick, 0);
    
    if (MQX_OK != _lwmsgq_init(usb_msgq, 20, USB_TASKQ_GRANM)) {
        // lwmsgq_init failed
        _task_block();
    }


   // ADK_adkInit();
#if 1
    PORT_PCR_REG(PORTB_BASE_PTR, 8) = PORT_PCR_MUX(0x01) | PORT_PCR_PE_MASK;
    GPIO_PDDR_REG(PTB_BASE_PTR) |= 1 << 8; /* PB8 as output */
    GPIO_PDOR_REG(PTB_BASE_PTR) &= ~(1 << 8); /* PB8 in high level */
    OS_Time_delay(1000);
    GPIO_PDOR_REG(PTB_BASE_PTR) |= 1 << 8; /* PB8 in high level */
    OS_Time_delay(100);
#endif

    if(!hci_transport_init()){
        printf("hci_transport_init fail\n");
        _task_block();
        }

    I2C_Init(); 
#ifdef BT_ADK2012_TEST    
    BSP_LED_BTN_INIT(1);
#endif

     for (;;) {
        /* Wait for event sent as a message */
        _lwmsgq_receive(&usb_msgq, (_mqx_max_type *) &msg, LWMSGQ_RECEIVE_BLOCK_ON_EMPTY, 0, 0);
         
        printf("msg.body is %d \n", msg.body);

        if (msg.body == USB_EVENT_ATTACH) {
          /* This event is not so important, because it does not inform about successfull USB stack enumeration */
          usb_task_state = USB_EVENT_ATTACH;
        } else if (msg.body == USB_EVENT_INTF )  { 
                
                if(hci_open () ) {
                	// I2C_Init(); 
                	//OS_Time_delay(3);
                    ADK_adkInit();
                    btStart();
                }
                else{
                    printf("hci_open: failed! \n");
                }
                usb_task_state = USB_EVENT_INTF;
                printf("usb task interfaced\n");
                //_lwsem_post(&USB_Stick);
       } else if (msg.body == USB_EVENT_DETACH) {  /* only acl interface detach send message and sco must detach before acl intf  */
           printf("usb task detached \n");
            if(usb_task_state == USB_EVENT_INTF) {
                printf("Call btDeinit \n");
                ADK_adkDeinit();   

                ADK_btSdpServiceDescriptorDel(sdpDescrADK);
                //sdpDescrADK[f] = dlci >> 1;
                ADK_btRfcommReleaseDlci( ((uint_8)(sdpDescrADK[magix_offset])) << 1);

                if(magix_offset) {
                sdpDescrADK[magix_offset] = MAGIX;
                magix_offset = 0;
                }

                hci_close();
            }
            usb_task_state = USB_EVENT_DETACH;
            //_lwsem_wait(&USB_Stick);
            /* Here, the device finishes its lifetime */            
        }
    }
    // will never run to this.
    // _lwmsgq_deinit(usb_msgq);  // 
    hci_transport_deinit();
}

/* EOF */
