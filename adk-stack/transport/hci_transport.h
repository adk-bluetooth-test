#ifndef _HCI_TRANSPORT_H_
#define _HCI_TRANSPORT_H_

//#define USE_SCO_TRANSFER

typedef struct {
    /* CLASS_CALL_STRUCT_PTR*/void * ccs;     /* class call struct of MSD instance */
    uint_8                body;    /* message body one of USB_EVENT_xxx as defined above */
} usb_msg_t;

#define USB_TASKQ_GRANM ((sizeof(usb_msg_t) - 1) / sizeof(_mqx_max_type) + 1)
extern _mqx_max_type  usb_msgq[20 * USB_TASKQ_GRANM * sizeof(_mqx_max_type)]; /* prepare message queue for 20 events */

typedef int HCI_TYPE ;
/*
enum HCI_TYPE {
	COMMAND_PACKET  = 1,
	DATA_PACKET_ACL = 2,
	DATA_PACKET_SCO = 3,
	EVENT_PACKET    = 4,
};*/
#define COMMAND_PACKET  1
#define DATA_PACKET_ACL  2
#define DATA_PACKET_SCO  3
#define EVENT_PACKET     4

/* usb */
#define USB_EVENT_ATTACH    (1)
#define USB_EVENT_DETACH    (2)
#define USB_EVENT_INTF      (3)            /* default, usb ACL interface */
#define USB_EVENT_SCO_INTF      (4)    /* usb SCO interface */

/* serial */

BOOL hci_transport_init();
void hci_transport_deinit();
BOOL hci_open (void);
void hci_close (void);
BOOL hci_write (HCI_TYPE type, unsigned char * pbuff,unsigned int len);
BOOL hci_read (HCI_TYPE *ptype, unsigned char * pbuff,unsigned int *plen);

#endif

