/* 
  usb dongle host driver 
*/
#include "os_utils.h"

#include "hci_transport.h"

#include "usbh.h"

#include "usbdrv.h"

//#define _UHDRV_DEBUG_

// extern function of usbhci.c 
extern void acl_detached(void * usb_dev);
extern BOOL acl_attached(void * usb_dev);
extern void usb_dev_set_pipe_handle(void * h_pipe, 
				unsigned int event ,unsigned int  in_out);
extern /*PIPE_HANDLE*/void * usb_dev_get_pipe_handle(unsigned int event ,
                         unsigned int  in_out);
void sent_packet_signal_complete(unsigned int idx_start);

#ifdef USE_SCO_TRANSFER
extern BOOL sco_attached(void * usb_dev,int alt_setting);
extern void sco_detached(void * usb_dev,int alt_setting);
extern void sco_set_interfaced(USB_DEV_HANDLE usb_dev);
#endif

void dev_set_min_packet_size(unsigned int size);
unsigned int dev_get_min_packet_size();
uint32_t usbdev_event_set(uint32_t event_mask);
void  usbdev_set_inited();
    

// local function declare
static void usb_bt_class_init
   (
      /* [IN] device/descriptor/pipe handles */
      PIPE_BUNDLE_STRUCT_PTR  pbs_ptr,
      
      /* [IN] class-interface data pointer + key */
      CLASS_CALL_STRUCT_PTR   cls_call_ptr
   );

static void usb_host_bt_device_event
   (
      /* [IN] pointer to device instance */
      _usb_device_instance_handle      dev_handle,

      /* [IN] pointer to interface descriptor */
      _usb_interface_descriptor_handle intf_handle,

      /* [IN] code number for event causing callback */
      uint32_t           event_code
   );

#ifdef USE_SCO_TRANSFER
static USB_STATUS usb_class_sco_get_config_from_descriptors
(
    SCO_DEVICE_STRUCT_PTR sco_dev,_usb_interface_descriptor_handle intf_handle,
    int alt_setting
);
#endif


/* we don't use pointer for avoid the mem fragment */
static APP_DEVICE_STRUCT g_drv_dev = {NULL,NULL};

/* The granularity of message queue is one message. Its size is the multiplier of _mqx_max_type. Get that multiplier */
_mqx_max_type  usb_msgq[20 * USB_TASKQ_GRANM * sizeof(_mqx_max_type)]; /* prepare message queue for 20 events */
// extern to application to get msg

#define BLOCKED_EVENT                (1<<0)

//extern USB_DEVICE_STRUCT_PTR   gp_usb_device;

static BT_INTERFACE_STRUCT_PTR acl_anchor = NULL;
#ifdef USE_SCO_TRANSFER
static BT_INTERFACE_STRUCT_PTR sco_anchor = NULL;
#endif

// extern to host_common.c
#define  USB_CLASS_BLUETOOTH          0xE0
#define  USB_SUBCLASS_BLUETOOTH          1
#define  USB_PROTOCOL_BLUETOOTH          1
CLASS_MAP class_interface_map[] =
{
#ifdef USBCLASS_INC_HUB
   {
      usb_class_hub_init,
      sizeof(USB_HUB_CLASS_INTF_STRUCT),
      USB_CLASS_HUB,
      USB_SUBCLASS_HUB_NONE,
      USB_PROTOCOL_HUB_FS,
      0xFF, 0x00, 0x00
   },
#endif
   {
      usb_bt_class_init,
      sizeof(BT_INTERFACE_STRUCT),
      USB_CLASS_BLUETOOTH,
      USB_SUBCLASS_BLUETOOTH,
      USB_PROTOCOL_BLUETOOTH,
      0xFF, 0x0F, 0x0F
   },
   {
      NULL,
      0,
      0, 0, 0,
      0, 0, 0
   }
};


static USB_HOST_DRIVER_INFO DriverInfoTable[] =
{
#ifdef USBCLASS_INC_HUB
    /* USB 1.1 hub */
    {
        {0x00,0x00},                  /* Vendor ID per USB-IF             */
        {0x00,0x00},                  /* Product ID per manufacturer      */
        USB_CLASS_HUB,                /* Class code                       */
        USB_SUBCLASS_HUB_NONE,        /* Sub-Class code                   */
        USB_PROTOCOL_HUB_LS,          /* Protocol                         */
        0,                            /* Reserved                         */
        usb_host_hub_device_event     /* Application call back function   */
    },
#endif
    {
        {0x00, 0x00},                 /* Vendor ID per USB-IF             */
        {0x00, 0x00},                 /* Product ID per manufacturer      */
        USB_CLASS_BLUETOOTH,          /* Class code                       */
        USB_SUBCLASS_BLUETOOTH,       /* Sub-Class code                   */
        USB_PROTOCOL_BLUETOOTH,       /* Protocol                         */
        0,                            /* Reserved                         */
        usb_host_bt_device_event      /* Application call back function   */
    },

    {
        {0x00, 0x00},                 /* Vendor ID per USB-IF             */
        {0x00, 0x00},                 /* Product ID per manufacturer      */
        0,                            /* Class code                       */
        0,                            /* Sub-Class code                   */
        0,                            /* Protocol                         */
        0,                            /* Reserved                         */
        NULL                          /* Application call back function   */
    }
};


/*
uint_8 get_pipe_max_packet_size(PIPE_HANDLE h_pipe)
{
    if(h_pipe){
        return ((PIPE_STRUCT_PTR)h_pipe)->MAX_PACKET_SIZE;
    }
} */

static int usb_host_out_block_callback(  /* transfer for cmd , acl blocked out , sco blocked out */
    /* [IN] pointer to pipe */
    _usb_pipe_handle pipe_handle,
    
    /* [IN] user-defined parameter */
    pointer user_parm,
    
    /* [IN] buffer address */
    uchar_ptr buffer,
    
    /* [IN] length of data transferred */
    uint32_t buflen,
    
    /* [IN] status, hopefully USB_OK or USB_DONE */
    uint32_t status)
{
    //PIPE_STRUCT_PTR         pipe_ptr = (PIPE_STRUCT_PTR)pipe_handle;
    tr_user_t * tr_ptr   = (tr_user_t *)user_parm;
    OS_Event_handle         block_event =  (OS_Event_handle)tr_ptr->tr_private;
    PIPE_STRUCT_PTR     pipe_ptr = (PIPE_STRUCT_PTR) pipe_handle;

#ifdef _UHDRV_DEBUG_
    printf("block send status=%d, buff len %d\n",status,buflen);
#endif

#ifndef USE_REAL_TRANSFER_HANDLE
    tr_ptr->tr_internal_ptr = (void *)status;           /* In blocked transfer ,we use tr_internal_ptr save transfer status  */
#endif

    if(pipe_ptr->PIPETYPE != USB_ISOCHRONOUS_PIPE) {
        tr_ptr->transfered = buflen;
        OS_Event_set(block_event,BLOCKED_EVENT);
    }
    else {
        if(status == USB_OK) { /* next frame transfer or complete transfer */
            tr_ptr->transfered += buflen;
            tr_ptr->lens_num ++;

            if(tr_ptr->lens_num < tr_ptr->frames) {
                return tr_ptr->lens_ptr[tr_ptr->lens_num];
            }
        }
        OS_Event_set(block_event,BLOCKED_EVENT);
    }

    return TR_CALLBACK_COMPLETE;
}

static int usb_host_out_nonblock_callback(  /* acl nonblocked out , sco nonblocked out */
    /* [IN] pointer to pipe */
    _usb_pipe_handle pipe_handle,
    
    /* [IN] user-defined parameter */
    pointer user_parm,
    
    /* [IN] buffer address */
    uchar_ptr buffer,
    
    /* [IN] length of data transferred */
    uint32_t buflen,
    
    /* [IN] status, hopefully USB_OK or USB_DONE */
    uint32_t status)
{
    PIPE_STRUCT_PTR     pipe_ptr = (PIPE_STRUCT_PTR) pipe_handle;
    tr_user_t * tr_ptr   = (tr_user_t *)user_parm;

#ifdef _UHDRV_DEBUG_
   printf("unblock send status=%d, buff len %d\n",status,buflen);
#endif

    int idx_start = (unsigned int)tr_ptr->tr_private;
#ifndef USE_REAL_TRANSFER_HANDLE
    tr_ptr->tr_internal_ptr = (void *)status;
#else
    if ((status == USB_OK))
        (TR_STRUCT_PTR)tr_ptr->tr_internal_ptr->STATUS  = USB_STATUS_TRANSFER_COMPLETE;
    else
        (TR_STRUCT_PTR)tr_ptr->tr_internal_ptr->STATUS  = USB_STATUS_TRANSFER_FAILED;
#endif

    if(pipe_ptr->PIPETYPE != USB_ISOCHRONOUS_PIPE) {
        tr_ptr->transfered = buflen;
        sent_packet_signal_complete (idx_start);
    }
    else {
        if(status == USB_OK) {
            tr_ptr->transfered += buflen;
            tr_ptr->lens_num ++;

            if(tr_ptr->lens_num < tr_ptr->frames) {
                return tr_ptr->lens_ptr[tr_ptr->lens_num];   /* return next tr size */
            }
        }
        sent_packet_signal_complete (idx_start);
    }
    
    return TR_CALLBACK_COMPLETE;
}

#ifdef USE_REAL_TRANSFER_HANDLE
TR_STRUCT_PTR _usb_host_get_tr
   (
      /* [IN] the pipe handle */
      _usb_pipe_handle     pipe_handle,

      /* [IN] the transfer number */
      uint32_t              tr_number
   )
{ /* Body */
   PIPE_STRUCT_PTR            pipe_ptr;
   TR_STRUCT_PTR              tr_list_ptr;
   int find_flag = 0;

   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("_usb_host_get_transfer\n");
   #endif
      
   USB_lock();

   pipe_ptr = (PIPE_STRUCT_PTR)pipe_handle;
   if (pipe_ptr!= NULL) {
      tr_list_ptr = pipe_ptr->tr_list_ptr;
      if (tr_list_ptr != NULL) {
         do {
            if (tr_list_ptr->TR_INDEX == tr_number) {
                find_flag = 1;
               break;
            } /* Endif */
            tr_list_ptr = tr_list_ptr->NEXT;   
         } while (tr_list_ptr != pipe_ptr->tr_list_ptr);
      } /* Endif */
   } /* Endif */
   
   USB_unlock();

   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("_usb_host_get_transfer_status SUCCESSFUL\n");
   #endif

   if(find_flag)
       return tr_list_ptr;
   else 
       return NULL;
} /* Endbody */
#endif

/*  the transfer of event , acl nonblocked in  and sco nonblocked in */
static int usb_host_in_nonblock_callback(  
    /* [IN] pointer to pipe */
    _usb_pipe_handle pipe_handle,
    
    /* [IN] user-defined parameter */
    pointer user_parm,
    
    /* [IN] buffer address */
    uchar_ptr buffer,
    
    /* [IN] length of data transferred */
    uint32_t buflen,
    
    /* [IN] status, hopefully USB_OK or USB_DONE */
    uint32_t status)
{
    PIPE_STRUCT_PTR            pipe_ptr = (PIPE_STRUCT_PTR)pipe_handle;
    tr_user_t * tr_ptr   = (tr_user_t *)user_parm;

#ifdef _UHDRV_DEBUG_
    printf("recv status=%d, buff len %d,pipetype %d\n",status,buflen,pipe_ptr->PIPETYPE);
#endif
    unsigned int event = (unsigned int )tr_ptr->tr_private;
    uint32_t  event_mask;

    ASSERT(event == EVENT_BULK || event == EVENT_ISOCH || event == EVENT_INTERRUPT);
    event_mask = (1 << event);

#ifndef USE_REAL_TRANSFER_HANDLE
    tr_ptr->tr_internal_ptr = (void *)status;
    if(USB_OK != status)
        printf("in %d ,err:%d\n",event,status);
#else
    if ((status == USB_OK)  || ( (status == USBERR_TR_SHORT) && 
                   ((pipe_ptr->PIPETYPE == USB_BULK_PIPE) || (pipe_ptr->PIPETYPE == USB_INTERRUPT_PIPE ))))
        (TR_STRUCT_PTR)tr_ptr->tr_internal_ptr->STATUS  = USB_STATUS_TRANSFER_COMPLETE;
    else
         (TR_STRUCT_PTR)tr_ptr->tr_internal_ptr->STATUS  = USB_STATUS_TRANSFER_FAILED;
#endif

    if(pipe_ptr->PIPETYPE != USB_ISOCHRONOUS_PIPE) {
        tr_ptr->transfered = buflen;
        usbdev_event_set(event_mask);
    }
    else {
        if(status == USB_OK) {
            tr_ptr->transfered += buflen;
            tr_ptr->lens_num ++;

            if(tr_ptr->lens_num < tr_ptr->frames) {
                return tr_ptr->lens_ptr[tr_ptr->lens_num];   /* return next tr size */
            }
        }
        else {
            if (tr_ptr->transfered > 0)
                tr_ptr->tr_internal_ptr = (void *)(USB_OK);
        }
        usbdev_event_set(event_mask);
    }
    return TR_CALLBACK_COMPLETE;
}

void close_transfer(tr_user_t * h_tr_ptr)
{
    h_tr_ptr->frames = 0;
    h_tr_ptr->lens_num = 0;
    h_tr_ptr->lens_ptr = NULL;
    h_tr_ptr->buffs_ptr = NULL;

    h_tr_ptr->transfered = 0;
    h_tr_ptr->tr_private = NULL;

#ifndef USE_REAL_TRANSFER_HANDLE
    return;
#else
    TR_STRUCT_PTR tr_ptr = (TR_STRUCT_PTR)h_tr_ptr->tr_internal_ptr;
    tr_ptr->TR_INDEX = 0;
    tr_ptr->STATUS = USB_STATUS_IDLE;
#endif
}

BOOL transfer_completed(tr_user_t * h_tr_ptr)  /* we only call this function after unblock transfer */
{
    return FALSE;
}

BOOL get_transfer_status(tr_user_t * h_tr_ptr,unsigned int * transfered)
{
    *transfered = h_tr_ptr->transfered;
#ifndef USE_REAL_TRANSFER_HANDLE
    unsigned int status = (unsigned int)h_tr_ptr->tr_internal_ptr;
    if  (status == USB_OK)
        return TRUE;
#else
    if(((TR_STRUCT_PTR)(h_tr_ptr->tr_internal_ptr))->STATUS  == USB_STATUS_TRANSFER_COMPLETE)
        return TRUE;
#endif
    return FALSE;
}

BOOL  issue_cmd_transfer(
    /* pipe_handle_t h_pipe, */
    void *  handle,
    tr_user_t *                   h_tr_ptr,   /* already allocate h_tr_ptr before call this transfer */
    unsigned char *                     buff_ptr,
    uint32_t                 		buf_len)
{
    ACL_DEVICE_STRUCT_PTR   acl_pdev =  
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)handle)->pdev_classes[ACL_CLASS_INTF]);

    BT_INTERFACE_STRUCT_PTR  ais_ptr;
    CLASS_CALL_STRUCT_PTR      ccs_ptr = (CLASS_CALL_STRUCT_PTR)&(acl_pdev->class_intf);

    USB_SETUP                        req;
    USB_STATUS                      status = USBERR_NO_INTERFACE;
    OS_Event_handle		     block_event;

#ifdef _UHDRV_DEBUG_
    DEBUG_LOG_TRACE("issue_cmd_transfer\n");
#endif
   
    USB_lock();
    /* Validity checking */
    if (usb_host_class_intf_validate(ccs_ptr)) {
      ais_ptr =
         (BT_INTERFACE_STRUCT_PTR) ccs_ptr->class_intf_handle;
      status = usb_hostdev_validate(ais_ptr->G.dev_handle);
    } /* Endif */

    if (!status) {
      /* Setup the request */
        req.BMREQUESTTYPE = (REQ_TYPE_OUT | REQ_TYPE_CLASS);
        req.BREQUEST = 0;
        *(uint_16*)req.WVALUE = 0;
        *(uint_16*)req.WINDEX = 0;
        *(uint_16*)req.WLENGTH = HOST_TO_LE_SHORT(buf_len);

        if ((block_event = OS_Event_create(1)) == NULL) {  /* auto clear */
            printf("\ncreate block_event failed!\n");
            USB_unlock();
            return FALSE;
        }

        h_tr_ptr->tr_blocked = TR_BLOCKED;
        h_tr_ptr->tr_private = (void *)block_event;
        
        status = _usb_hostdev_cntrl_request(acl_pdev->dev_handle, &req, buff_ptr,  /* if want get a transfer num(tr_index) ,rewrite  _usb_hostdev_cntrl_request get tr_index*/
        usb_host_out_block_callback, (pointer)h_tr_ptr);
   } /* Endif */
   USB_unlock();

   if (status != USB_STATUS_TRANSFER_QUEUED) {
      #ifdef _UHDRV_DEBUG_
         DEBUG_LOG_TRACE("issue_cmd_transfer, FAILED\n");
      #endif
      USB_log_error(__FILE__,__LINE__,status);
      return FALSE;
   }
   else {
	if (MQX_OK != OS_Event_wait(block_event, BLOCKED_EVENT, FALSE, 0)) {
		printf("\n_lwevent_wait_ticks block_event failed.\n");
		//_task_block();
		OS_Event_destroy(block_event);
		return FALSE;
   	}
   }

   OS_Event_destroy(block_event);
   
   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("issue_cmd_transfer, SUCCESSFUL\n");
   #endif

   return TRUE;
}

BOOL issue_event_transfer(
    /* pipe_handle_t h_pipe */
    void *  handle,
    tr_user_t *                h_tr_ptr,
    unsigned char *          buff_ptr,
    uint32_t                 		buf_len)
{
    ACL_DEVICE_STRUCT_PTR   acl_pdev =  
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)handle)->pdev_classes[ACL_CLASS_INTF]);

    BT_INTERFACE_STRUCT_PTR        ais_ptr;
    CLASS_CALL_STRUCT_PTR      ccs_ptr = (CLASS_CALL_STRUCT_PTR)&(acl_pdev->class_intf);

    TR_INIT_PARAM_STRUCT         tr;
    //uint32_t tr_num;

    USB_STATUS                 status = USBERR_NO_INTERFACE;
    _usb_pipe_handle           pipe_handle;

   #ifdef _UHDRV_DEBUG_
        DEBUG_LOG_TRACE("issue_event_transfer\n");
   #endif
   
    USB_lock();
    if((pipe_handle = (_usb_pipe_handle)usb_dev_get_pipe_handle(EVENT_INTERRUPT,USB_RECV)) != NULL) {
        /* Validity checking */
        if (usb_host_class_intf_validate(ccs_ptr)) {
          ais_ptr =
             (BT_INTERFACE_STRUCT_PTR) ccs_ptr->class_intf_handle;
          status = usb_hostdev_validate(ais_ptr->G.dev_handle);
        } /* Endif */

        if (!status) {
            h_tr_ptr->tr_blocked = TR_UNBLOCKED;
            h_tr_ptr->tr_private = (void *)EVENT_INTERRUPT;
#ifdef USE_REAL_TRANSFER_HANDLE
            tr_num = usb_hostdev_tr_init(&tr, (tr_callback) usb_host_in_nonblock_callback, (pointer) h_tr_ptr);
#else
            usb_hostdev_tr_init(&tr, (tr_callback) usb_host_in_nonblock_callback, (pointer) h_tr_ptr);
#endif
            tr.G.RX_BUFFER = (uchar_ptr) buff_ptr;
            tr.G.RX_LENGTH = buf_len;
            //tr.G.FLAG      = (TR_PERIOD_KEEP);
            status = _usb_host_recv_data(ais_ptr->G.host_handle,pipe_handle, &tr);
        } /* Endif */
    }
    USB_unlock();

   if (status != USB_STATUS_TRANSFER_QUEUED) {
      #ifdef _UHDRV_DEBUG_
         DEBUG_LOG_TRACE("issue_event_transfer, FAILED\n");
      #endif
      USB_log_error(__FILE__,__LINE__,status);
      return FALSE;
   }
   else{
#ifdef USE_REAL_TRANSFER_HANDLE    
   	h_tr_ptr->tr_internal_ptr = _usb_host_get_tr(pipe_handle, tr_num);
#endif
   }
   
   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("issue_event_transfer, SUCCESSFUL\n");
   #endif

   return TRUE;
}

BOOL issue_acl_transfer(
    /* pipe_handle_t h_pipe, */
    void *  handle,
    tr_user_t *                h_tr_ptr,
    uint32_t flags,             /* block or nonblock and transfer in or out */
    /* Callback to application */
    //tr_callback               	callback,
    /* Callback parameter */
    pointer                   	cb_unblocked_param,
    unsigned char *             buff_ptr,
    uint32_t                 		buf_len)
{
    ACL_DEVICE_STRUCT_PTR   acl_pdev =  
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)handle)->pdev_classes[ACL_CLASS_INTF]);

    BT_INTERFACE_STRUCT_PTR  ais_ptr;
    CLASS_CALL_STRUCT_PTR      ccs_ptr = (CLASS_CALL_STRUCT_PTR)&(acl_pdev->class_intf);

    //USB_SETUP                        req;
    USB_STATUS                      status = USBERR_NO_INTERFACE;
    _usb_pipe_handle                pipe_handle;
    OS_Event_handle		     block_event;

    TR_INIT_PARAM_STRUCT           tr;
    //uint32_t tr_num;

    tr_callback               	callback;
    /* Callback parameter */
    // pointer                   	callback_param;

#ifdef _UHDRV_DEBUG_
        DEBUG_LOG_TRACE("issue_acl_transfer\n");
#endif
   
    USB_lock();
    /* Validity checking */
    if (usb_host_class_intf_validate(ccs_ptr)) {
      ais_ptr =
         (BT_INTERFACE_STRUCT_PTR) ccs_ptr->class_intf_handle;
      status = usb_hostdev_validate(ais_ptr->G.dev_handle);
    } /* Endif */

    if (!status) {
        if((pipe_handle = (_usb_pipe_handle)usb_dev_get_pipe_handle(EVENT_BULK,(flags & USB_IN_TRANSFER) ? USB_RECV : USB_SEND)) != NULL) {

            if(flags & USB_IN_TRANSFER) {  /* USB_IN_TRANSFER  always unblocked */
                callback = usb_host_in_nonblock_callback;
                h_tr_ptr->tr_blocked = TR_UNBLOCKED;
                h_tr_ptr->tr_private = (void *)EVENT_BULK;
            }
            else {
                if (flags & USB_NO_WAIT) {
                    callback = usb_host_out_nonblock_callback;
                    h_tr_ptr->tr_blocked = TR_UNBLOCKED;
                    h_tr_ptr->tr_private = cb_unblocked_param;  // set to index of ring packet index
                }
                else {
                    if ((block_event = OS_Event_create(1)) == NULL) {
                        printf("\ncreate block_event failed!\n");
                        USB_unlock();
                        return FALSE;
                    }
                    callback = usb_host_out_block_callback;
                    h_tr_ptr->tr_blocked = TR_BLOCKED;
                    h_tr_ptr->tr_private = (void *)block_event;
                 }
            }

#ifdef USE_REAL_TRANSFER_HANDLE
            tr_num = usb_hostdev_tr_init(&tr, (tr_callback) callback, (pointer) h_tr_ptr);
#else
            usb_hostdev_tr_init(&tr, (tr_callback) callback, (pointer) h_tr_ptr);
#endif
            // tr.G.RX_BUFFER = (uchar_ptr) buff_ptr;
            // tr.G.RX_LENGTH = buf_len;

            if (flags & USB_IN_TRANSFER) {
                //tr.G.FLAG      =  USB_SHORT_TRANSFER_OK; // bluk in,interrupt in always admit short transfer
                tr.G.RX_BUFFER = (uchar_ptr) buff_ptr;
                tr.G.RX_LENGTH = buf_len;
                status = _usb_host_recv_data (ais_ptr->G.host_handle, pipe_handle, &tr);
            } else {
                tr.G.TX_BUFFER = (uchar_ptr) buff_ptr;
                tr.G.TX_LENGTH = buf_len;
                status = _usb_host_send_data (ais_ptr->G.host_handle, pipe_handle, &tr);
            }
        } /* EndIf */
    }
    USB_unlock();

    if (status != USB_STATUS_TRANSFER_QUEUED) {
#ifdef _UHDRV_DEBUG_
    DEBUG_LOG_TRACE("issue_acl_transfer, FAILED\n");
#endif
    USB_log_error(__FILE__,__LINE__,status);
    printf("queue acl_transfer, FAILED %d\n",status);
    return FALSE;
    }
    else {
        if(flags & USB_NO_WAIT) {
#ifdef USE_REAL_TRANSFER_HANDLE
       	h_tr_ptr->tr_internal_ptr = _usb_host_get_tr(pipe_handle, tr_num);
#endif
        }
        else {  /* trace it at blocked transfer ! */
            if (MQX_OK != OS_Event_wait(block_event, BLOCKED_EVENT, FALSE, 0)) {
                printf("\n_lwevent_wait_ticks block_event failed.\n");
                OS_Event_destroy(block_event);
                return FALSE;
            }
            OS_Event_destroy(block_event);
        }
    }
   
   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("issue_acl_transfer, SUCCESSFUL\n");
   #endif

   return TRUE;
}

#ifdef USE_SCO_TRANSFER
BOOL issue_sco_transfer(
    /* pipe_handle_t h_pipe, */
    void *  handle,
    tr_user_t *                h_tr_ptr,
    uint32_t flags,             /* block or nonblock */
    /* Callback parameter */
    pointer                   	cb_unblocked_param,
    uint32_t frames,          /* number of frames in transfer */
    unsigned int * lens_ptr,  /*  array of lengths  (one entry per frame) */
    unsigned char *            buff_ptr             /* data buffer */
)
{
    SCO_DEVICE_STRUCT_PTR   sco_pdev =  
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)handle)->pdev_classes[SCO_CLASS_INTF]);

    BT_INTERFACE_STRUCT_PTR  ais_ptr;
    CLASS_CALL_STRUCT_PTR      ccs_ptr = (CLASS_CALL_STRUCT_PTR)&(sco_pdev->class_intf);

    USB_STATUS                      status = USBERR_NO_INTERFACE;
    _usb_pipe_handle                pipe_handle;
    OS_Event_handle		     block_event;

    TR_INIT_PARAM_STRUCT           tr;
    //uint32_t tr_num;

    tr_callback               	callback;
    /* Callback parameter */
    // pointer                   	callback_param;

#ifdef _UHDRV_DEBUG_
        DEBUG_LOG_TRACE("issue_acl_transfer\n");
#endif
   
    USB_lock();
    /* Validity checking */
    if (usb_host_class_intf_validate(ccs_ptr)) {
      ais_ptr =
         (BT_INTERFACE_STRUCT_PTR) ccs_ptr->class_intf_handle;
      status = usb_hostdev_validate(ais_ptr->G.dev_handle);
    } /* Endif */

    if (!status) {
        if((pipe_handle = (_usb_pipe_handle)usb_dev_get_pipe_handle(EVENT_ISOCH,(flags & USB_IN_TRANSFER) ? USB_RECV : USB_SEND)) != NULL) {
            h_tr_ptr->frames = frames;
            h_tr_ptr->lens_ptr = lens_ptr;
            h_tr_ptr->buffs_ptr = buff_ptr;
            h_tr_ptr->transfered = 0;
            
            if(flags & USB_IN_TRANSFER) {  /* USB_IN_TRANSFER  always unblocked */
                callback = usb_host_in_nonblock_callback;

                h_tr_ptr->tr_blocked = TR_UNBLOCKED;
                h_tr_ptr->tr_private = (void *)EVENT_ISOCH;
            }
            else {
                if (flags & USB_NO_WAIT) {
                    callback = usb_host_out_nonblock_callback;
                    h_tr_ptr->tr_blocked = TR_UNBLOCKED;
                    h_tr_ptr->tr_private = cb_unblocked_param;  // set to index of ring packet index
                }
                else {
                    if ((block_event = OS_Event_create(1)) == NULL) {
                        printf("\ncreate block_event failed!\n");
                        USB_unlock();
                        return FALSE;
                    }
                    callback = usb_host_out_block_callback;
                    h_tr_ptr->tr_blocked = TR_BLOCKED;
                    h_tr_ptr->tr_private = (void *)block_event;
                 }
            }

#ifdef USE_REAL_TRANSFER_HANDLE
            tr_num = usb_hostdev_tr_init(&tr, (tr_callback) callback, (pointer) h_tr_ptr);
#else
            usb_hostdev_tr_init(&tr, (tr_callback) callback, (pointer) h_tr_ptr);
#endif
            // tr.G.RX_BUFFER = (uchar_ptr) buff_ptr;
            // tr.G.RX_LENGTH = buf_len;

            if (flags & USB_IN_TRANSFER) {
                //tr.G.FLAG      =  USB_SHORT_TRANSFER_OK; // bluk in,interrupt in always admit short transfer
                tr.G.RX_BUFFER = (uchar_ptr) buff_ptr;
                tr.G.RX_LENGTH = lens_ptr[0];
                status = _usb_host_recv_data (ais_ptr->G.host_handle, pipe_handle, &tr);
            } else {
                tr.G.TX_BUFFER = (uchar_ptr) buff_ptr;
                tr.G.TX_LENGTH = lens_ptr[0];
                status = _usb_host_send_data (ais_ptr->G.host_handle, pipe_handle, &tr);
            }
        } /* EndIf */
    }
    USB_unlock();

    if (status != USB_STATUS_TRANSFER_QUEUED) {
#ifdef _UHDRV_DEBUG_
    DEBUG_LOG_TRACE("issue_acl_transfer, FAILED\n");
#endif
    USB_log_error(__FILE__,__LINE__,status);
    return FALSE;
    }
    else {
        if(flags & USB_NO_WAIT) {
#ifdef USE_REAL_TRANSFER_HANDLE
       	h_tr_ptr->tr_internal_ptr = _usb_host_get_tr(pipe_handle, tr_num);
#endif
        }
        else {  /* trace it at blocked transfer ! */
            if (MQX_OK != OS_Event_wait(block_event, BLOCKED_EVENT, FALSE, 0)) {
                printf("\n_lwevent_wait_ticks block_event failed.\n");
                OS_Event_destroy(block_event);
                return FALSE;
            }
            OS_Event_destroy(block_event);
        }
    }
   
   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("issue_acl_transfer, SUCCESSFUL\n");
   #endif

   return TRUE;

}
#endif

static void usb_bt_class_deinit 
    ( _usb_class_intf_handle  if_handle )
{
    BT_INTERFACE_STRUCT_PTR if_ptr = (BT_INTERFACE_STRUCT_PTR)if_handle;
    INTERFACE_DESCRIPTOR_PTR      intf_desc;
    intf_desc =  (INTERFACE_DESCRIPTOR_PTR)  if_ptr->G.intf_handle;
    /*
    if (intf_desc->bInterfaceNumber == ACL_CLASS_INTF) {
    }

    if (intf_desc->bInterfaceNumber == SCO_CLASS_INTF) {
    }
    */
    // todo
    
    return;
}

void usb_bt_class_init
   (
      /* [IN] device/descriptor/pipe handles */
      PIPE_BUNDLE_STRUCT_PTR  pbs_ptr,
      
      /* [IN] class-interface data pointer + key */
      CLASS_CALL_STRUCT_PTR   cls_call_ptr
   )
{ /* Body */

	BT_INTERFACE_STRUCT_PTR  bt_intf;   /* judge with intf_num for acl and soc */
   INTERFACE_DESCRIPTOR_PTR      intf_desc;
   _usb_pipe_handle h_pipe;
    pointer                 anchor_ptr;

   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("usb_acl_class_init\n");
   #endif

   intf_desc = (INTERFACE_DESCRIPTOR_PTR)pbs_ptr->intf_handle;

   if (intf_desc->bInterfaceNumber == ACL_CLASS_INTF)
        anchor_ptr = &acl_anchor;
#ifdef USE_SCO_TRANSFER
   else
        anchor_ptr = &sco_anchor;
#endif
  
   //printf("liutest usb_acl_class_init+\n");
   /* Pointer validity-checking, clear memory, init header */
   bt_intf = (BT_INTERFACE_STRUCT_PTR)cls_call_ptr->class_intf_handle;
   if (USB_OK != usb_host_class_intf_init (pbs_ptr, (pointer)bt_intf, (pointer)anchor_ptr, NULL)) {  /* todo add deinit function ,call deinit class at detach */
      #ifdef _UHDRV_DEBUG_
         DEBUG_LOG_TRACE("usb_acl_class_init, error class init\n");
      #endif
      return;
   }
   
#if 0
    if ((acl_intf->block_event = OS_Event_create(1)) == NULL) {
        printf("\ncreate block_event failed!\n");
        return;
    }
#endif
    
    USB_lock();
    if (intf_desc->bInterfaceNumber == ACL_CLASS_INTF) {

        cls_call_ptr->code_key = 0;
        cls_call_ptr->code_key = usb_host_class_intf_validate(cls_call_ptr);

        if (NULL == 
            (h_pipe = usb_hostdev_get_pipe_handle
            (pbs_ptr, USB_BULK_PIPE, USB_SEND)) )
                goto Bad_Exit;
        usb_dev_set_pipe_handle((PIPE_HANDLE)h_pipe, EVENT_BULK ,USB_SEND);

        // memset 0 already in usb_host_class_intf_init
        if (NULL ==
            (h_pipe = usb_hostdev_get_pipe_handle
            (pbs_ptr, USB_BULK_PIPE, USB_RECV) ))
                goto Bad_Exit;
        usb_dev_set_pipe_handle((PIPE_HANDLE)h_pipe, EVENT_BULK ,USB_RECV);


        if (NULL ==
            (h_pipe = usb_hostdev_get_pipe_handle
            (pbs_ptr, USB_INTERRUPT_PIPE, USB_RECV) ))
                goto Bad_Exit;
        usb_dev_set_pipe_handle((PIPE_HANDLE)h_pipe, EVENT_INTERRUPT ,USB_SEND);

        /* Store application handle */
        //bt_intf->APP = cls_call_ptr;   /* CLASS_CALL_STRUCT_PTR must place at the first of application struct */
    }
#ifdef USE_SCO_TRANSFER
    else if ((intf_desc->bInterfaceNumber == SCO_CLASS_INTF)) {
        if (intf_desc->bAlternateSetting != /* SCO_DEFAULT_ALT_SETTING*/0) {
        if (NULL == 
            (h_pipe = usb_hostdev_get_pipe_handle
            (pbs_ptr, USB_ISOCHRONOUS_PIPE, USB_SEND)))
                goto Bad_Exit;
        usb_dev_set_pipe_handle((PIPE_HANDLE)h_pipe, EVENT_ISOCH ,USB_SEND);

        // memset 0 already in usb_host_class_intf_init
        if (NULL ==
            (h_pipe = usb_hostdev_get_pipe_handle
            (pbs_ptr, USB_ISOCHRONOUS_PIPE, USB_RECV) ))
                goto Bad_Exit;
        usb_dev_set_pipe_handle((PIPE_HANDLE)h_pipe, EVENT_ISOCH ,USB_RECV);
            }
    }
#endif

    /* Store application handle */
    bt_intf->APP = cls_call_ptr;   /* CLASS_CALL_STRUCT_PTR must place at the first of application struct */

    USB_unlock ();

#ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("usb_acl_class_init, SUCCESSFUL\n");
#endif
   // printf("liutest usb_bt_class_init-\n");
   
   return;  /* Good exit, interface struct initialized */

Bad_Exit:
   USB_mem_zero (bt_intf,sizeof(BT_INTERFACE_STRUCT_PTR));
   cls_call_ptr->class_intf_handle = NULL;
   cls_call_ptr->code_key = 0;
   USB_unlock ();
   #ifdef _UHDRV_DEBUG_
      DEBUG_LOG_TRACE("usb_acl_class_init, bad exit\n");
   #endif
   
} /* Endbody */

static USB_STATUS usb_class_bt_get_app
   (
      /* [IN] handle of device */
      _usb_device_instance_handle dev_ptr,

      /* [IN] pointer to interface descriptor */
      _usb_interface_descriptor_handle intf_ptr,

      /* [OUT] pointer to CLASS_CALL_STRUCT to be filled in */
      CLASS_CALL_STRUCT_PTR   _PTR_ ccs_ptr
   )
{
    USB_STATUS                    error;
    GENERAL_CLASS_PTR             parser;
    // ACL_DEVICE_STRUCT_PTR   _PTR_  acl_dev_ptr;
    // SCO_DEVICE_STRUCT_PTR   _PTR_  sco_dev_ptr;
     GENERAL_CLASS_PTR   bt_anchor;

    USB_lock();
 
    error = usb_hostdev_validate (dev_ptr);
    if (error != USB_OK) {
        USB_unlock();
        #ifdef _UHDRV_DEBUG_
            DEBUG_LOG_TRACE("usb_class_hub_get_app, FAILED\n");
        #endif
        return USB_log_error(__FILE__,__LINE__,error);
    } /* EndIf */

   if (((INTERFACE_DESCRIPTOR_PTR)intf_ptr)->bInterfaceNumber == ACL_CLASS_INTF)
        bt_anchor = (GENERAL_CLASS_PTR)acl_anchor;
#ifdef USE_SCO_TRANSFER
   else
        bt_anchor = (GENERAL_CLASS_PTR)sco_anchor;
#endif
    
    for (parser = (GENERAL_CLASS_PTR) bt_anchor; parser != NULL; parser = parser->next) {
        if (parser->dev_handle == dev_ptr && parser->intf_handle == intf_ptr)
            break;
    }
    
    if (parser != NULL) {
        BT_INTERFACE_STRUCT_PTR bt_intf = (BT_INTERFACE_STRUCT_PTR) parser;
        * ccs_ptr = bt_intf->APP;
    }
    else {
        USB_unlock();
        #ifdef _UHDRV_DEBUG_
            DEBUG_LOG_TRACE("usb_class_mass_get_app, not found\n");
        #endif
        return USB_log_error(__FILE__,__LINE__,USBERR_NOT_FOUND);
    }
    
    USB_unlock();
    #ifdef _UHDRV_DEBUG_
        DEBUG_LOG_TRACE("usb_class_mass_get_app, SUCCESSFUL\n");
    #endif
    return USB_OK;
    
}

static USB_STATUS usb_class_acl_get_config_from_descriptors
(
    ACL_DEVICE_STRUCT_PTR acl_dev
)
{
   // USB_DEVICE_STRUCT_PTR usb_device = container_of(app_acl_dev, ACL_DEVICE_STRUCT, usb_acl_handle);
   //USB_DEVICE_STRUCT_PTR usb_device = gp_usb_device;
    
    int index;
    _usb_device_instance_handle      dev_handle = acl_dev->dev_handle;
    _usb_interface_descriptor_handle intf_handle = acl_dev->intf_handle;
    INTERFACE_DESCRIPTOR_PTR      intf_ptr = (INTERFACE_DESCRIPTOR_PTR)intf_handle;
      
    USB_STATUS                 status;
    ENDPOINT_DESCRIPTOR_PTR                 endp;
    uint_16 max_packet_size = 0;

    //unsigned int min_packet_size;

#ifdef _UHDRV_DEBUG_
    DEBUG_LOG_TRACE("usb_class_acl_get_descriptors\n");
#endif

   status = USB_OK;
   for (index = 0; index < intf_ptr->bNumEndpoints; index++) {

        /* collect all interface functional descriptors */
         if (USB_OK != _usb_hostdev_get_descriptor( 
                                                           dev_handle,
                                                           intf_handle,
                                                           /* Functional descriptor 
                                                            * for interface */
                                                           USB_DESC_TYPE_EP,  
                                                           /* Index of descriptor */
                                                           index + 1,      
                                                           /* Index of interface alternate */
                                                           intf_ptr->bAlternateSetting, 
                                                           (pointer _PTR_)&endp)
                                                          ) {
            //*endp = NULL;
            status = USBERR_INIT_FAILED;
            break;
         }

        max_packet_size =  (uint_16)(SHORT_UNALIGNED_LE_TO_HOST(endp->wMaxPacketSize) & PACKET_SIZE_MASK);
        if (max_packet_size > dev_get_min_packet_size())
            dev_set_min_packet_size( max_packet_size);


       switch (endp->bmAttributes & EP_TYPE_MASK) {
            //case ISOCH_ENDPOINT:
                // break;
          case BULK_ENDPOINT:
             if(endp->bEndpointAddress & IN_ENDPOINT)
                acl_dev->endp_acl_in = endp;
             else
                acl_dev->endp_acl_out = endp;
             break;
          case IRRPT_ENDPOINT:
                acl_dev->endp_events = endp;
             break;
          default:
             break;
       } /* EndSwitch */
    }

    if ((acl_dev->endp_acl_in == NULL) || (acl_dev->endp_acl_out == NULL) || (acl_dev->endp_events == NULL))
        status = USBERR_INIT_FAILED;

     return  status;
}

/*
 detach :
   1. detach event call
   2. list_close_pipe        ( a. control pipe b. other pipe )
           tr canncel call
           _usb_host_close_pipe 
*/     
void usb_host_bt_device_event     /* USB_HOST_DRIVER_INFO  event callback !!! */
   (
      /* [IN] pointer to device instance */
      _usb_device_instance_handle      dev_handle,

      /* [IN] pointer to interface descriptor */
      _usb_interface_descriptor_handle intf_handle,

      /* [IN] code number for event causing callback */
      uint32_t           event_code
   )
{
   usb_msg_t                  msg;
   ACL_DEVICE_STRUCT_PTR  acl_pdev = NULL;
#ifdef USE_SCO_TRANSFER    
   SCO_DEVICE_STRUCT_PTR  sco_pdev = NULL;
#endif   

   uint_8  inft_num = ((INTERFACE_DESCRIPTOR_PTR)intf_handle)->bInterfaceNumber;
   ASSERT((inft_num == ACL_CLASS_INTF) || (inft_num == SCO_CLASS_INTF));

   switch (event_code) {
      case USB_CONFIG_EVENT:
         /* Drop through into attach, same processing */
      case USB_ATTACH_EVENT:
        if(g_drv_dev.pdev_classes[inft_num] == NULL) {
            if (inft_num == ACL_CLASS_INTF)
               g_drv_dev.pdev_classes[inft_num] = (DEVICE_STRUCT_PTR)os_mem_alloc_zero(
                    sizeof(ACL_DEVICE_STRUCT));
#ifdef USE_SCO_TRANSFER            
            else
                g_drv_dev.pdev_classes[inft_num] = (DEVICE_STRUCT_PTR)os_mem_alloc_zero(
                sizeof(SCO_DEVICE_STRUCT));
#endif
            /*g_drv_dev.pdev_classes[inft_num] = (DEVICE_STRUCT_PTR)os_mem_alloc_zero(
                (inft_num == ACL_CLASS_INTF) ?  sizeof(ACL_DEVICE_STRUCT) : sizeof(SCO_DEVICE_STRUCT));
                */
            if(g_drv_dev.pdev_classes[inft_num] == NULL) {
                printf("alloc %d DEVICE_STRUCT failed\n", inft_num);
                break;
            }
            g_drv_dev.pdev_classes[inft_num]->dev_handle = dev_handle;
            g_drv_dev.pdev_classes[inft_num]->intf_handle = intf_handle;
        }

        if(inft_num == ACL_CLASS_INTF) {
            /* Here, the device starts its lifetime */
            if (!acl_attached((void *)&g_drv_dev)) {
                printf("acl_attached failed\n");
                os_mem_free(g_drv_dev.pdev_classes[ACL_CLASS_INTF]);
                break;
            }

            acl_pdev = (ACL_DEVICE_STRUCT_PTR)(g_drv_dev.pdev_classes[ACL_CLASS_INTF]);

            if (USB_OK != _usb_hostdev_select_interface(dev_handle, intf_handle,
                    &g_drv_dev.pdev_classes[ACL_CLASS_INTF]->class_intf)) {
                    printf("select ACL intf err\n");
                    acl_detached((void *)&g_drv_dev);
                    os_mem_free(g_drv_dev.pdev_classes[ACL_CLASS_INTF]);
            }
        }
#ifdef USE_SCO_TRANSFER        
        else /* if (inft_num == SCO_CLASS_INTF)*/ {
            uint_8 alt_setting = ((INTERFACE_DESCRIPTOR_PTR)intf_handle)->bAlternateSetting;
            sco_pdev = (SCO_DEVICE_STRUCT_PTR)(g_drv_dev.pdev_classes[SCO_CLASS_INTF]);
            ASSERT(sco_pdev && (alt_setting >= 0) && (alt_setting < NUM_SCO_ALTSETTINGS));

            sco_pdev->interface_sco[alt_setting] = (INTERFACE_DESCRIPTOR_PTR)intf_handle;

            if(alt_setting == 0) {  /* no active sco at attach */
                 if (!sco_attached((void *)&g_drv_dev,alt_setting)) {  /* only attach at altsetting 0 */
                    printf("sco_attached failed\n");
                    os_mem_free(g_drv_dev.pdev_classes[SCO_CLASS_INTF]);
                    break;
                 }
                 /* select interface at  open_connection
                if (USB_OK != _usb_hostdev_select_interface(dev_handle, intf_handle,
                        &g_drv_dev.pdev_classes[SCO_CLASS_INTF]->class_intf)) {
                        sco_detached((void *)&g_drv_dev,alt_setting);
                        os_mem_free(g_drv_dev.pdev_classes[SCO_CLASS_INTF]);
                }*/
            }
            else {
                // get all alt_settings endp discript
                if(usb_class_sco_get_config_from_descriptors(sco_pdev,intf_handle,alt_setting) != USB_OK) {
                    printf("get sco interface alt setting %d, ep desciprts failed\n",alt_setting);
                }
            }
            
        }
#endif        

        break;

      case USB_INTF_EVENT:
        if(inft_num == ACL_CLASS_INTF) {
             if (USB_OK != usb_class_bt_get_app(dev_handle, intf_handle, ((CLASS_CALL_STRUCT_PTR _PTR_)&acl_pdev))){
                printf("get acl cls app err\n");
                break;
             }

            if(USB_OK != usb_class_acl_get_config_from_descriptors(acl_pdev)) {
                acl_detached((void *)&g_drv_dev);
                os_mem_free(g_drv_dev.pdev_classes[ACL_CLASS_INTF]);
                printf("get descirptors failed\n");
                break;
            }
       
             usbdev_set_inited();
             msg.ccs = (CLASS_CALL_STRUCT_PTR)(acl_pdev);
             msg.body = USB_EVENT_INTF;
             if (LWMSGQ_FULL == _lwmsgq_send(usb_msgq, (uint32_t *) &msg, 0)) {  /* todo ,set msg receiver */
                printf("Could not inform USB task about device interfaced\n");
             }
             else printf("inform USB task about device interfaced\n");
         }
#ifdef USE_SCO_TRANSFER        
        else {
            printf("SCO interfaced alt setting %d\n",((INTERFACE_DESCRIPTOR_PTR)intf_handle)->bAlternateSetting);
            if(((INTERFACE_DESCRIPTOR_PTR)intf_handle)->bAlternateSetting != 0) {
                msg.ccs = (CLASS_CALL_STRUCT_PTR)(sco_pdev);
                msg.body = USB_EVENT_SCO_INTF;
                if (LWMSGQ_FULL == _lwmsgq_send(usb_msgq, (uint32_t *) &msg, 0)) {  /* todo ,set msg receiver */
                    printf("Could not inform USB task about device interfaced\n");
                }
            }
            sco_set_interfaced((void *)&g_drv_dev);
        }
#endif        
        break;
      case USB_DETACH_EVENT:
        if(inft_num == ACL_CLASS_INTF) {
             if (USB_OK != usb_class_bt_get_app(dev_handle, intf_handle, (CLASS_CALL_STRUCT_PTR _PTR_)&acl_pdev)) {
                printf("get acl cls app err\n");
                break;
             }
             acl_detached((void *)&g_drv_dev);

             ASSERT( g_drv_dev.pdev_classes[SCO_CLASS_INTF] == NULL);  /* sco must detach before acl intf !*/
          
             msg.ccs =  /*(CLASS_CALL_STRUCT_PTR)(acl_pdev)*/NULL;  /* dont send acl_pdev to app, because below will release it */
             msg.body = USB_EVENT_DETACH;
             if (LWMSGQ_FULL == _lwmsgq_send(usb_msgq, (uint32_t *) &msg, 0)) {
                printf("Could not inform USB task about device detached\n");
             }
             else printf("inform USB task about device detached\n");
         }
#ifdef USE_SCO_TRANSFER        
        else {
            printf("SCO detached alt setting %d\n",((INTERFACE_DESCRIPTOR_PTR)intf_handle)->bAlternateSetting);
            if (USB_OK != usb_class_bt_get_app(dev_handle, intf_handle, (CLASS_CALL_STRUCT_PTR _PTR_)&sco_pdev))
                break;
             sco_detached((void *)&g_drv_dev);

        }
#endif

        if(g_drv_dev.pdev_classes[inft_num]) {
            os_mem_free(g_drv_dev.pdev_classes[inft_num]);
            g_drv_dev.pdev_classes[inft_num] = NULL;
        }

         break;

      default:
         break;
   } 
} 

/* open all the interface (open the endpoint ) */
int open_acl_interface(void * usb_dev)
{
#if 0
    ACL_DEVICE_STRUCT_PTR acl_dev = 
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[ACL_CLASS_INTF]);

    if (USB_OK != _usb_hostdev_select_interface(acl_dev->dev_handle, 
           acl_dev->intf_handle, &acl_dev->class_intf)) {
        return -1;        
    }
#endif
    return 0;
}

void close_acl_interface(void * usb_dev)
{
#if 0
    /*
    DEV_INSTANCE dev_inst = (DEV_INSTANCE)dev_handle;
    usb_dev_list_detach_device((pointer)(dev_inst->host), 0, 0);
    */
    ACL_DEVICE_STRUCT_PTR acl_dev = 
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[ACL_CLASS_INTF]);

    usb_hostdev_delete_interface((acl_dev->dev_handle, acl_dev->intf_handle);
#endif
}

#ifdef USE_SCO_TRANSFER
int open_sco_interface(void * usb_dev, int altsetting)
{
    SCO_DEVICE_STRUCT_PTR sco_dev = 
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[SCO_CLASS_INTF]);

       if (USB_OK != _usb_hostdev_select_interface(sco_dev->dev_handle, (_usb_interface_descriptor_handle)sco_dev->interface_sco[altsetting], &sco_dev->class_intf)) {  /* todo move to open_connect */
            return -1;
       }

       // wait interfaced event
       
       return 0;
}

int close_sco_interface(void * usb_dev,int altsetting)
{
    SCO_DEVICE_STRUCT_PTR sco_dev = 
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[SCO_CLASS_INTF]);

    usb_hostdev_delete_interface(sco_dev->dev_handle, (_usb_interface_descriptor_handle)sco_dev->interface_sco[altsetting]);
    return 0;
}

unsigned short get_sco_endp_max_packetsize(void * usb_dev,int idx, int in_out)
{
    SCO_DEVICE_STRUCT_PTR sco_dev = 
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[SCO_CLASS_INTF]);

    if(in_out == USB_SEND) {
        return  (uint_16)(SHORT_UNALIGNED_LE_TO_HOST(sco_dev->endp_sco_out[idx]->wMaxPacketSize) & PACKET_SIZE_MASK);
    }
    else {
        return  (uint_16)(SHORT_UNALIGNED_LE_TO_HOST(sco_dev->endp_sco_in[idx]->wMaxPacketSize) & PACKET_SIZE_MASK);
    }
}

static USB_STATUS usb_class_sco_get_config_from_descriptors (
    SCO_DEVICE_STRUCT_PTR sco_dev,
    _usb_interface_descriptor_handle intf_handle,
    int alt_setting
)
{
   // USB_DEVICE_STRUCT_PTR usb_device = container_of(app_acl_dev, ACL_DEVICE_STRUCT, usb_acl_handle);
   //USB_DEVICE_STRUCT_PTR usb_device = gp_usb_device;
    
    int index;
    _usb_device_instance_handle      dev_handle = sco_dev->dev_handle;
    INTERFACE_DESCRIPTOR_PTR      intf_ptr = (INTERFACE_DESCRIPTOR_PTR)intf_handle;

    USB_STATUS                 status;
    ENDPOINT_DESCRIPTOR_PTR                 endp;

    //unsigned int min_packet_size;

#ifdef _UHDRV_DEBUG_
    DEBUG_LOG_TRACE("usb_class_sco_get_descriptors\n");
#endif

   status = USB_OK;
   for (index = 0; index < intf_ptr->bNumEndpoints; index++) {

        /* collect all interface functional descriptors */
         if (USB_OK != _usb_hostdev_get_descriptor( 
                                                           dev_handle,
                                                           intf_handle,
                                                           /* Functional descriptor 
                                                            * for interface */
                                                           USB_DESC_TYPE_EP,  
                                                           /* Index of descriptor */
                                                           index + 1,      
                                                           /* Index of interface alternate */
                                                           intf_ptr->bAlternateSetting, 
                                                           (pointer _PTR_)&endp)
                                                          ) {
            //*endp = NULL;
            status = USBERR_INIT_FAILED;
            break;
         }

       // ASSERT( alt_setting > 0);
       switch (endp->bmAttributes & EP_TYPE_MASK) {
            case ISOCH_ENDPOINT:
             if(endp->bEndpointAddress & IN_ENDPOINT)
                sco_dev->endp_sco_in[alt_setting] = endp;
             else
                sco_dev->endp_sco_out[alt_setting] = endp;
             break;
          case BULK_ENDPOINT:
          case IRRPT_ENDPOINT:
          default:
            ASSERT(0);
             break;
       } /* EndSwitch */
    }

    if ((sco_dev->endp_sco_in[alt_setting]  == NULL) || (sco_dev->endp_sco_out[alt_setting] == NULL))
        status = USBERR_INIT_FAILED;

     return  status;
}
#endif

BOOL usb_drv_init()
{
    USB_STATUS           error = USB_OK;
    _usb_host_handle     host_handle;
    USB_lock();

    _int_install_unexpected_isr();

   if (MQX_OK != _usb_host_driver_install(USBCFG_DEFAULT_HOST_CONTROLLER)) {
      printf("\n Driver installation failed");
      return FALSE;// OS_Task_suspend(0);
   }

    error = _usb_host_init(USBCFG_DEFAULT_HOST_CONTROLLER, &host_handle);
    if (error == USB_OK) {
        error = _usb_host_driver_info_register(host_handle, (pointer)DriverInfoTable);
        if (error == USB_OK) {
            error = _usb_host_register_service(host_handle, USB_SERVICE_HOST_RESUME, NULL);
        }
    }
    USB_unlock();

    if (error != USB_OK) {
		printf("usb host init failed\n");
		return FALSE; //OS_Task_suspend(0);
    }
    
    return TRUE;

}



