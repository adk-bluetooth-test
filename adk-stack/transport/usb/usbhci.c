/* usb bt dongle hci driver */

#include "usbh.h"

#include "usbhci.h"

#include "usbdrv.h"

#ifdef DEBUG_COMPLETE_PKT_NUM
unsigned int complete_packets_num = 0;
#endif

#ifdef USE_WRITE_PACKET_BUFFER
static inline int ring_buffer_lock(ring_buffer_t * rbuff)
{
    return OS_Mutex_lock(rbuff->r_lock);
}

static inline int ring_buffer_unlock(ring_buffer_t * rbuff)
{
    return OS_Mutex_unlock(rbuff->r_lock);
}

static inline void ring_buff_zero(ring_buffer_t * rbuff)
{
    rbuff->free_space = RING_BUFFER_END;
    rbuff->buffer = NULL;
    rbuff->idx_current = 0;
    rbuff->packets = 0;
    rbuff->next_desc = 0;
    rbuff->last_returned_desc = 0;
    rbuff->idx_complete = 0;
    rbuff->r_lock = NULL;
#if  1
    for(unsigned int i=0; i<MAX_ASYNC_PACKETS; i++) {
        rbuff->desc[i].returned = TRUE;
        rbuff->desc[i].idx_start = 0xFFFFFFFF;
    }
#endif
}
#endif

static inline int ring_buff_init(ring_buffer_t * rbuff)
{
#ifdef USE_WRITE_PACKET_BUFFER
    ring_buff_zero(rbuff);
    if (! rbuff->buffer) {
        rbuff->buffer = os_mem_alloc(RING_BUFFER_SIZE);
        if (! rbuff->buffer) {
            return -1;
        }
    }

    if((rbuff->r_lock = OS_Mutex_create()) == NULL)
        return -1;
#endif
    return 0;
}

static inline void ring_buff_deinit(ring_buffer_t * rbuff)
{
#ifdef USE_WRITE_PACKET_BUFFER
    if(rbuff->r_lock)
        OS_Mutex_destroy(rbuff->r_lock);

    os_mem_free(rbuff->buffer);
    rbuff->buffer = NULL;
#endif    
}

static inline usb_write_packet_t * rbuff_get_write_packet(ring_buffer_t * rbuff,unsigned int len, unsigned int * /* start_idx*/idx_start) 
{
    usb_write_packet_t* p_packet = NULL;
#ifdef USE_WRITE_PACKET_BUFFER
    ring_buffer_lock(rbuff);

    if (rbuff->free_space < len) {
        // No space left in ring buffer
        goto exit;
    }

    if (rbuff->packets >= MAX_ASYNC_PACKETS) {
        // Out of packet descriptors
        goto exit;
    }

    if (len > MAX_ASYNC_PACKET_SIZE) {
        // Packet is too large
        goto exit;
    }

    ASSERT(rbuff->desc[rbuff->next_desc].returned);
    rbuff->desc[rbuff->next_desc].returned = FALSE;
    rbuff->desc[rbuff->next_desc].idx_start = rbuff->idx_current;
    rbuff->desc[rbuff->next_desc].total_len = len;
    //rbuff->desc[rbuff->next_desc].packet.h_transfer = NULL;
    os_mem_zero(&(rbuff->desc[rbuff->next_desc].packet.h_transfer),sizeof(tr_user_t));

    *idx_start = rbuff->idx_current;

    // get packet
    p_packet = & rbuff->desc[rbuff->next_desc].packet;
    p_packet->buffer = &rbuff->buffer[rbuff->idx_current];

    rbuff->free_space -= len;
    rbuff->idx_current += len;
    if (rbuff->idx_current > RING_BUFFER_END) {
        rbuff->free_space += rbuff->idx_current - RING_BUFFER_END; // recalculate free space
        rbuff->idx_current = 0; // wrap around
    }

    rbuff->packets++;
    rbuff->next_desc = (rbuff->next_desc + 1) % MAX_ASYNC_PACKETS;
exit:
    ring_buffer_unlock(rbuff);
#endif    
    return p_packet;
}

static inline void rbuff_signal_complete(ring_buffer_t * rbuff,unsigned int idx_start,OS_Event_handle dev_wr_event) 
{
#ifdef USE_WRITE_PACKET_BUFFER

    ring_buffer_lock(rbuff);

    rbuff->complete[rbuff->idx_complete] = idx_start;
    rbuff->idx_complete++;

    OS_Event_set(/*((USB_DEVICE_STRUCT_PTR)usb_pdev)->usbdev_event*/dev_wr_event, WRITEN_EVENT_MASK);
    ring_buffer_unlock(rbuff);
#endif    
}

static inline int rbuff_complete_packet(ring_buffer_t * rbuff,unsigned int idx_start, tr_user_t * * ph_transfer) 
{
    int ret = 0;
#ifdef USE_WRITE_PACKET_BUFFER
    ring_buffer_lock(rbuff);

    int i = rbuff->last_returned_desc;
    unsigned int count = 0;
    while ((count < rbuff->packets) && (rbuff->desc[i].idx_start != idx_start)) {
        count++;
        i++;

        if (i == MAX_ASYNC_PACKETS) {
            i = 0;
        }
    }

    if (count == rbuff->packets) {
        ret = -1; // not found
        ASSERT(FALSE);
        goto exit;
    }

    rbuff->desc[i].returned = TRUE;

    if (ph_transfer)
        *ph_transfer = &(rbuff->desc[i].packet.h_transfer);

    ASSERT(rbuff->last_returned_desc < MAX_ASYNC_PACKETS);
    count = 0;
    unsigned int total = rbuff->packets;
    while ((count < total) && rbuff->desc[rbuff->last_returned_desc].returned) {
        ASSERT(rbuff->packets);
        rbuff->packets--;

        // calculate free space
        unsigned int idx_end = rbuff->desc[rbuff->last_returned_desc].idx_start + rbuff->desc[rbuff->last_returned_desc].total_len;
        if (idx_end > RING_BUFFER_END) {
            ASSERT(rbuff->desc[rbuff->last_returned_desc].idx_start <= RING_BUFFER_END);
            rbuff->free_space += (RING_BUFFER_END - rbuff->desc[rbuff->last_returned_desc].idx_start);
        } else {
            rbuff->free_space += rbuff->desc[rbuff->last_returned_desc].total_len;
        }

        rbuff->last_returned_desc = (rbuff->last_returned_desc + 1) % MAX_ASYNC_PACKETS;

        count++;
    }

exit:
    ring_buffer_unlock(rbuff);
#endif    
    return ret;
}

static inline unsigned int rbuff_complete_packets(ring_buffer_t * rbuff)
{
#ifdef USE_WRITE_PACKET_BUFFER

    ring_buffer_lock(rbuff);

    tr_user_t * h_ptransfer;

    for (unsigned int i = 0; i < rbuff->idx_complete; i++) {
        h_ptransfer = NULL;
        if (0 == rbuff_complete_packet(rbuff,rbuff->complete[i], &h_ptransfer)) {
            if (h_ptransfer) {
                close_transfer(h_ptransfer); /* close transfer */  /* tr complete callback will set 0 to close transfer */
            }
        }
    }

    rbuff->idx_complete = 0;        

    ring_buffer_unlock(rbuff);
#endif    

    return 0;
}
//#endif  /* #ifdef USE_WRITE_PACKET_BUFFER */

//
// global variables
//
USB_DEVICE_STRUCT_PTR   gp_usb_device = NULL;
OS_Mutex_handle			g_usbdev_synch = NULL;      /* allocate and init at program start */

BOOL    g_dev_attached = FALSE;
int     opend_sco = 0;

// local function declare
static void read_task_proc_stub(void * param);
static BOOL submit_read_request(int event);
static BOOL check_device();

static inline void clear_tr_handle(tr_user_t * tr_ptr)
{
    tr_ptr->tr_blocked = 0;
    //tr_ptr->tr_internal_ptr = 0;
    //tr_ptr->drv_private = 0;
    //tr_ptr->transfered = 0;
}

BOOL acl_attached(USB_DEV_HANDLE usb_dev)
{
    OS_Mutex_lock(g_usbdev_synch);

    gp_usb_device->usb_dev = usb_dev;
    if(gp_usb_device->usb_dev == NULL) {
        OS_Mutex_unlock(g_usbdev_synch);
        printf("get usb dev failed\n");
        return FALSE;
    }
#if 0    
    /* move to hci open    */
    if ((gp_usb_device->usbdev_event = OS_Event_create(0/* 0 - manual clear */)) == NULL) {
        printf("\ncreate usbdev_event failed!\n");
        goto fail1;
    }
#endif

//#ifndef USE_SCO_TRANSFER   /* if has sco then move to sco attach  */
    // gp_usb_device->initialized = TRUE;	 // inited at interface
    g_dev_attached = TRUE;
//#endif

    OS_Mutex_unlock(g_usbdev_synch);

    return TRUE;
/*
fail1:
    gp_usb_device->usb_dev = NULL;
    OS_Mutex_unlock(g_usbdev_synch);
    return FALSE;
*/    
}

void dev_set_min_packet_size(unsigned int size)
{
    OS_Mutex_lock(g_usbdev_synch);
    gp_usb_device->min_packet_size = size;
    OS_Mutex_unlock(g_usbdev_synch);    
}

unsigned int dev_get_min_packet_size()
{
    OS_Mutex_lock(g_usbdev_synch);
    return gp_usb_device->min_packet_size;
    OS_Mutex_unlock(g_usbdev_synch);
}

void acl_detached(USB_DEV_HANDLE usb_dev)
{
    OS_Mutex_lock(g_usbdev_synch);
    if(gp_usb_device->usb_dev != usb_dev) {
        printf("usb dev is invalid at detach\n");
    }
    else {
        OS_Event_set(gp_usb_device->usbdev_event,CLOSE_EVENT_MASK);
        /*
        OS_Event_destroy(gp_usb_device->usbdev_event);
        gp_usb_device->usbdev_event = NULL;
        gp_usb_device->usb_dev = NULL;*/
        
        g_dev_attached = FALSE;
        //-gp_usb_device->initialized = FALSE;
    }
    OS_Mutex_unlock(g_usbdev_synch);
}

void usb_dev_set_pipe_handle(PIPE_HANDLE h_pipe, unsigned int event ,unsigned int  in_out)
{
    switch(event) {
    case EVENT_INTERRUPT:
        gp_usb_device->usb_pipes[EVENT_INTERRUPT].h_pipe = h_pipe;
        break;
    case EVENT_BULK:
        if(in_out) /* 1 => USB_SEND  ,0 => USB_RECV */
            gp_usb_device->out_pipes[EVENT_BULK] = h_pipe;
     else
            gp_usb_device->usb_pipes[EVENT_BULK].h_pipe = h_pipe;
    break;
#ifdef USE_SCO_TRANSFER
    case EVENT_ISOCH:
        if(in_out) /* 1 => USB_SEND  ,0 => USB_RECV */
            gp_usb_device->out_pipes[EVENT_ISOCH] = h_pipe;
        else
            gp_usb_device->usb_pipes[EVENT_ISOCH].h_pipe = h_pipe;
        break;
#endif
    default :
        printf("error pipe event\n");
    }
}

PIPE_HANDLE usb_dev_get_pipe_handle(unsigned int event ,unsigned int  in_out)
{
    switch(event) {
    case EVENT_INTERRUPT:
        return gp_usb_device->usb_pipes[EVENT_INTERRUPT].h_pipe;
    case EVENT_BULK:
        if(in_out) /* 1 => USB_SEND  ,0 => USB_RECV */
            return gp_usb_device->out_pipes[EVENT_BULK];
        else
            return gp_usb_device->usb_pipes[EVENT_BULK].h_pipe;
        break;
#ifdef USE_SCO_TRANSFER
    case EVENT_ISOCH:
        if(in_out) /* 1 => USB_SEND  ,0 => USB_RECV */
            return gp_usb_device->out_pipes[EVENT_ISOCH];
        else
            return gp_usb_device->usb_pipes[EVENT_ISOCH].h_pipe;
        break;
#endif
    default :
        printf("error pipe event\n");
    }
    return NULL;
}

uint32_t usbdev_event_set(uint32_t event_mask)
{
	uint32_t ret;	
    OS_Mutex_lock(g_usbdev_synch);
    ret = OS_Event_set(gp_usb_device->usbdev_event,event_mask);
    OS_Mutex_unlock(g_usbdev_synch);
    return ret;
}

void  usbdev_set_inited()
{
    OS_Mutex_lock(g_usbdev_synch);
    gp_usb_device->initialized = TRUE;
    OS_Mutex_unlock(g_usbdev_synch);
}

unsigned int usbdev_check_attached()
{
    //OS_Mutex_lock(g_usbdev_synch);
    return g_dev_attached;
    //OS_Mutex_unlock(g_usbdev_synch);
}

#ifdef USE_SCO_TRANSFER
#if 0
uint_16 drv_sco_packet_maxsize(void * usb_dev,int in_out,int ep_num)
{
    SCO_DEVICE_STRUCT_PTR sco_dev = 
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->dev_classes[SCO_CLASS_INTF]);
    if(in_out)  /* 1 - USB_SEND, 0 - USB_RECV */
        return (uint_16)(SHORT_UNALIGNED_LE_TO_HOST(sco_dev->endp_sco_out[ep_num].wMaxPacketSize) & PACKET_SIZE_MASK);
    else
        return (uint_16)(SHORT_UNALIGNED_LE_TO_HOST(sco_dev->endp_sco_in[ep_num].wMaxPacketSize) & PACKET_SIZE_MASK);
}
#endif

/* the same interface will callback at attach for diffrent alt settings */
BOOL sco_attached(USB_DEV_HANDLE usb_dev,int alt_setting)
{
    OS_Mutex_lock(g_usbdev_synch);
    if(gp_usb_device->sco_pdata == NULL) {
        //ASSERT(gp_usb_device->sco_pdata == NULL);
        gp_usb_device->sco_pdata = (SCO_DATA_PRT) os_mem_alloc_zero(sizeof(SCO_DATA));
        if(gp_usb_device->sco_pdata == NULL) {
            OS_Mutex_destroy(g_usbdev_synch);
            printf("alloc USB_DEVICE_STRUCT failed\n");
            return FALSE;
        }
    }

    //get describ  //todo
    //g_dev_attached = TRUE;

    OS_Mutex_unlock(g_usbdev_synch);

    return TRUE;
}

void sco_detached(USB_DEV_HANDLE usb_dev,int alt_setting)
{
    OS_Mutex_lock(g_usbdev_synch);
    //if(alt_setting == 0) {
    if(gp_usb_device->sco_pdata != NULL) {
        // ASSERT(gp_usb_device->sco_pdata != NULL);
        os_mem_free(gp_usb_device->sco_pdata );
        gp_usb_device->sco_pdata = NULL;
    }
    // todo other
    
    OS_Mutex_unlock(g_usbdev_synch);
}

void sco_set_interfaced(USB_DEV_HANDLE usb_dev)
{
    OS_Mutex_lock(g_usbdev_synch);
    opend_sco = 1;
    OS_Mutex_unlock(g_usbdev_synch);
}

static int sco_actived(/* USB_DEV_HANDLE usb_dev */)
{
    return (gp_usb_device->sco_pdata->sco_pipe_idx >= 0 && 
        gp_usb_device->sco_pdata->sco_pipe_idx < NUM_SCO_ENDPOINTS);
}
#endif

void sent_packet_signal_complete(unsigned int idx_start)
{
    rbuff_signal_complete(&gp_usb_device->ring_buffer,idx_start,gp_usb_device->usbdev_event);
}

static BOOL check_device()
{
    // call this function ,must after gp_usb_device locked !
    if(!g_dev_attached)
        return FALSE;

    if (!gp_usb_device->initialized) {
        printf("[HCI] [USB] check_device - device not active\n");
        return FALSE;
    }
    
    if (gp_usb_device->closing) {
        printf("[HCI] [USB] check_device - device closing...\n");
        return FALSE;
    }

    return TRUE;
}

/* called by  hci_open */
static BOOL open_connection(void)
{
    // int opend_sco = 0; // move to global
#ifdef  USE_SCO_TRANSFER
    USB_DEV_HANDLE  udev_handle = gp_usb_device->usb_dev;
    SCO_DATA_PRT sco_pdata = gp_usb_device->sco_pdata;
    int sco_rd_pkts = SCO_READ_PACKETS_PER_TRANSFER;
#endif
    int i;

    OS_Mutex_lock(g_usbdev_synch);

    if ((gp_usb_device->usbdev_event = OS_Event_create(0/* 0 - manual clear */)) == NULL) {
        printf("\ncreate usbdev_event failed!\n");
        return FALSE;
    }
    
    if (! check_device()) {
        printf("[USB] -open_connection : ERROR_NOT_READY\n");
        OS_Mutex_unlock(g_usbdev_synch);
        return FALSE;
    }

    // open acl interface at attach event callback
    // open_acl_interface

    gp_usb_device->packet_size = DEFPACKETSIZE;
    gp_usb_device->block_size = DEFBLOCKSIZE;

#ifdef  USE_SCO_TRANSFER
    gp_usb_device->sco_pdata->suggested_sco_alt_setting = SCO_DEFAULT_ALT_SETTING;  /* */

    /* SCO_READ_PACKETS_PER_TRANSFER x  SCO_READ_FRAMES_PER_PACKET = 9*3 = 27 */
    sco_pdata->sco_read_frames = sco_rd_pkts * SCO_READ_FRAMES_PER_PACKET;
    if (sco_pdata->sco_read_frames  > MAX_SCO_READ_FRAMES_PER_TRANSFER) {
        sco_pdata->sco_read_frames = MAX_SCO_READ_FRAMES_PER_TRANSFER;
    }

    if (sco_pdata->suggested_sco_alt_setting != 0) {
        for (int i = 0 ; i < sco_pdata->sco_in_count ; ++i ) {
            if ((sco_pdata->endp_sco_alt_setting[i] == sco_pdata->suggested_sco_alt_setting)) {
                sco_pdata->sco_pipe_idx = i;

                uint_16  ep_max_size = get_sco_endp_max_packetsize(udev_handle, i, USB_RECV);
                if (( ep_max_size * sco_pdata->sco_read_frames) > gp_usb_device->min_packet_size)
                	gp_usb_device->min_packet_size = ep_max_size * sco_pdata->sco_read_frames;
            }
        }
    }

    // check --can remove for cut code size
    if (sco_actived())  {
		int sco_idx = sco_pdata->sco_pipe_idx;
		uint_16 sco_in_ep_size = get_sco_endp_max_packetsize(udev_handle, sco_idx, USB_RECV);
		uint_16 sco_out_ep_size = get_sco_endp_max_packetsize(udev_handle, sco_idx, USB_SEND);
        if ((sco_in_ep_size != sco_out_ep_size) || (sco_pdata->sco_out_count  != sco_pdata->sco_in_count) ) {
            sco_pdata->sco_pipe_idx = -1;
            printf("[HCI] [USB] iso endpoints in/out mismatch!\n");
            ASSERT(0);
        }
    }
#endif

    OS_Event_clear(gp_usb_device->usbdev_event, CLOSE_EVENT_MASK);
    OS_Event_clear(gp_usb_device->usbdev_event, WRITEN_EVENT_MASK);

#ifdef  USE_SCO_TRANSFER
    if (sco_actived()) {
            if(open_sco_interface( gp_usb_device->usb_dev, gp_usb_device->sco_pdata->suggested_sco_alt_setting)) {
                // sco_detached(gp_usb_device->usb_dev,gp_usb_device->sco_pdata->suggested_sco_alt_setting);
                printf("open sco intf altsetting %d failed\n",gp_usb_device->sco_pdata->suggested_sco_alt_setting);
                // goto Cleanup;
            }
        // opend_sco = 1;
       // OS_Time_delay(10); // waiting set interface finish.
    }
#endif

    if (gp_usb_device->packet_size < gp_usb_device->min_packet_size)
        gp_usb_device->packet_size = gp_usb_device->min_packet_size;
    
    if(ring_buff_init(&gp_usb_device->ring_buffer)) {
		printf("alloc ring_buffer failed\n");
		goto Cleanup;
    }

    gp_usb_device->fmem_id = os_alloc_fixed_mem (offsetof(PACKET, data) + gp_usb_device->packet_size, gp_usb_device->block_size);
    if (!gp_usb_device->fmem_id) {
        printf("[HCI] [USB] Failed to allocate fixed memory for packets\n");
        goto Cleanup;
    }

    // Ensure all events are reset
    for (i=0; i<NUM_IN_PIPES; i++) {
        OS_Event_clear(gp_usb_device->usbdev_event ,(1 << i));
    }
    
    // creat read task 
    gp_usb_device->task_handle = OS_Task_create(read_task_proc_stub, (void*)NULL, (uint32_t)USB_READ_TASK_DEF_PRIORITY, USB_READ_TASK_STACKSIZE, "USB read task", NULL);
    
    if (gp_usb_device->task_handle == (uint32_t)OS_TASK_ERROR) {
        goto Cleanup;
    }
    //_task_ready(_task_get_td(task_id));
    //OS_Task_resume(task_id);

    for (i = EVENT_BULK; i <= (opend_sco ? EVENT_ISOCH : EVENT_INTERRUPT ); i++) {
        if (!submit_read_request(i)) {
            printf("[HCI] [USB] Read Request to %d failed.\n", i);
            goto Cleanup;
        }
    }

    printf("[USB] -open_connection : success\n");
    OS_Mutex_unlock(g_usbdev_synch);

    return TRUE;

Cleanup:
    OS_Mutex_unlock(g_usbdev_synch);
    //close_connection();
    printf("[USB] -open_connection : err\n");

    return FALSE;
}

BOOL pre_close_connection()
{
    OS_Mutex_lock(g_usbdev_synch);

    if (!gp_usb_device->initialized) {
        OS_Mutex_unlock(g_usbdev_synch);
        return FALSE;
    }

    printf("[HCI] set CLOSE_EVENT)\n");
    OS_Event_set(gp_usb_device->usbdev_event,CLOSE_EVENT_MASK);

	/* complete or cancel all in transfer , and close pipe */
    close_acl_interface(gp_usb_device->usb_dev); /* will cancel the all pipe transfer ,todo check it */
    for (int i = 0; i<NUM_IN_PIPES; i++) {
#ifdef FIXED_USE_DYNAMIC
        PACKET		*p_packet;
        while(gp_usb_device->usb_pipes[i].packets) {
            p_packet = gp_usb_device->usb_pipes[i].packets;
            gp_usb_device->usb_pipes[i].packets = gp_usb_device->usb_pipes[i].packets->next;
            os_put_fixed(p_packet,gp_usb_device->fmem_id);
        }
#endif        
	/* complete or cancel all in transfer , and close pipe */
	memset(&gp_usb_device->usb_pipes[i],0 ,sizeof(struct usb_pipe));
    }

    for (int i=0; i<NUM_OUT_PIPES; i++) {
	/* close all of out pipe ,dont need cancel transfer  */
        if (gp_usb_device->out_pipes[i]) {
            gp_usb_device->out_pipes[i] = NULL;
        }
    }

    if (gp_usb_device->fmem_id) {
        os_free_fixed_mem(gp_usb_device->fmem_id);
        gp_usb_device->fmem_id = NULL;
    }

    while (gp_usb_device->packet_list) {
        completed_packet_t *p_next = gp_usb_device->packet_list->next;
        os_mem_free (gp_usb_device->packet_list);
        gp_usb_device->packet_list = p_next;
    }

    ASSERT(gp_usb_device->packet_list == NULL);
    gp_usb_device->last_packet = NULL;
#ifdef DEBUG_COMPLETE_PKT_NUM
    complete_packets_num = 0;
#endif

#ifdef USE_SCO_TRANSFER
    gp_usb_device->sco_pdata->sco_pipe_idx = -1;
#endif    

    OS_Mutex_unlock(g_usbdev_synch);
    
    return TRUE;
}

void close_connection(void)
{
    // in this point ,it is not  locked.
    printf("[USB] +close_connection\n");
    if (!pre_close_connection()) {
        printf("[HCI] [USB] -close_connection - device not active\n");
        return;
    }

    OS_Mutex_lock(g_usbdev_synch);

    ring_buff_deinit(&gp_usb_device->ring_buffer);         /* ring buffer deinit */

	/* clean task some else */
	/* wait task close */
    if (gp_usb_device->task_handle) {
        OS_Task_delete(gp_usb_device->task_handle);
	  gp_usb_device->task_handle = 0;
    }

    OS_Event_destroy(gp_usb_device->usbdev_event);
    gp_usb_device->usbdev_event = NULL;
    //    gp_usb_device->usb_dev = NULL;

    OS_Mutex_unlock(g_usbdev_synch);

    printf("[USB] -CloseConnection\n");
}

void close_usbdev(void)
{
    OS_Mutex_lock(g_usbdev_synch);
    if ((! gp_usb_device->initialized) || gp_usb_device->closing) {
        OS_Mutex_unlock(g_usbdev_synch);
        return ;
    }
        
    gp_usb_device->closing = TRUE;
    close_connection();
    gp_usb_device->closing = FALSE;
    gp_usb_device->initialized = FALSE;
    OS_Mutex_unlock(g_usbdev_synch);
}

static BOOL write_packet (unsigned char *p_buffer, int len, HCI_TYPE e_type)
{
    BOOL tranfer_ret;
    tr_user_t  blocked_tr_handle;  /* todo clean user tr handle */

    if(len == 0)
        return TRUE;
    
    OS_Mutex_lock(g_usbdev_synch);

    //printf("[USB] write_packet : %s : %d bytes\n", e_type == COMMAND_PACKET ? "COMMAND" : (e_type == DATA_PACKET_ACL ? "ACL" : (e_type == DATA_PACKET_SCO ? "SCO" : L"Error")), len));

    if (!check_device()) {
        OS_Mutex_unlock(g_usbdev_synch);
        return FALSE;
    }

#ifdef USE_SCO_TRANSFER	
    if ((DATA_PACKET_SCO == e_type) && !sco_actived()) {
 
        OS_Mutex_unlock(g_usbdev_synch);
        return FALSE;
    }
#endif	

    //ASSERT(p_buffer);
    //ASSERT(len > 0);

    switch (e_type) 
    {
        case COMMAND_PACKET:
        {
		OS_Mutex_unlock(g_usbdev_synch);
		tranfer_ret =  issue_cmd_transfer( gp_usb_device->usb_dev,&blocked_tr_handle,p_buffer,len);
		OS_Mutex_lock(g_usbdev_synch);

            if (!tranfer_ret || !check_device()) {
                printf("[USB] -write_packet : (COMMAND_PACKET) failure!\n");
                OS_Mutex_unlock(g_usbdev_synch);
                return FALSE;
            }

		/* in some usbhost stack ,must close this transfer after transfer complete */
            break;
        }

        case DATA_PACKET_ACL:
        {
            // ASSERT(gp_usb_device->out_pipes[PACKET_ACL]);
            unsigned int idx_start = 0xffffffff;
            int align_len = (len & 3) ? len + (4 - (len & 3)) : len;
            usb_write_packet_t* p_packet = rbuff_get_write_packet(&gp_usb_device->ring_buffer, align_len, &idx_start);
            if (! p_packet) {
                OS_Mutex_unlock(g_usbdev_synch);
                tranfer_ret = issue_acl_transfer(gp_usb_device->usb_dev,&blocked_tr_handle,
                                                                        USB_OUT_TRANSFER/*  wait */,/*NULL,*/NULL,p_buffer,len);
                OS_Mutex_lock(g_usbdev_synch);

                if ( !tranfer_ret || !check_device()) {
                    printf("[USB] -write_packet : (DATA_PACKET_ACL) failure!\n");
                    
                     OS_Mutex_unlock(g_usbdev_synch);
                    return FALSE;
                }
                /* after complete close this transfer */
            }
            else {
                p_packet->e_type = e_type;
                memcpy(p_packet->buffer, p_buffer, len);

                unsigned char * p_tmp = p_packet->buffer;

                OS_Mutex_unlock(g_usbdev_synch);
                tranfer_ret = issue_acl_transfer(gp_usb_device->usb_dev,&p_packet->h_transfer,
                                                            USB_OUT_TRANSFER | USB_NO_WAIT ,(pointer)idx_start,p_tmp,len);
                OS_Mutex_lock(g_usbdev_synch);

                if (!tranfer_ret || !check_device()) {
                    printf("[USB] -WritePacket : (DATA_PACKET_ACL) failure!\n");

                    rbuff_complete_packet( &gp_usb_device->ring_buffer,idx_start,NULL);

                   OS_Mutex_unlock(g_usbdev_synch);
                    return FALSE;
                }

                if (transfer_completed(&p_packet->h_transfer)) {
                   close_transfer(&p_packet->h_transfer);
                }
            }
            break;
        }
#ifdef USE_SCO_TRANSFER
        case DATA_PACKET_SCO:
        {
            unsigned short max_pkt_out_size = get_sco_endp_max_packetsize(gp_usb_device->usb_dev,
                            gp_usb_device->sco_pdata->sco_pipe_idx,USB_SEND);

            unsigned int idx_start = 0xffffffff;
            int align_len = (len & 3) ? len + (4 - (len & 3)) : len;
            usb_write_packet_t* p_packet = rbuff_get_write_packet(&gp_usb_device->ring_buffer, align_len, &idx_start);

            ASSERT( (len /max_pkt_out_size) <= SCO_WRITE_FRAMES_PER_PACKET );
            if ( (len / max_pkt_out_size) > SCO_WRITE_FRAMES_PER_PACKET ) {/* nLen / sco max packet size must little than 3 frames  */
                printf("[USB] SCO packet len exceeds 3 frames\n");
                break;
            }

            if (! p_packet) {
                unsigned int lens[SCO_WRITE_FRAMES_PER_PACKET];

                for (int i = 0; i < SCO_WRITE_FRAMES_PER_PACKET; i++) {
                    lens[i] = SCO_DEF_SAMPLE_SIZE;
                    if (i < 3) {
   
                        lens[i]++;
                    }
                }

                OS_Mutex_unlock(g_usbdev_synch);
                tranfer_ret = issue_sco_transfer(gp_usb_device->usb_dev,&blocked_tr_handle,
                USB_OUT_TRANSFER/* and wait */,NULL,SCO_WRITE_FRAMES_PER_PACKET,lens,p_buffer);
                OS_Mutex_lock(g_usbdev_synch);

                if ( !tranfer_ret || !check_device()) {
                    printf("[USB] -write_packet : (DATA_PACKET_SCO) failure!\n");
                    OS_Mutex_unlock(g_usbdev_synch);
                    return FALSE;
                }
                
            }
            else {
                p_packet->e_type = e_type;
                memcpy(p_packet->buffer, p_buffer, len);

                for (int i = 0; i < SCO_WRITE_FRAMES_PER_PACKET; i++) {
                    p_packet->lens[i] = SCO_DEF_SAMPLE_SIZE;
                    if (i < 3) {
                        // First 3 packets have 1 extra byte for SCO header
                        p_packet->lens[i]++;
                    }
                }

                unsigned int * p_tmplens = p_packet->lens;
                unsigned char * p_tmpbuff = p_packet->buffer;

                OS_Mutex_unlock(g_usbdev_synch);
                tranfer_ret = issue_sco_transfer(gp_usb_device->usb_dev,&blocked_tr_handle,
                USB_OUT_TRANSFER | USB_NO_WAIT,NULL,SCO_WRITE_FRAMES_PER_PACKET,p_tmplens,p_tmpbuff);
                OS_Mutex_lock(g_usbdev_synch);

                if (!tranfer_ret || !check_device()) {
                    printf("[USB] -WritePacket : (DATA_PACKET_SCO) failure!\n");

                    rbuff_complete_packet( &gp_usb_device->ring_buffer,idx_start,NULL);

                    OS_Mutex_unlock(g_usbdev_synch);
                    return FALSE;
                }

                if (transfer_completed(&p_packet->h_transfer)) {
                    close_transfer(&p_packet->h_transfer);
                }
            }
        }
        break;
#endif
        default:
            printf("[HCI - USB] write_packet :: Invalid code!\n");
            break;
    }

    OS_Mutex_unlock(g_usbdev_synch);

    return TRUE;
}

/* acl ,sco,  event, (cmd ?) , get number(needed) of bytes to d buffer from the offset of the buffer */
static BOOL  get_buffer (int type, int offset, int needed, unsigned char *d, int clean_packets)
{
    // If we remove packets from the queue, the transfer must be for the entire packet.
    // The code below assumes it in calculation of how much data has been removed from the
    // queue.

    // ASSERT ((! cleanPackets) || (offset == 0));

    usb_pipe_t *p_pipe = &gp_usb_device->usb_pipes[type];

    if (p_pipe->packets == NULL)  {
        ASSERT (! clean_packets);
        return FALSE;
    }

    PACKET *p_packet = p_pipe->packets;

    while (p_packet) {
        if (p_packet->size - p_packet->offset > offset)  /* find offset (for locate) */
            break;

        offset -= p_packet->size - p_packet->offset;
        if (clean_packets) {
            p_pipe->packets = p_packet->next;
            if (! p_pipe->packets)
                p_pipe->last_packet = NULL;
            //printf("put fixed %x\n",p_packet);
            os_put_fixed(p_packet,gp_usb_device->fmem_id);   /* delete fixed PACKET */
            p_packet = p_pipe->packets;
        }
        else
            p_packet = p_packet->next;
    }

    while (p_packet && needed) {
        int transfer = p_packet->size - p_packet->offset - offset;
        if (transfer > needed)
            transfer = needed;

        memcpy (d, p_packet->data + p_packet->offset + offset, transfer);

        if (clean_packets) {
            p_pipe->total_queue -= transfer;

            p_packet->offset += transfer + offset;
            ASSERT (p_packet->offset <= p_packet->size);

            if (p_packet->offset == p_packet->size) {
                p_pipe->packets = p_packet->next;
                if (! p_pipe->packets)
                    p_pipe->last_packet = NULL;

                //os_mem_free(p_packet);
                // printf("put fixed1 %x\n",p_packet);
                os_put_fixed(p_packet,gp_usb_device->fmem_id);   /* delete fixed PACKET */

                p_packet = p_pipe->packets;
            }
            else
                ASSERT (needed == transfer);
        }
        else
            p_packet = p_packet->next;

        d += transfer;
        needed -= transfer;
        offset = 0;

        ASSERT (needed >= 0);
    }

    if (needed != 0) {
        ASSERT (! clean_packets);
        return FALSE;
    }

    return TRUE;
}

static int packet_size (int type)
{
    unsigned char d[2];

    usb_pipe_t *p_pipe = &gp_usb_device->usb_pipes[type];

    switch (type)
    {
#ifdef USE_SCO_TRANSFER
    case EVENT_ISOCH:
        if (!get_buffer(type, 2, 1, d, FALSE))
            return 0;
        return d[0] + SCO_HEADER_SIZE;    /* HCI sco */
#endif		

    case EVENT_BULK:
        if (!get_buffer(type, 2, 2, d, FALSE))    /* HCI acl */
            return 0;
        ASSERT (p_pipe->packets->size >= 4);
        return (d[0] | (d[1] << 8)) + ACL_HEADER_SIZE;  

    case EVENT_INTERRUPT:                           /* HCI event  */
        if (!get_buffer(type, 1, 1, d, FALSE))
            return 0;
        ASSERT (p_pipe->packets->size >= 3);
        return d[0] + EVENT_HEADER_SIZE;
    }

    printf("[HCI] [USB] UNKNOWN PACKET ERROR!\n");
    ASSERT (0);

    return 0;
}

BOOL complete_packet (int type ) {  /* get complete packet then hang up to packetlist  */
    int size_packet = packet_size(type);

    usb_pipe_t *p_pipe = &gp_usb_device->usb_pipes[type];
	
    ASSERT ((size_packet == 0) || (p_pipe->total_queue > 0));

    if ((size_packet == 0) || (p_pipe->total_queue < size_packet)) {  /* this is normal error !*/
        //-printf("[HCI] [USB] type %d, Packet of invalid size1 %d .\n",/* size_packet */type,size_packet/*packet_size(type)*/);
        return FALSE;
    }

    if ((size_packet <= 0) || (size_packet > PACKET_SIZE_R)) {
        printf("[HCI] [USB] Packet of invalid size %d.\n",size_packet);
        return FALSE;
    }

    BOOL res = FALSE;

    int aligned_size =  offsetof (completed_packet_t, cdata) + size_packet + (4 - (size_packet & 3));
    completed_packet_t *p_new_packet = (completed_packet_t *)os_mem_alloc_zero (aligned_size/*offsetof (completed_packet_t, cdata) + size_packet*/);
#ifdef DEBUG_COMPLETE_PKT_NUM    
    printf("+cpkt %d,0x%x,%d\n ", ++complete_packets_num,p_new_packet,/*size_packet*/type);
#endif
    if (p_new_packet) {
        p_new_packet->e_type   = type;
        p_new_packet->clen = size_packet;
        p_new_packet->next   = NULL;
        /* get the CompletedPacket from packet list of pipe[type] */
        res = get_buffer (p_new_packet->e_type, 0, p_new_packet->clen, p_new_packet->cdata, TRUE/* clean packets */);
        if (res) {
            if (gp_usb_device->last_packet)
                gp_usb_device->last_packet->next = p_new_packet;  /* from pipe packet list get complete packet to usb_dev complete packet list */
            else
                gp_usb_device->packet_list = p_new_packet;
                gp_usb_device->last_packet = p_new_packet;
        } else {
            ASSERT (0);
            os_mem_free (p_new_packet);
        }
    } else
        ASSERT (0); //out of mem

    return res;
}

BOOL retrieve_packet (unsigned char *p_buffer, int *p_len, int *p_type, unsigned int *p_timeout)
{
    if (gp_usb_device->packet_list) { /* callby hci read, (btTaskf call it) , btTaskf's prority can't more than read task */
        *p_len = gp_usb_device->packet_list->clen;
        *p_type = gp_usb_device->packet_list->e_type;
        memcpy (p_buffer, gp_usb_device->packet_list->cdata, gp_usb_device->packet_list->clen);

        completed_packet_t *p_next = gp_usb_device->packet_list->next;
        os_mem_free (gp_usb_device->packet_list);
#ifdef DEBUG_COMPLETE_PKT_NUM
        printf("-cpkt %d,0x%x\n ", --complete_packets_num,gp_usb_device->packet_list);
#endif
        gp_usb_device->packet_list = p_next;
        if (! gp_usb_device->packet_list )
            gp_usb_device->last_packet = NULL;

        return TRUE;
    }
    return FALSE;
}

static BOOL read_packet (unsigned char *p_buffer, int *p_len, unsigned int *p_type)   /* pnType get type of readed */
{
    // printf("[USB] +read_packet\n");

    for ( ; ; ) {
        OS_Mutex_lock(g_usbdev_synch);

        if (!check_device()) {
            printf("[USB] -read_packet : FALSE (device dead)\n");
            OS_Mutex_unlock(g_usbdev_synch);
            //OS_Time_delay(1);   /* workaround for detach ,give usb task some time to do bt_deinit */
            return FALSE;
        }

        unsigned int timeout = EVENT_TIMEOUT_INFINITE;

        if (retrieve_packet (p_buffer, p_len, (int *)p_type, &timeout)) {   /*  read at first, then wait event at below */
           /* printf("[USB] -read_packet : TRUE (got stuff) _usbPipes[%d].iTotalQueue=%d\n", 
            *p_type, gp_usb_device->usb_pipes[*p_type].total_queue); */
            OS_Mutex_unlock(g_usbdev_synch);
            return TRUE;
        }

        unsigned int events_mask = (PACKETS_EVENT_MASK | CLOSE_EVENT_MASK);

        // printf(DebugOut (DEBUG_HCI_TRANSPORT, L"[USB] read_packet : WAIT\n"));
        OS_Mutex_unlock(g_usbdev_synch);

        if (MQX_OK != OS_Event_wait(gp_usb_device->usbdev_event, events_mask, FALSE, timeout)) {
            printf("\n_lwevent_wait_ticks usbdev_event failed.\n");
            continue;
        }

        //OS_Mutex_lock(g_usbdev_synch);
        if(OS_Event_check_bit(gp_usb_device->usbdev_event,PACKETS_EVENT_MASK)) {
            OS_Event_clear(gp_usb_device->usbdev_event,PACKETS_EVENT_MASK);
            //OS_Mutex_unlock(g_usbdev_synch);
            continue;
        }
        //OS_Mutex_unlock(g_usbdev_synch);  
        // printf("[USB] -read_packet : FALSE (device closed)\n");
        return FALSE;
    }
}

BOOL read_task_proc(void)
{
    unsigned int event_mask = READ_EVENT_BULK_MASK | READ_EVENT_INTERRUPT_MASK
#ifdef  USE_SCO_TRANSFER
    | READ_EVENT_ISO_MASK
#endif
    | WRITEN_EVENT_MASK  | CLOSE_EVENT_MASK;

    int stop_flag = FALSE;
    unsigned int event_status;

    // unsigned int err = USB_OK;
    BOOL success;
    unsigned int event;

    while(TRUE) {
        if (OS_Event_wait(gp_usb_device->usbdev_event, event_mask, FALSE, EVENT_TIMEOUT_INFINITE)) {
            printf("\n_lwevent_wait_ticks usbdev_event failed.\n");
            stop_flag = TRUE;
            break;
        }

        event_status = OS_Event_status(gp_usb_device->usbdev_event);

        OS_Mutex_lock(g_usbdev_synch);

        //-printf("event_status %d set.\n", event_status);

        if (event_status & CLOSE_EVENT_MASK) {
            printf("[HCI] [USB] read_task_proc waited close event.\n");
            OS_Mutex_unlock(g_usbdev_synch);	
            break;
        }

        if (!check_device()) {
            printf("[HCI] [USB] Device closed. Exiting read thread...\n");
            OS_Mutex_unlock(g_usbdev_synch);
            break;
        }
        // 1. event writen
        if (event_status & WRITEN_EVENT_MASK) {
            OS_Event_clear(gp_usb_device->usbdev_event,WRITEN_EVENT_MASK);
            if(rbuff_complete_packets(&gp_usb_device->ring_buffer)) {
                printf("[WARN] USB write completed async but could not complete packets.\n");
            }

            OS_Mutex_unlock(g_usbdev_synch);
            continue;
        }

        // unsigned int err = USB_OK;
        // BOOL success;
        // unsigned int event;
#if 1       
        if(event_status &  /* READ_EVENT_BULK_MASK */READ_EVENT_INTERRUPT_MASK)
            event = /* EVENT_BULK*/EVENT_INTERRUPT;
        else if(event_status &  /* READ_EVENT_INTERRUPT_MASK */READ_EVENT_BULK_MASK)
            event = /* EVENT_INTERRUPT */EVENT_BULK;
#else
        if(event_status &  READ_EVENT_BULK_MASK)
            event = EVENT_BULK;
        else if(event_status &  READ_EVENT_INTERRUPT_MASK)
            event = EVENT_INTERRUPT;
#endif        
#ifdef USE_SCO_TRANSFER	  
        else if(event_status &  READ_EVENT_ISO_MASK)
            event = EVENT_ISOCH;
#endif

//#ifdef USE_SCO_TRANSFER
//        // 2. event iso  , below are events of in pipe
//        if (event ==  EVENT_ISOCH) {
//        // todo			
//        } else 
//#endif

        {  /* 3. other event  EVENT_BULK EVENT_INTERRUPT EVENT_ */
            success = get_transfer_status(&( gp_usb_device->usb_pipes[event].h_transfer),
            (unsigned int *)&gp_usb_device->usb_pipes[event].cur_packet->size);
        }

        if (!success) {
            printf("[HCI] [USB] Invalid USB transfer handle. Exiting task...\n");
            OS_Mutex_unlock(g_usbdev_synch);
            stop_flag = TRUE;
            break;
        }
        else if (gp_usb_device->usb_pipes[event].cur_packet->size == 0) {
            if (event!= EVENT_ISOCH) {
                printf("[HCI] [USB] Packet size 0 : ignoring \n");
            }
        }
        else if (gp_usb_device->usb_pipes[event].cur_packet->size > gp_usb_device->packet_size) {
            printf("[HCI] [USB] Invalid Packet size %d : ignoring packet\n", gp_usb_device->usb_pipes[event].cur_packet->size);
            ASSERT(0);  /* todo remove it */
            gp_usb_device->usb_pipes[event].cur_packet->size = 0;
        }
        else {  /* in transfer ok */
            // printf("[USB] get_transfer_status returns %d bytes transferred.\n", gp_usb_device->usb_pipes[event].cur_packet->size);
            // ASSERT(gp_usb_device->usb_pipes[event].cur_packet->size <= gp_usb_device->packet_size);

            if (gp_usb_device->usb_pipes[event].last_packet) {   /* last_packet  point to the last cur_packet , packets pointer the first cur_packet */
                ASSERT(gp_usb_device->usb_pipes[event].last_packet->next == NULL);
                gp_usb_device->usb_pipes[event].last_packet->next = gp_usb_device->usb_pipes[event].cur_packet;
                gp_usb_device->usb_pipes[event].last_packet = gp_usb_device->usb_pipes[event].cur_packet;
            }
            else {
                ASSERT(gp_usb_device->usb_pipes[event].packets == NULL);  /* first packet*/
                gp_usb_device->usb_pipes[event].last_packet = gp_usb_device->usb_pipes[event].packets = gp_usb_device->usb_pipes[event].cur_packet;
            }

            gp_usb_device->usb_pipes[event].total_queue += gp_usb_device->usb_pipes[event].cur_packet->size;

            ASSERT (gp_usb_device->usb_pipes[event].last_packet->next == NULL);
            gp_usb_device->usb_pipes[event].cur_packet = NULL;

            if (complete_packet (event)) 
                OS_Event_set(gp_usb_device->usbdev_event, PACKETS_EVENT_MASK);  /*  Get completePacket  then set evnet to read_packet */
        }

        //ASSERT(gp_usb_device->usb_pipes[event].h_transfer.tr_blocked & TR_CLEAR_MASK);
        close_transfer(&gp_usb_device->usb_pipes[event].h_transfer);
        clear_tr_handle(& gp_usb_device->usb_pipes[event].h_transfer);

        BOOL submit_flag = submit_read_request(event);

        OS_Mutex_unlock(g_usbdev_synch);

        if (!submit_flag) {
		printf("[HCI] [USB] reader thread : Could not resubmit the request. Closing down\n");
		stop_flag = TRUE;
		break;
        }
    }

    if (stop_flag) {
        printf("[HCI] [USB] reader task : closing down\n");
        OS_Event_set(gp_usb_device->usbdev_event , CLOSE_EVENT_MASK);
        pre_close_connection();
    }

    printf("[USB] reader task : exited\n");
    return FALSE;
}

static void read_task_proc_stub(void * param)
{
    read_task_proc();
    //printf("read task exit\n");
    //OS_Task_suspend(0);
    return;
}

static BOOL submit_read_request(int event)
{
    tr_user_t  * tr_hptr;
    BOOL tr_ret;
    int i;
    unsigned short max_pkt_in_size;

    //printf("[USB] SubmitReadRequest %s\n", event == EVENT_BULK ? L"ACL" : (event == EVENT_INTERRUPT ? L"Event" : (event == EVENT_ISOCH ? L"SCO" : L"Error"))));

    // call after g_usbdev_synch locked
#ifdef USE_SCO_TRANSFER
    if ( (event == EVENT_ISOCH) && !sco_actived() ) {
        printf("[HCI] [USB] -submit_read_request :: SCO read when no ISOCH endpoint found\n");
        return FALSE;
    }
#endif

    OS_Event_clear(gp_usb_device->usbdev_event,(1<< event));

    // ASSERT(!gp_usb_device->usb_pipes[event].h_transfer); // h_transfr must clear 

    if (! gp_usb_device->usb_pipes[event].cur_packet)  // if bulk transfer fail ,dont delete ,then use the old PACKET
        gp_usb_device->usb_pipes[event].cur_packet =  os_get_fixed(gp_usb_device->fmem_id);   /* get packet from the fixed memory */

    if (! gp_usb_device->usb_pipes[event].cur_packet ) {
        printf("[HCI] [USB] -submit_read_request :: out of memory!\n");
        return FALSE;
    }
    else {
        memset(gp_usb_device->usb_pipes[event].cur_packet,0,sizeof(PACKET));
        // sprintf("get fixed %x\n", gp_usb_device->usb_pipes[event].cur_packet);
    }

    // reset halt pipe
    switch(event)
    {
    case EVENT_BULK:
        tr_hptr = & gp_usb_device->usb_pipes[event].h_transfer;   /* to do clean user tr handle */
        tr_ret = issue_acl_transfer(gp_usb_device->usb_dev,
        tr_hptr, USB_IN_TRANSFER | USB_NO_WAIT ,(pointer)event,
        gp_usb_device->usb_pipes[event].cur_packet->data, gp_usb_device->packet_size);

        break;
    case EVENT_INTERRUPT:
        tr_hptr = & gp_usb_device->usb_pipes[event].h_transfer;
        tr_ret =  issue_event_transfer(gp_usb_device->usb_dev,  tr_hptr,
        /* USB_IN_TRANSFER | USB_NO_WAIT ,(pointer)event, */
        gp_usb_device->usb_pipes[event].cur_packet->data, gp_usb_device->packet_size);

        break;
#ifdef USE_SCO_TRANSFER
    case EVENT_ISOCH:
        max_pkt_in_size = get_sco_endp_max_packetsize(gp_usb_device->usb_dev,
                            gp_usb_device->sco_pdata->sco_pipe_idx,USB_RECV);
 
        for (i = 0; i < gp_usb_device->sco_pdata->sco_read_frames; i++) {
            gp_usb_device->sco_pdata->in_transfer_lens [i] = max_pkt_in_size;
        }

        tr_hptr = & gp_usb_device->usb_pipes[event].h_transfer;   /* to do clean user tr handle */
        tr_ret = issue_sco_transfer(gp_usb_device->usb_dev,
        tr_hptr, USB_IN_TRANSFER | USB_NO_WAIT ,(pointer)event,
        gp_usb_device->sco_pdata->sco_read_frames, gp_usb_device->sco_pdata->in_transfer_lens,
        gp_usb_device->usb_pipes[event].cur_packet->data);
        
        break;
#endif
    default:
        ASSERT(FALSE);
    break;
    }

    // check gp_usb_device->usb_pipes[event].h_transfer must not clear

    // printf("[USB] -submit_read_request %s : TRUE\n", event == EVENT_BULK ? L"ACL" : (event == EVENT_INTERRUPT ? L"Event" : (event == EVENT_ISOCH ? L"SCO" : L"Error"))));

    return tr_ret;
}

BOOL hci_transport_init()
{
    if(!usb_drv_init())
        return FALSE;

    ASSERT((gp_usb_device == NULL) && (g_usbdev_synch == NULL));

    if ((g_usbdev_synch = OS_Mutex_create()) == NULL) {
        printf("\ncreate usbdev_synch failed!\n");
        return FALSE;
    }

    gp_usb_device = (USB_DEVICE_STRUCT_PTR) os_mem_alloc_zero(sizeof(USB_DEVICE_STRUCT));
    if(gp_usb_device == NULL) {
        OS_Mutex_destroy(g_usbdev_synch);
        printf("alloc USB_DEVICE_STRUCT failed\n");
        return FALSE;
    }
    return TRUE;
}

void hci_transport_deinit()
{
    ASSERT((gp_usb_device != NULL) && (g_usbdev_synch != NULL));
    os_mem_free(gp_usb_device);
    if(g_usbdev_synch) {
        OS_Mutex_destroy(g_usbdev_synch);
        g_usbdev_synch = NULL;
    }
    // todo add shutdown usb drv
}

BOOL hci_open (void) 
{
    printf("hci_open \n");
    if (open_connection()) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

void hci_close (void) 
{
    //close_connection();
    close_usbdev();
    printf("hci_close \n\n\n");
}

BOOL hci_write (HCI_TYPE type, unsigned char * pbuff,unsigned int len)
{
    //print("hci_write type 0x%02x len %d\n", type, len);

    if (len > PACKET_SIZE_W) {
        printf("[USB] Packet too big (%d, should be <= %d), or no space for header!\n", len, PACKET_SIZE_W);
        return FALSE;
    }

    if (!write_packet(pbuff, len, type)) {
		printf("write pakect failed\n");
        return FALSE;
	}

    return TRUE;
}

BOOL hci_read (HCI_TYPE *ptype, unsigned char * pbuff,unsigned int *plen)
{

#define UART_PKT_TYP_CMD	1
#define UART_PKT_TYP_ACL	2
#define UART_PKT_TYP_SCO	3
#define UART_PKT_TYP_EVT	4

	int type;
    //printf("hci_read\n");
    
	/*
    if (*plen > PACKET_SIZE_R) {
        printf("want read packet too big (should be <= %d), or no space for header!\n", PACKET_SIZE_W));
        return FALSE;
    }*/

    if (!read_packet(pbuff, (int *)plen, &type)) {
        printf("read packet failed\n");
        return FALSE;
    }

    ASSERT(*plen < PACKET_SIZE_R);

    switch(type)
    {
        case EVENT_BULK:            // ACL packet
            *ptype = UART_PKT_TYP_ACL;
            return TRUE;

        case EVENT_INTERRUPT:           // HCI Event
            *ptype = UART_PKT_TYP_EVT;
            return TRUE;

        case EVENT_ISOCH:           // SCO packet
            *ptype = UART_PKT_TYP_SCO;
            return TRUE;

        default: 
            printf("unknown type packet (ignoring)\n");
            //ASSERT(FALSE);
            break;
    }

    return FALSE;
}

