#ifndef _USB_HCI_H_
#define _USB_HCI_H_

#include "os_utils.h"
#include "hci_transport.h"
//#include "usb.h"
//#include "hostapi.h"

//#define DEBUG_COMPLETE_PKT_NUM
//#define USE_WRITE_PACKET_BUFFER	/* write to ring buffer at first */
//#define usb_class_intf_to_dev(c)	container_of(c, struct dev_class, dev)

#ifdef USE_WRITE_PACKET_BUFFER
#define USB_READ_TASK_DEF_PRIORITY          10
#else
#define USB_READ_TASK_DEF_PRIORITY          10 
#endif

#define USB_READ_TASK_STACKSIZE            2000 //1600

// constants used for selecting the SCO pipe
#define SCO_INTERFACE_NUMBER             1
#define SCO_DEFAULT_ALT_SETTING          0 // by default, SCO is off
#define SCO_MAX_ALT_SETTING              5

#define SCO_DEF_SAMPLE_SIZE             8
#define SCO_DEF_WRITE_LOW_NUM_PACKETS   4
#define SCO_DEF_WRITE_HIGH_NUM_PACKETS  8
#define SCO_DEF_PACKET_SIZE             27

#define SCO_WRITE_FRAMES_PER_PACKET		3     /* one packet send in there frames  */
#define SCO_READ_FRAMES_PER_PACKET		3      /* one packet recv in there frames */

#define SCO_READ_PACKETS_PER_TRANSFER       9        /* one sco transfer includes nine packets */
#define MAX_SCO_READ_PACKETS_PER_TRANSFER   15
#define MAX_SCO_READ_FRAMES_PER_TRANSFER    (MAX_SCO_READ_PACKETS_PER_TRANSFER * SCO_READ_FRAMES_PER_PACKET)  /* 3x15 */


#define DEFPACKETSIZE       /* 256 */384		/*  256Bytes per packet for receive  */
#define DEFBLOCKSIZE        6 /*5*/			/*  default buffers is 5 packet */

#if 0
#ifdef USE_SCO_TRANSFER
  #define NUM_SCO_ENDPOINTS			6
  #define NUM_SCO_IN_PIPES        1
  #define NUM_SCO_OUT_PIPES       1
#else
  //#define NUM_SCO_ENDPOINTS			0
  //#define NUM_SCO_IN_PIPES        0
  //#define NUM_SCO_OUT_PIPES       0
#endif
#endif

/*
#define PACKET_SCO          0
#define PACKET_ACL          1
#define PACKET_COMMAND      2
*/
#define PACKET_ACL          0
#define PACKET_COMMAND		1
#define PACKET_SCO          2

#define SCO_HEADER_SIZE     3         	/* core4.0 spec HCI DATA FORMATS  */
#define ACL_HEADER_SIZE     4
#define EVENT_HEADER_SIZE   2

#define PACKET_SIZE_R      1024 // (/*64*/2 * 1024 + 128) // same as adk stack packetStore size
#define PACKET_SIZE_W       (2048)

typedef struct _packet
{
    int             size;
    /* if noffset == nSize then discard this packet. */
    int             offset;
    struct _packet * next;

    unsigned char   data[1];
} PACKET , * PACKET_T;

typedef struct _completed_packet {
    struct _completed_packet * next;
    int             e_type;
    int             clen;

    unsigned char   cdata[1];
} completed_packet_t;

/* in pipe struct */
typedef struct usb_pipe		
{
    /* mqx pipe handle => usb_hostdev_get_pipe_handle pipe_handle_t  */
    PIPE_HANDLE     h_pipe;	 
    
    /* we don't use pointer for avoid the mem fragment */
    tr_user_t       h_transfer;	        
    
    /* mqx  _usb_host_set_up_tr => _usb_host_get_tr_element  tr will linked on the tr_list_ptr  ( _usb_host_get_transfer_status ) */	
    PACKET		*cur_packet;	
    PACKET		* packets;
    PACKET		* last_packet;

    /* size of all items that linked in the packet list pointed by packets */
    int			total_queue;
} usb_pipe_t;

#ifdef USE_SCO_TRANSFER
typedef struct _sco_data
{
    int                                     sco_pipe_idx;
    unsigned int                         sco_read_frames;
    unsigned int                         close_sco_io;
    
    uint_8						endp_sco_alt_setting[NUM_SCO_ENDPOINTS];
    int                                      sco_in_count, sco_out_count;
    unsigned int                         suggested_sco_alt_setting;

    unsigned int                         in_transfer_lens[MAX_SCO_READ_FRAMES_PER_TRANSFER];
    unsigned int                         out_transfer_lens[SCO_WRITE_FRAMES_PER_PACKET];
} SCO_DATA, _PTR_ SCO_DATA_PRT;
#endif

typedef struct _usb_write_packet {
    tr_user_t  h_transfer;  /* don't use pointer */
    HCI_TYPE e_type;
    unsigned char * buffer;
#ifdef USE_SCO_TRANSFER
    unsigned int lens[SCO_WRITE_FRAMES_PER_PACKET];
#endif
} usb_write_packet_t;

typedef struct _usb_descriptor {
    usb_write_packet_t packet;
    unsigned int idx_start;
    unsigned int returned;
    unsigned int  total_len;
} usb_descriptor_t;

#define RING_BUFFER_SIZE        10240
#define MAX_ASYNC_PACKET_SIZE   2048
#define RING_BUFFER_END         (RING_BUFFER_SIZE - MAX_ASYNC_PACKET_SIZE)
#define MAX_ASYNC_PACKETS       32

typedef struct _ring_buffer
{
	unsigned int idx_current;
	unsigned int free_space;
	unsigned char * buffer;
	unsigned int packets;
	unsigned int last_returned_desc;
	unsigned int next_desc;
	usb_descriptor_t desc[MAX_ASYNC_PACKETS];
	unsigned int idx_complete;
	unsigned int complete[MAX_ASYNC_PACKETS];
	OS_Mutex_handle /* unsigned int*/ r_lock;
} ring_buffer_t;

/* gloab usb device handle */
typedef struct usb_device_struct
{
    USB_DEV_HANDLE /* APP_DEVICE_STRUCT */  usb_dev;

    fixed_mem_id_t            fmem_id;
    ring_buffer_t		    ring_buffer;
    
    /* incoming */
    struct usb_pipe				usb_pipes[NUM_IN_PIPES];
    /* outgoing  */
    PIPE_HANDLE			 	out_pipes[NUM_OUT_PIPES];

    unsigned int                         closing; 

    unsigned int				initialized;

    /* set to  DEFPACKETSIZE at first ,the second instead of the minimux max packet size of all pipes */
    unsigned int                         packet_size; 
    unsigned int                         block_size;
    unsigned int                         min_packet_size;
#ifdef USE_SCO_TRANSFER
    SCO_DATA_PRT                    sco_pdata;   /* alloc at sco attach */
#endif
    completed_packet_t            * packet_list;
    completed_packet_t            * last_packet;

    // usb bt class event bits 
#define READ_EVENT_ISO_MASK                 (1 << EVENT_ISOCH)
#define READ_EVENT_BULK_MASK               (1 << EVENT_BULK)
#define READ_EVENT_INTERRUPT_MASK       (1 << EVENT_INTERRUPT)

#define WRITEN_EVENT_MASK				(1 << EVENT_WRITTEN )
#define CLOSE_EVENT_MASK				(1 << EVENT_CLOSE)
#define PACKETS_EVENT_MASK				(1 << EVENT_PACKETS)
    OS_Event_handle						usbdev_event;

    uint32_t task_handle;
} USB_DEVICE_STRUCT,  _PTR_ USB_DEVICE_STRUCT_PTR;


/* declare drv function */

// extern usb driver function
extern BOOL  issue_cmd_transfer(
    void *  handle,
    tr_user_t *                   h_tr_ptr,
    unsigned char *                    buff_ptr,
    unsigned int                 		buf_len);

extern  BOOL issue_event_transfer(
    void *  handle,
    tr_user_t *                h_tr_ptr,
    unsigned char *                    buff_ptr,
    unsigned int                 		buf_len);

extern BOOL issue_acl_transfer(
    void *  handle,
    tr_user_t *                h_tr_ptr,
    unsigned int flags,
    pointer                   	cb_unblocked_param,
    unsigned char *                    buff_ptr,
    unsigned int                 		buf_len);

extern int open_acl_interface(void * usb_dev);
extern void close_acl_interface(void * usb_dev);
extern void close_transfer(tr_user_t * h_tr_ptr);
extern BOOL transfer_completed(tr_user_t * h_tr_ptr);  // judge the complete of a transfer
extern BOOL get_transfer_status(tr_user_t * h_tr_ptr,unsigned int * transfered);

extern  BOOL usb_drv_init();

#ifdef USE_SCO_TRANSFER
// extern unsigned short drv_sco_packet_maxsize(void * usb_dev,int in_out,int ep_num);
extern unsigned short get_sco_endp_max_packetsize(void * usb_dev,int idx, int in_out);
extern int open_sco_interface(void * usb_dev, int altsetting);
extern int close_sco_interface(void * usb_dev,int altsetting);
extern BOOL issue_sco_transfer(
    /* pipe_handle_t h_pipe, */
    void *  handle,
    tr_user_t *                h_tr_ptr,
    uint32_t flags,             /* block or nonblock */
    /* Callback parameter */
    pointer                   	cb_unblocked_param,
    uint32_t frames,          /* number of frames in transfer */
    unsigned int * lens_ptr,  /*  array of lengths  (one entry per frame) */
    unsigned char * buff_ptr             /* data buffer */
);
#endif


#endif  /* _USB_HCI_H_ */

