@echo off

rem convert path to backslash format
set MQXROOTDIR=%1
set MQXROOTDIR=%MQXROOTDIR:/=\%
set OUTPUTDIR=%2
set OUTPUTDIR=%OUTPUTDIR:/=\%
set TOOL=%3


rem copy common files
IF NOT EXIST %OUTPUTDIR% mkdir %OUTPUTDIR%
copy %MQXROOTDIR%\common\include\usb.h %OUTPUTDIR%\usb.h /Y
copy %MQXROOTDIR%\common\include\usb_debug.h %OUTPUTDIR%\usb_debug.h /Y
copy %MQXROOTDIR%\common\include\usb_desc.h %OUTPUTDIR%\usb_desc.h /Y
copy %MQXROOTDIR%\common\include\usb_error.h %OUTPUTDIR%\usb_error.h /Y
copy %MQXROOTDIR%\common\include\usb_misc.h %OUTPUTDIR%\usb_misc.h /Y
copy %MQXROOTDIR%\common\include\usb_misc_prv.h %OUTPUTDIR%\usb_misc_prv.h /Y
copy %MQXROOTDIR%\common\include\usb_prv.h %OUTPUTDIR%\usb_prv.h /Y
copy %MQXROOTDIR%\common\include\usb_types.h %OUTPUTDIR%\usb_types.h /Y
copy %MQXROOTDIR%\host\source\host\ehci\ehci_cache.h %OUTPUTDIR%\ehci_cache.h /Y
copy %MQXROOTDIR%\host\source\host\khci\khci.h %OUTPUTDIR%\khci.h /Y
copy %MQXROOTDIR%\host\source\include\host_ch9.h %OUTPUTDIR%\host_ch9.h /Y
copy %MQXROOTDIR%\host\source\include\host_close.h %OUTPUTDIR%\host_close.h /Y
copy %MQXROOTDIR%\host\source\include\host_cnfg.h %OUTPUTDIR%\host_cnfg.h /Y
copy %MQXROOTDIR%\host\source\include\host_cnl.h %OUTPUTDIR%\host_cnl.h /Y
copy %MQXROOTDIR%\host\source\include\host_common.h %OUTPUTDIR%\host_common.h /Y
copy %MQXROOTDIR%\host\source\include\host_dev_list.h %OUTPUTDIR%\host_dev_list.h /Y
copy %MQXROOTDIR%\host\source\include\host_main.h %OUTPUTDIR%\host_main.h /Y
copy %MQXROOTDIR%\host\source\include\host_rcv.h %OUTPUTDIR%\host_rcv.h /Y
copy %MQXROOTDIR%\host\source\include\host_shut.h %OUTPUTDIR%\host_shut.h /Y
copy %MQXROOTDIR%\host\source\include\host_snd.h %OUTPUTDIR%\host_snd.h /Y
copy %MQXROOTDIR%\host\source\include\hostapi.h %OUTPUTDIR%\hostapi.h /Y
copy %MQXROOTDIR%\host\source\rtos\mqx\mqx_dll.h %OUTPUTDIR%\mqx_dll.h /Y
copy %MQXROOTDIR%\host\source\rtos\mqx\mqx_host.h %OUTPUTDIR%\mqx_host.h /Y
copy %MQXROOTDIR%\host\source\rtos\mqx\usb_mqx.h %OUTPUTDIR%\usb_mqx.h /Y


goto tool_%TOOL%

rem cw10 files
:tool_cw10
goto copy_end

rem iar files
:tool_iar
goto copy_end

rem cw10gcc files
:tool_cw10gcc
goto copy_end

rem uv4 files
:tool_uv4
goto copy_end

:copy_end

