/**HEADER********************************************************************
* 
* Copyright (c) 2008 Freescale Semiconductor;
* All Rights Reserved
*
*************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: khci.c$
* $Version : 3.8.86.0$
* $Date    : Sep-24-2012$
*
* Comments:
*
*   This file contains KHCI-specific implementations of USB interfaces
*
*END************************************************************************/
#include <mqx.h>
#include <lwevent.h>
#include <lwmsgq.h>

#include "usb.h"
#include "usb_prv.h"

#include "host_dev_list.h"


#include "mqx_host.h"
#include "khci.h"
#include "khci_prv.h"


#ifdef KHCI_DEBUG
struct debug_messaging {
  char inout;
  TR_MSG_TYPE type;
  uint_8 ep;
  uint_16 size;
};
volatile static uint_16 dm_index = 0;
volatile static struct debug_messaging dm[1024] = { 0 }; /* note, the array is for 1024 records only */
#   define KHCI_DEBUG_LOG(a, b, c, d) \
  { \
    dm[dm_index].inout = a; \
      dm[dm_index].type = b; \
      dm[dm_index].ep = c; \
      dm[dm_index].size = d; \
      dm_index++; \
  }
#else
#   define KHCI_DEBUG_LOG(a, b, c, d) {}
#endif

// KHCI task parameters
#define USB_KHCI_TASK_NUM_MESSAGES         16
#define USB_KHCI_TASK_TEMPLATE_INDEX       0
#define USB_KHCI_TASK_ADDRESS              _usb_khci_task
#define USB_KHCI_TASK_STACKSIZE            (1600 + 1000)
#define USB_KHCI_TASK_NAME                 "KHCI Task"
#define USB_KHCI_TASK_ATTRIBUTES           0
#define USB_KHCI_TASK_CREATION_PARAMETER   0
#define USB_KHCI_TASK_DEFAULT_TIME_SLICE   0

// atom transaction error results
#define KHCI_ATOM_TR_PID_ERROR  (-1)
#define KHCI_ATOM_TR_EOF_ERROR  (-2)
#define KHCI_ATOM_TR_CRC_ERROR  (-4)
#define KHCI_ATOM_TR_TO         (-16)
#define KHCI_ATOM_TR_DMA_ERROR  (-32)
#define KHCI_ATOM_TR_BTS_ERROR  (-128)
#define KHCI_ATOM_TR_NAK        (-256)
#define KHCI_ATOM_TR_DATA_ERROR (-512)
#define KHCI_ATOM_TR_STALL      (-1024)
#define KHCI_ATOM_TR_RESET      (-2048)

#define KHCI_ATOM_TR_CANCEL      (-8192)

#if defined( __ICCCF__ ) || defined( __ICCARM__ )
    #pragma segment="USB_BDT_Z"
    #pragma data_alignment=512
    __no_init static uint_8 bdt[512] @ "USB_BDT_Z";
#elif defined (__CC_ARM) || defined(__GNUC__)
    __attribute__((section(".usb_bdt"),unused)) static uint_8 bdt[512] = { 1 };
#else
  #pragma define_section usb_bdt ".usb_bdt" RW
  __declspec(usb_bdt) static uint_8 bdt[512] = { 1 };    // DO NOT REMOVE INITIALIZATION !!! bug in CW (cw 7.1) - generate wrong binary code, data
#endif  


/* Prototypes of functions */
static USB_STATUS _usb_khci_preinit(_usb_host_handle *handle);
static USB_STATUS _usb_khci_init(_usb_host_handle handle);
static USB_STATUS _usb_khci_shutdown(_usb_host_handle handle);
static USB_STATUS _usb_khci_send(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr);
static USB_STATUS _usb_khci_send_setup(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr);
static USB_STATUS _usb_khci_recv(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr);
static USB_STATUS _usb_khci_cancel(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr);
static USB_STATUS _usb_khci_bus_control(_usb_host_handle handle, uint_8 bus_control);
static uint_32 _usb_khci_get_frame_number(_usb_host_handle handle);

static USB_STATUS _usb_khci_host_close_pipe(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr);
static USB_STATUS _usb_khci_open_pipe ( _usb_host_handle handle,  PIPE_STRUCT_PTR pipe_ptr);

static /*void*/int _usb_khci_process_tr_complete(KHCI_PIPE_STRUCT_PTR pipe_ptr, KHCI_TR_STRUCT_PTR tr_ptr, uint_32 required, uint_32 remain, int_32 err);

const struct usb_host_callback_functions_struct _usb_khci_host_callback_table = {
   /* The Host/Device preinit function */
   _usb_khci_preinit,

  /* The Host/Device init function */
   _usb_khci_init,

   /* The function to shutdown the host/device */
   _usb_khci_shutdown,

   /* The function to send data */
   _usb_khci_send,

   /* The function to send setup data */
   _usb_khci_send_setup,

   /* The function to receive data */
   _usb_khci_recv,
   
   /* The function to cancel the transfer */
   _usb_khci_cancel,
   
   /* The function for USB bus control */
   _usb_khci_bus_control,

   NULL,

   /* The function to close pipe */
   _usb_khci_host_close_pipe,
    
   /* Get frame number function */
   _usb_khci_get_frame_number,

   /* Get microframe number function - not applicable on FS */
   NULL,
   
   /* Open pipe function */
   /* NULL*/_usb_khci_open_pipe,
   
   NULL,
   
   NULL
};

// KHCI event bits
#define KHCI_EVENT_ATTACH       0x01
#define KHCI_EVENT_RESET        0x02
#define KHCI_EVENT_TOK_DONE     0x04
#define KHCI_EVENT_SOF_TOK      0x08
#define KHCI_EVENT_DETACH       0x10
#define KHCI_EVENT_MASK         0x1f


/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_get_frame_number
*  Returned Value : frame number
*  Comments       :
*        The function to get frame number
*END*-----------------------------------------------------------------*/
uint_32 _usb_khci_get_frame_number(_usb_host_handle handle) {
    USB_HOST_STATE_STRUCT_PTR  usb_host_ptr = (USB_HOST_STATE_STRUCT_PTR) handle;
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->DEV_PTR;

    return (usb_ptr->FRMNUMH << 8 | usb_ptr->FRMNUML);
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_get_hot_int_tr
*  Returned Value : 0 successful
*  Comments       :
*        Make a message copy for transaction which need evaluation
*END*-----------------------------------------------------------------*/
static uint32_t _usb_khci_get_total_frame_count(_usb_host_handle handle)
{
    static uint32_t total_frame_number = 0;
    static uint16_t old_frame_number = 0;
    uint16_t  frame_number = 0xFFFF;

    //frame_number = ((usb_ptr->FRMNUMH) << 8) | (usb_ptr->FRMNUML);
    frame_number = _usb_khci_get_frame_number(handle);

    if(frame_number < old_frame_number) {
        total_frame_number += 2048;
    }

    old_frame_number = frame_number;

    //printf("t %d\n", frame_number + total_frame_number);

    return (frame_number + total_frame_number);
}


/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_isr
*  Returned Value : None
*  Comments       :
*        Service all the interrupts in the kirin usb hardware
*END*-----------------------------------------------------------------*/
static void _usb_khci_isr(_usb_host_handle handle) {
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR)handle;
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;
    uchar status;

    while (1)
    {
        status = (uint_8)(usb_ptr->ISTAT & usb_ptr->INTEN);

        if (!status)
            break;
        
        usb_ptr->ISTAT = status;
    
        if (status & USB_ISTAT_ATTACH_MASK) {
            /* USB device is (being) attached */
            usb_ptr->INTEN &= ~USB_INTEN_ATTACHEN_MASK;
            _lwevent_set(&usb_host_ptr->khci_event, KHCI_EVENT_ATTACH);
        }
        
        if (status & USB_ISTAT_TOKDNE_MASK) {
            // atom transaction done - token done
            
            _lwevent_set(&usb_host_ptr->khci_event, KHCI_EVENT_TOK_DONE);
        }
        
        if (status & USB_ISTAT_USBRST_MASK) {
            // usb reset
            _lwevent_set(&usb_host_ptr->khci_event, KHCI_EVENT_RESET);
        }

        if (status & USB_ISTAT_SOFTOK_MASK) { // start of frame
            // usb sof
            _lwevent_set(&usb_host_ptr->khci_event, KHCI_EVENT_SOF_TOK);
        }
    }
}


/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_attach
*  Returned Value : none
*  Comments       :
*        KHCI attach event
*END*-----------------------------------------------------------------*/
static void _usb_khci_attach(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr) {
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;

    usb_ptr->ADDR = 0;
    _time_delay(100);

    // speed check, set
    usb_host_ptr->G.SPEED = (uint_8)((usb_ptr->CTL & USB_CTL_JSTATE_MASK) ? USB_SPEED_FULL : USB_SPEED_LOW);
    if (usb_host_ptr->G.SPEED == USB_SPEED_FULL)
	{
        usb_ptr->ADDR &= ~USB_ADDR_LSEN_MASK;
	}
    usb_ptr->ISTAT = 0xff;   // clean each int flags
    usb_ptr->INTEN = \
        USB_INTEN_TOKDNEEN_MASK |
        USB_INTEN_USBRSTEN_MASK;

    //usb_ptr->INTEN |= (USB_INTEN_SOFTOKEN_MASK);

    // bus reset
    usb_ptr->CTL |= USB_CTL_RESET_MASK;
    _time_delay(30); //wait for 30 milliseconds (2.5 is minimum for reset, 10 recommended)
    usb_ptr->CTL &= ~USB_CTL_RESET_MASK;

    // Delay after reset was provided to be sure about speed- HS / FS. Since the KHCI does not run HS, the delay is redundant.
    // Some kinetis devices cannot recover after the delay, so it is better not to have delayed speed detection and SOF packet generation 
    // This is potentional risk as any high priority task will get CPU now, the host will not begin the enumeration process.
    //_time_delay(10);

        usb_ptr->SOFTHLD = 0;
#ifdef SCHEDULING_CHECK_TIME
    usb_host_ptr->schedule_state = /*START_GET_FRAME_HWTICK*/START_STABLE_HWTICK;
#else
    usb_host_ptr->schedule_state = /*0*/START_STABLE_HWTICK;
#endif
    usb_host_ptr->schedule_tr_ptr = NULL;
    usb_host_ptr->last_bd_ptr = NULL;

    /* usb_host_ptr->one_frame_tick = 0;
    usb_host_ptr->sof_start_tick
    usb_host_ptr->max_bandwidth
    usb_host_ptr->free_bandwidth
    usb_host_ptr->frame_num
    */
    usb_host_ptr->this_frame_transfered = 0;

    
    // enable SOF sending
    usb_ptr->CTL |= USB_CTL_USBENSOFEN_MASK;

    _time_delay(100);

    //usb_ptr->ISTAT = 0xff;

    usb_ptr->INTEN = \
        USB_INTEN_TOKDNEEN_MASK |
        USB_INTEN_USBRSTEN_MASK;

    // liutest enable sof interrupt and start schdule
    usb_ptr->INTEN |= (USB_INTEN_SOFTOKEN_MASK);

    usb_dev_list_attach_device((pointer)usb_host_ptr, (uint_8)(usb_host_ptr->G.SPEED), 0, 0);

    _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_ATTACH);
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_reset
*  Returned Value : none
*  Comments       :
*        KHCI reset event
*END*-----------------------------------------------------------------*/
static void _usb_khci_reset(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr) {
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;

    // clear attach flag
    usb_ptr->ISTAT |= USB_ISTAT_ATTACH_MASK;

    /* Test the presence of USB device */
    if (usb_ptr->ISTAT & USB_ISTAT_ATTACH_MASK) {
        /* device attached, so really normal reset was performed */
        usb_ptr->ADDR = 0;
        usb_ptr->ENDPOINT[0].ENDPT |= USB_ENDPT_HOSTWOHUB_MASK;
    }
    else {
        /* device was detached, the reset event is false- nevermind, notify about detach */
        _lwevent_set(&usb_host_ptr->khci_event, KHCI_EVENT_DETACH);
    }

    _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_RESET | KHCI_EVENT_TOK_DONE  |KHCI_EVENT_SOF_TOK );
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_detach
*  Returned Value : none
*  Comments       :
*        KHCI detach event
*END*-----------------------------------------------------------------*/
static void _usb_khci_detach(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr) {
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;

    usb_dev_list_detach_device((pointer)usb_host_ptr, 0, 0);

    /* Cleaning interrupt transaction queue from device is done
    ** by calling _usb_khci_host_close_pipe from upper layer
    */
    
    /* Now, disable bus control for any events, disable SOFs,
    ** just prepare for attach in host mode.
    */
    usb_ptr->CTL = USB_CTL_HOSTMODEEN_MASK;
    /* This will clear all pending interrupts... In fact, there shouldnt be any
    ** after detaching a device.
    */
    usb_ptr->ISTAT = 0xFF;
    /* Now, enable only USB interrupt attach for host mode */
    usb_ptr->INTEN = USB_INTEN_ATTACHEN_MASK;

    _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_DETACH);
}

static void _usb_khci_task(uint_32 dev_inst_ptr);

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_task_create
*  Returned Value : error or USB_OK
*  Comments       :
*        Create KHCI task
*END*-----------------------------------------------------------------*/
static USB_STATUS _usb_khci_task_create(_usb_host_handle handle) {
    //USB_STATUS status;
    _task_id task_id;
    TASK_TEMPLATE_STRUCT task_template;
    
    // check wait time for events - must be > 0
    if (USBCFG_KHCI_WAIT_TICK < 1)
        return USBERR_UNKNOWN_ERROR;

    /* create task for processing interrupt deferred work */
    task_template.TASK_TEMPLATE_INDEX = USB_KHCI_TASK_TEMPLATE_INDEX;
    task_template.TASK_ADDRESS = USB_KHCI_TASK_ADDRESS;
    task_template.TASK_STACKSIZE = USB_KHCI_TASK_STACKSIZE;
    task_template.TASK_PRIORITY = USBCFG_KHCI_TASK_PRIORITY;
    task_template.TASK_NAME = USB_KHCI_TASK_NAME;
    task_template.TASK_ATTRIBUTES = USB_KHCI_TASK_ATTRIBUTES;
    task_template.CREATION_PARAMETER = (uint_32)handle;   //USB_KHCI_TASK_CREATION_PARAMETER;
    task_template.DEFAULT_TIME_SLICE = USB_KHCI_TASK_DEFAULT_TIME_SLICE;

    task_id = _task_create_blocked(0, 0, (uint_32)&task_template);
    
    if (task_id == 0) {
        return USBERR_UNKNOWN_ERROR;
    }
    
    _task_ready(_task_get_td(task_id));

    return USB_OK;
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_preinit
*  Returned Value : error or USB_OK
*  Comments       :
*        Allocate the structures for KHCI
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_preinit(_usb_host_handle *handle) {
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR) USB_mem_alloc_zero(sizeof(USB_KHCI_HOST_STATE_STRUCT));
    KHCI_PIPE_STRUCT_PTR p, pp;
    int i;

    if (NULL != usb_host_ptr) {
        /* Allocate the USB Host Pipe Descriptors */
        usb_host_ptr->G.PIPE_DESCRIPTOR_BASE_PTR = (PIPE_STRUCT_PTR)USB_mem_alloc(sizeof(KHCI_PIPE_STRUCT) * USBCFG_MAX_PIPES);
        if (usb_host_ptr->G.PIPE_DESCRIPTOR_BASE_PTR == NULL) {
            USB_mem_free(usb_host_ptr);

            return USBERR_ALLOC;
        }
        USB_mem_zero(usb_host_ptr->G.PIPE_DESCRIPTOR_BASE_PTR, sizeof(KHCI_PIPE_STRUCT) * USBCFG_MAX_PIPES);

        p = (KHCI_PIPE_STRUCT_PTR) usb_host_ptr->G.PIPE_DESCRIPTOR_BASE_PTR;
        pp = NULL;
        for (i = 0; i < USBCFG_MAX_PIPES; i++) {
           if (pp != NULL) {
              pp->G.NEXT = (PIPE_STRUCT_PTR) p;
           }
           pp = p;
           p++;
        }

        usb_host_ptr->G.PIPE_SIZE = sizeof(KHCI_PIPE_STRUCT);
        usb_host_ptr->G.TR_SIZE = sizeof(KHCI_TR_STRUCT);

    } /* Endif */
    
    *handle = (_usb_host_handle) usb_host_ptr;
    return USB_OK;
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_init
*  Returned Value : error or USB_OK
*  Comments       :
*        Initialize the kirin HCI controller
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_init(_usb_host_handle handle) {
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR)handle;
    USB_KHCI_HOST_INIT_STRUCT_PTR param = (USB_KHCI_HOST_INIT_STRUCT_PTR) usb_host_ptr->G.INIT_PARAM;
    USB_MemMapPtr           usb_ptr;
    USB_STATUS              status = USB_OK;

    /* create lwevent group */
    if (_lwevent_create(&usb_host_ptr->khci_event, 0/*manual clear */) != MQX_OK) {
        return USBERR_GET_MEMORY_FAILED;
    }

   if (param == NULL)
       return USBERR_INIT_DATA;

    usb_ptr = (USB_MemMapPtr) param->BASE_PTR;
    usb_host_ptr->G.DEV_PTR = (USB_HOST_STATE_STRUCT_PTR) usb_ptr;
   
    _usb_khci_task_create(usb_host_ptr);
    
    // install isr
    if (!(USB_install_isr(param->VECTOR, _usb_khci_isr, (pointer)usb_host_ptr)))
        return USBERR_INSTALL_ISR;
    
    usb_ptr->ISTAT = 0xFF;
    /* Enable week pull-downs, usefull for detecting detach (effectivelly bus discharge) */
    usb_ptr->USBCTRL |= USB_USBCTRL_PDE_MASK;
    /* Remove suspend state */
    usb_ptr->USBCTRL &= ~USB_USBCTRL_SUSP_MASK;


    usb_ptr->CTL |= USB_CTL_ODDRST_MASK;
    
    usb_ptr->BDTPAGE1 = (uint_8)((uint_32)BDT_BASE >> 8);
    usb_ptr->BDTPAGE2 = (uint_8)((uint_32)BDT_BASE >> 16);
    usb_ptr->BDTPAGE3 = (uint_8)((uint_32)BDT_BASE >> 24);
    
    /* Set SOF threshold */
    usb_ptr->SOFTHLD = 1;
    
    usb_ptr->CTL = USB_CTL_HOSTMODEEN_MASK;

    /* Following is for OTG control instead of internal bus control */
    //    usb_ptr->OTGCTL = USB_OTGCTL_DMLOW_MASK | USB_OTGCTL_DPLOW_MASK | USB_OTGCTL_OTGEN_MASK;

    /* Wait for attach interrupt */
    usb_ptr->INTEN = USB_INTEN_ATTACHEN_MASK;

    // schedule que lock init
    /*    
    usb_host_ptr->que_lock = _mem_alloc_system_zero(sizeof(MUTEX_STRUCT));
    if (usb_host_ptr->que_lock == NULL) {
        return USBERR_ERROR;
    }

    if (_mutex_init(usb_host_ptr->que_lock, NULL) != MQX_OK) {
        _mem_free(usb_host_ptr->que_lock);
        return USBERR_ERROR;
    }
    */
    return status;
}

/*
USB_STATUS _usb_khci_deinit(_usb_host_handle handle) {
    return USB_OK;
}*/
    
/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_shutdown - NOT IMPLEMENT YET
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to shutdown the host
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_shutdown(_usb_host_handle handle) {
    // schedule que lock init
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR)handle;
    /*    
    if(usb_host_ptr->que_lock) {
        _mutex_destroy(usb_host_ptr->que_lock);
        _mem_free(usb_host_ptr->que_lock);
    }
    */    
    return USB_OK;
}

static inline USB_STATUS _usb_khci_transfer(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr) {
    USB_STATUS status = USB_OK;
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR) handle;
    KHCI_TR_STRUCT_PTR khci_tr_ptr = (KHCI_TR_STRUCT_PTR)tr_ptr;

    /* limitation !!!  the len of  perodic transfer length must not more than  max length of transcation packet.
    */
    khci_tr_ptr->retry_cnt = 0;             /* interrupt , iso don't retry*/
                
    TR_QUEUE_STRUCT_PTR tr_que_ptr;
    switch (pipe_ptr->PIPETYPE) {
        case USB_ISOCHRONOUS_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[iso_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[iso_out_tr_queue];
            break;
        case USB_INTERRUPT_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[intr_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[intr_out_tr_queue];
            break;
        case USB_CONTROL_PIPE:
            tr_que_ptr = usb_host_ptr->schdule_queues[ctrl_tr_queue];
            khci_tr_ptr->retry_cnt = pipe_ptr->NAK_COUNT;
            break;
        case USB_BULK_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[bulk_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[bulk_out_tr_queue];
            
            khci_tr_ptr->retry_cnt = pipe_ptr->NAK_COUNT;
            break;
        default:
            ASSERT(0); // fail pipetype
            break;
    }

    KHCI_DEBUG_LOG('i', pipe_ptr->PIPETYPE, pipe_ptr->G.ENDPOINT_NUMBER, tr_ptr->TX_LENGTH);

    //ASSERT(!tr_que_ptr);

    SCHED_QUE_LOCK(usb_host_ptr->que_lock);
    if(enque_schdule_tr_que(tr_que_ptr,khci_tr_ptr,(KHCI_PIPE_STRUCT_PTR)pipe_ptr) == -1) {
        printf("enque schdule transfer fail\n");
        status = USBERR_ALLOC;
    }
    SCHED_QUE_UNLOCK(usb_host_ptr->que_lock);
    
    return status;
    
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_send
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to send data
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_send(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr) {
    return _usb_khci_transfer(handle, pipe_ptr, tr_ptr);
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_send_setup
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to send setup data
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_send_setup(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr) {
    return _usb_khci_transfer(handle, pipe_ptr, tr_ptr);
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_recv
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to receive data
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_recv(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr) {
    return _usb_khci_transfer(handle, pipe_ptr, tr_ptr);
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_cancel
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to cancel the transfer
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_cancel(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr, TR_STRUCT_PTR tr_ptr) {
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR) handle;

    TR_QUEUE_STRUCT_PTR tr_que_ptr;
    /* todo, wating schedule_tr_ptr become NULL  */
    switch (pipe_ptr->PIPETYPE) {
        case USB_ISOCHRONOUS_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[iso_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[iso_out_tr_queue];
            break;
        case USB_INTERRUPT_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[intr_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[intr_out_tr_queue];
            break;
        case USB_CONTROL_PIPE:
            tr_que_ptr = usb_host_ptr->schdule_queues[ctrl_tr_queue];
            break;
        case USB_BULK_PIPE:
            if(pipe_ptr->DIRECTION == USB_RECV)
                tr_que_ptr = usb_host_ptr->schdule_queues[bulk_in_tr_queue];
            else
                tr_que_ptr = usb_host_ptr->schdule_queues[bulk_in_tr_queue];
            break;
        default:
            ASSERT(0); // fail pipetype
            break;
    }

    /* how to cancle the schedule_tr_ptr (current transfer) todo */
   
    _usb_khci_process_tr_complete((KHCI_PIPE_STRUCT_PTR)pipe_ptr,(KHCI_TR_STRUCT_PTR)tr_ptr,0,0,KHCI_ATOM_TR_CANCEL);

    if(usb_host_ptr->schedule_tr_ptr->pipe_tr == (KHCI_TR_STRUCT_PTR)tr_ptr)
        printf("cancel pending tr\n");
    
    if(cancel_schdule_tr_que(tr_que_ptr,(KHCI_TR_STRUCT_PTR)tr_ptr))  /*  judge only with tr_ptr ,don't care pipe_ptr */
         return USB_OK;

    return USBERR_ERROR; 

}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_bus_control
*  Returned Value : error or USB_OK
*  Comments       :
*        The function for USB bus control
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_bus_control(_usb_host_handle handle, uint_8 bus_control) {
    
    USB_KHCI_HOST_STATE_STRUCT_PTR  usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR) handle;
    
    return USB_OK;
}

/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_process_tr_complete
*  Returned Value : none
*  Comments       :
*        Transaction complete
*END*-----------------------------------------------------------------*/
static /*void*/int _usb_khci_process_tr_complete(
    KHCI_PIPE_STRUCT_PTR       pipe_desc_ptr,
    KHCI_TR_STRUCT_PTR         pipe_tr_ptr,
    uint_32                    required,
    uint_32                    remaining,
    int_32                     err
)
{
    uchar_ptr buffer_ptr = NULL;
    uint_32 status = 0;
    int tr_cb_status;
    
    KHCI_DEBUG_LOG('o', TR_MSG_UNKNOWN, pipe_desc_ptr->G.ENDPOINT_NUMBER, err)

    unsigned int  in_out = pipe_desc_ptr->G.DIRECTION;
    // unsigned int  max_packet_size = pipe_desc_ptr->G.MAX_PACKET_SIZE;
        
    if (err == KHCI_ATOM_TR_STALL) {
        status = USBERR_ENDPOINT_STALLED;
    }
    else if ((err == KHCI_ATOM_TR_NAK) || (err >= 0)) {
        status = USB_OK;
        
        //if ((remaining > 0) || (err == KHCI_ATOM_TR_NAK)) //remaining bytes
        //    status = USBERR_TR_FAILED;
        if (((remaining > 0) && (pipe_desc_ptr->G.PIPETYPE != USB_CONTROL_PIPE) && (in_out == USB_SEND) && 
            ((pipe_tr_ptr->G.FLAG & USB_SHORT_TRANSFER) == 0)) ||
            (err == KHCI_ATOM_TR_NAK)) { /* except ctrl endpoint ,all other in-endpoint are admited transfer short packet */
            status = USBERR_TR_FAILED;
        }

        if (pipe_desc_ptr->G.PIPETYPE == USB_CONTROL_PIPE) {
            if (pipe_tr_ptr->G.SEND_PHASE) {
                buffer_ptr = pipe_tr_ptr->G.TX_BUFFER;
                pipe_tr_ptr->G.SEND_PHASE = FALSE;
            }
            else {
                buffer_ptr = pipe_tr_ptr->G.RX_BUFFER;
            }
        }
        else {
            if (pipe_desc_ptr->G.DIRECTION) {
                buffer_ptr = pipe_tr_ptr->G.TX_BUFFER;
            }
            else {
                buffer_ptr = pipe_tr_ptr->G.RX_BUFFER;
            }
        }
    }
    else if (err < 0)
        status = USBERR_TR_FAILED;
    
    pipe_tr_ptr->status = USB_STATUS_IDLE;   /* use as khci interal status */
    
    if (pipe_tr_ptr->status == USB_STATUS_IDLE) {
        /* Transfer done. Call the callback function for this 
        ** transaction if there is one (usually true). 
        */
        tr_cb_status = TR_CALLBACK_COMPLETE;   /* if callback is null ,set it always is complete */
        if (pipe_tr_ptr->G.CALLBACK != NULL) {
            tr_cb_status = pipe_tr_ptr->G.CALLBACK((pointer)pipe_desc_ptr, pipe_tr_ptr->G.CALLBACK_PARAM,
                buffer_ptr, required - remaining, status);
                
            /* If the application enqueued another request on this pipe 
            ** in this callback context then it will be at the end of the list
            */
        }
    }

    if(tr_cb_status == TR_CALLBACK_COMPLETE ) {
        /* don't use pipe anymore */
        pipe_tr_ptr->G.TR_INDEX = 0;

        // liutest add for next use this tr mem (see _usb_host_get_tr_element )
        pipe_tr_ptr->pos = 0;
        pipe_tr_ptr->retry_cnt = 0; //pipe_desc_ptr->G.NAK_COUNT;   set retry count at enqueue transfer
        pipe_tr_ptr->status = 0;
        pipe_tr_ptr->tr_error = 0;
        /* pipe_tr_ptr->next_frame = 0; */
    }

    return tr_cb_status;

}

static USB_STATUS _usb_khci_open_pipe ( _usb_host_handle handle,  PIPE_STRUCT_PTR pipe_ptr )
{
    USB_KHCI_HOST_STATE_STRUCT_PTR  usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR)handle;
    
    switch(pipe_ptr->PIPETYPE) {
        case USB_ISOCHRONOUS_PIPE:
            // ASSERT(pipe_ptr->MAX_PACKET_SIZE);
            //if(pipe_ptr->MAX_PACKET_SIZE == 0)
            //    break;
            if(pipe_ptr->DIRECTION == USB_RECV) {
                usb_host_ptr->schdule_queues[iso_in_tr_queue] = new_schdule_tr_que(ISO_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[iso_in_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
           }
           else {
                usb_host_ptr->schdule_queues[iso_out_tr_queue] = new_schdule_tr_que(ISO_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[iso_out_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
           }
           break;
        case USB_INTERRUPT_PIPE:
           if(pipe_ptr->DIRECTION == USB_RECV) {
                usb_host_ptr->schdule_queues[intr_in_tr_queue] = new_schdule_tr_que(INTR_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[intr_in_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
           }
           else {
                usb_host_ptr->schdule_queues[intr_out_tr_queue] = new_schdule_tr_que(INTR_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[intr_out_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
           }
           break;
        case USB_CONTROL_PIPE:
            if(!usb_host_ptr->schdule_queues[ctrl_tr_queue]) {
                usb_host_ptr->schdule_queues[ctrl_tr_queue] = new_schdule_tr_que(CTRL_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[ctrl_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
            }
            break;
        case USB_BULK_PIPE:
            if(pipe_ptr->DIRECTION == USB_SEND) {
                //if(!usb_host_ptr->schdule_queues[bulk_out_tr_queue]) { /* bulk in and out fifo */
                    usb_host_ptr->schdule_queues[bulk_out_tr_queue] = new_schdule_tr_que(BULK_TR_QUEUE_SIZE);
                    usb_host_ptr->schdule_queues[bulk_out_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
                //}
            }
            else {
                usb_host_ptr->schdule_queues[bulk_in_tr_queue] = new_schdule_tr_que(BULK_TR_QUEUE_SIZE);
                usb_host_ptr->schdule_queues[bulk_in_tr_queue]->INTERVAL = pipe_ptr->INTERVAL;
            }
            break;
        default:
            ASSERT(0);
    }

    //-printf("open pipe type %d, direct %d, interval %d\n",pipe_ptr->PIPETYPE, pipe_ptr->DIRECTION ,pipe_ptr->INTERVAL);
    
    return USB_OK;
}



/*FUNCTION*-------------------------------------------------------------
*
*  Function Name  : _usb_khci_host_close_pipe
*  Returned Value : error or USB_OK
*  Comments       :
*        The function to close pipe
*END*-----------------------------------------------------------------*/
USB_STATUS _usb_khci_host_close_pipe(_usb_host_handle handle, PIPE_STRUCT_PTR pipe_ptr) {

    USB_KHCI_HOST_STATE_STRUCT_PTR  usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR)handle;
    uint_32 que_num = 0;
    
    switch(pipe_ptr->PIPETYPE) {
        case USB_ISOCHRONOUS_PIPE:
            if(pipe_ptr->DIRECTION == USB_SEND) {
                que_num = iso_in_tr_queue;
           }
           else {
                que_num = iso_out_tr_queue;
           }
           break;
        case USB_INTERRUPT_PIPE:
           if(pipe_ptr->DIRECTION == USB_SEND) {
                que_num = intr_in_tr_queue;
           }
           else {
                que_num = intr_in_tr_queue;
           }
           break;
        case USB_CONTROL_PIPE:
            //if(usb_host_ptr->schdule_queues[ctrl_tr_queue]) {
                que_num = ctrl_tr_queue;
            //}
            break;
        case USB_BULK_PIPE:
            if(pipe_ptr->DIRECTION == USB_SEND) {
                que_num = bulk_out_tr_queue;
            }
            else {
                que_num = bulk_in_tr_queue;
            }
            break;
        default:
            ASSERT(0);
    }

    // todo complete the tr with error
    delete_schdule_tr_que(usb_host_ptr->schdule_queues[que_num]);
    usb_host_ptr->schdule_queues[que_num] = NULL;

    return USB_OK;
}


/* liutest start schedule khci */
#if 1
uint_8 buf_tmp[64];
static int_32 _usb_khci_dispatch_transation(
    USB_KHCI_HOST_STATE_STRUCT_PTR  usb_host_ptr,
    uint_32                    type,
    KHCI_PIPE_STRUCT_PTR       pipe_desc_ptr,
    // KHCI_TR_STRUCT_PTR  tr_prt
    uint_8                     *buf,
    uint_32                    len
)
{
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;
    //uint_32 bd;
    uint_32 *bd_ptr = NULL;
    int_32 res = 0;

    ASSERT(((uint32_t)buf % /*4*/2) == 0);
    /*
    buf = tr_prt->G.RX_BUFFER; */
    len = (len > pipe_desc_ptr->G.MAX_PACKET_SIZE) ? pipe_desc_ptr->G.MAX_PACKET_SIZE : len;
    
    // usb_host_ptr->dispatched_tr = tr_prt; // This function must lock when it called by more than one task these priority higher than it.
    
    /* ADDR must be written before ENDPT0 (undocumented behavior) for to generate PRE packet */
    usb_ptr->ADDR = (uint_8)((pipe_desc_ptr->G.SPEED == USB_SPEED_FULL) ? USB_ADDR_ADDR(pipe_desc_ptr->G.DEVICE_ADDRESS) : USB_ADDR_LSEN_MASK | USB_ADDR_ADDR(pipe_desc_ptr->G.DEVICE_ADDRESS));
    
    if((pipe_desc_ptr->G.PIPETYPE != USB_ISOCHRONOUS_PIPE)) {
        usb_ptr->ENDPOINT[0].ENDPT =
            (usb_host_ptr->G.SPEED == USB_SPEED_LOW ? USB_ENDPT_HOSTWOHUB_MASK : 0) | USB_ENDPT_RETRYDIS_MASK |
            USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPHSHK_MASK;
    }
    else {
        usb_ptr->ENDPOINT[0].ENDPT =
            (usb_host_ptr->G.SPEED == USB_SPEED_LOW ? USB_ENDPT_HOSTWOHUB_MASK : 0) | USB_ENDPT_RETRYDIS_MASK |
            USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPRXEN_MASK;
    }

    // wait for USB ready, but with timeout
    /* while (usb_ptr->CTL & USB_CTL_TXSUSPENDTOKENBUSY_MASK) {
        if (_lwevent_wait_ticks(&usb_host_ptr->khci_event, KHCI_EVENT_MASK, FALSE, 1) == MQX_OK) {
            res = KHCI_ATOM_TR_RESET;
            break;
        }
    }*/
    //ASSERT(!(usb_ptr->CTL & USB_CTL_TXSUSPENDTOKENBUSY_MASK));  /* we called this dispatch after tokenden so bus must free*/
    while(usb_ptr->CTL & USB_CTL_TXSUSPENDTOKENBUSY_MASK);
    // all is ok, do transaction

#ifdef KHCICFG_BASIC_SCHEDULING
    //usb_ptr->ISTAT |= USB_ISTAT_SOFTOK_MASK; //clear SOF
    //while (!(usb_ptr->ISTAT & USB_ISTAT_SOFTOK_MASK))
    /* wait for next SOF */;

    //-usb_ptr->SOFTHLD = 0;
    //usb_ptr->SOFTHLD = len * 7 / 6 + KHCICFG_THSLD_DELAY;
#else
    if (pipe_desc_ptr->G.SPEED == USB_SPEED_FULL)
        usb_ptr->SOFTHLD = len * 7 / 6 + KHCICFG_THSLD_DELAY;
    else
        usb_ptr->SOFTHLD = len * 12 * 7 / 6 + KHCICFG_THSLD_DELAY;
#endif          
    //usb_ptr->ISTAT |= USB_ISTAT_SOFTOK_MASK; //clear SOF
    usb_ptr->ERRSTAT = 0xff; //clear error status

    switch (type) {
    case TR_CTRL:    
        bd_ptr = (uint_32*) BD_PTR(0, 1, usb_host_ptr->tx_bd);
            /* fixed aligned issue */
#ifdef FIX_KHCI_ALIGN_ISSUE            
            if((((uint_32)buf) & 0x3) && (pipe_desc_ptr->G.PIPETYPE == USB_BULK_PIPE) && (pipe_desc_ptr->G.DIRECTION == USB_RECV))
            *(bd_ptr + 1) = HOST_TO_LE_LONG((uint_32)buf_tmp);
            else
#endif                
        *(bd_ptr + 1) = HOST_TO_LE_LONG((uint_32)buf);
        *bd_ptr = HOST_TO_LE_LONG(USB_BD_BC(len) | USB_BD_OWN);
        usb_ptr->TOKEN = (uint_8)(USB_TOKEN_TOKENENDPT((uint_8)pipe_desc_ptr->G.ENDPOINT_NUMBER) | USB_TOKEN_TOKENPID_SETUP);
        usb_host_ptr->tx_bd ^= 1;
        break;
    case TR_IN:
        bd_ptr = (uint_32*) BD_PTR(0, 0, usb_host_ptr->rx_bd);
        *(bd_ptr + 1) = HOST_TO_LE_LONG((uint_32)buf);
        *bd_ptr = HOST_TO_LE_LONG(USB_BD_BC(len) | USB_BD_OWN | USB_BD_DATA01(pipe_desc_ptr->G.NEXTDATA01));
        usb_ptr->TOKEN = (uint_8)(USB_TOKEN_TOKENENDPT(pipe_desc_ptr->G.ENDPOINT_NUMBER) | USB_TOKEN_TOKENPID_IN);
        usb_host_ptr->rx_bd ^= 1;
        break;
    case TR_OUT:
        bd_ptr = (uint_32*) BD_PTR(0, 1, usb_host_ptr->tx_bd);
        *(bd_ptr + 1) = HOST_TO_LE_LONG((uint_32)buf);
        *bd_ptr = HOST_TO_LE_LONG(USB_BD_BC(len) | USB_BD_OWN | USB_BD_DATA01(pipe_desc_ptr->G.NEXTDATA01));
        usb_ptr->TOKEN = (uint_8)(USB_TOKEN_TOKENENDPT(pipe_desc_ptr->G.ENDPOINT_NUMBER) | USB_TOKEN_TOKENPID_OUT);
        usb_host_ptr->tx_bd ^= 1;
        break;
    default:
        bd_ptr = NULL;
    }

    usb_host_ptr->last_bd_ptr = bd_ptr;
    return res;
}

static inline int _usb_khci_do_transfer(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr)
{
    SCHED_TR_STRUCT_PTR    sch_tr_ptr =  usb_host_ptr->schedule_tr_ptr;
    uint_32                    type;
    // KHCI_PIPE_STRUCT_PTR       pipe_desc_ptr;
    uint_8                     * buf = NULL;
    uint_32                    len = 0;

    //ASSERT(sch_tr_ptr);
    if(sch_tr_ptr == NULL) { /* */
        printf("_usb_khci_do_transfer fail: sch_tr_ptr is NULL\n");
        return 0;
    }
    
    switch (sch_tr_ptr->pipe_desc->G.PIPETYPE)
    {
        case USB_CONTROL_PIPE:
        {
            switch(sch_tr_ptr->pipe_tr->status)
            {
                case TRF_SETUP_QUEUE:
                    type = TR_CTRL;
                    buf = (uint_8 *) &(sch_tr_ptr->pipe_tr->G.setup_packet);
                    len = 8;
                    /*
                    if (sch_tr_ptr->pipe_tr->G.RX_LENGTH > 0)
`                    sch_tr_ptr->pipe_tr->status = TRF_SETUP_DTOH;
                    else if(sch_tr_ptr->pipe_tr->G.TX_LENGTH > 0)
                        sch_tr_ptr->pipe_tr->status = TRF_SETUP_HTOD;
                    else
                        sch_tr_ptr->pipe_tr->status = TRF_SETUP_DTOH_OUT;
                    */
                    break;
                case TRF_SETUP_DTOH:  // start ctrl read
                    type = TR_IN;
                    buf = sch_tr_ptr->pipe_tr->G.RX_BUFFER;
                    len = sch_tr_ptr->pipe_tr->G.RX_LENGTH;
                    // sch_tr_ptr->pipe_tr->status = TRF_SETUP_HTOD_IN;  // update state at token done
                    break;
                
                case TRF_SETUP_HTOD:  // start ctrl write
                    type = TR_OUT;
                    buf = sch_tr_ptr->pipe_tr->G.TX_BUFFER;
                    len = sch_tr_ptr->pipe_tr->G.TX_LENGTH;
                    break;
                case TRF_SETUP_DTOH_IN:  // continue ctrl read
                    type = TR_IN;
                    buf =  sch_tr_ptr->pipe_tr->G.RX_BUFFER +  sch_tr_ptr->pipe_tr->pos;
                    len = sch_tr_ptr->pipe_tr->G.RX_LENGTH - sch_tr_ptr->pipe_tr->pos;
                    break;
                case TRF_SETUP_HTOD_OUT:  // continue ctrl write
                    type = TR_OUT;
                    buf = sch_tr_ptr->pipe_tr->G.TX_BUFFER +  sch_tr_ptr->pipe_tr->pos;
                    len = sch_tr_ptr->pipe_tr->G.TX_LENGTH - sch_tr_ptr->pipe_tr->pos;
                     break;
                case TRF_SETUP_DTOH_OUT:  // ctrl read ack out
                    sch_tr_ptr->pipe_desc->G.NEXTDATA01 = 1;
                    type = TR_OUT;
                     break;
                case TRF_SETUP_HTOD_IN:  // ctrl write ack in
                    sch_tr_ptr->pipe_desc->G.NEXTDATA01 = 1;
                    type = TR_IN;
                    break;
             }
        }
        break;
        
        case USB_BULK_PIPE:
        case USB_ISOCHRONOUS_PIPE:
        case USB_INTERRUPT_PIPE:
            if(sch_tr_ptr->pipe_desc->G.DIRECTION == USB_SEND) {
                if (sch_tr_ptr->pipe_tr->status == TRF_OUT_QUEUE) {
                    type = TR_OUT;
                    buf = sch_tr_ptr->pipe_tr->G.TX_BUFFER;
                    len = sch_tr_ptr->pipe_tr->G.TX_LENGTH;
                }
                else if(sch_tr_ptr->pipe_tr->status == TRF_OUT)  {
                    type = TR_OUT;
                    buf = sch_tr_ptr->pipe_tr->G.TX_BUFFER +  sch_tr_ptr->pipe_tr->pos;
                    len = sch_tr_ptr->pipe_tr->G.TX_LENGTH -  sch_tr_ptr->pipe_tr->pos;
                    if(sch_tr_ptr->pipe_desc->G.PIPETYPE == USB_BULK_PIPE)
                        type = TR_OUT;
                }
            }
            else {
                if (sch_tr_ptr->pipe_tr->status == TRF_IN_QUEUE) {
                    type = TR_IN;
                    buf = sch_tr_ptr->pipe_tr->G.RX_BUFFER;
                    len = sch_tr_ptr->pipe_tr->G.RX_LENGTH;
                }
                else if(sch_tr_ptr->pipe_tr->status == TRF_IN)  {
                    type = TR_IN;
                    buf = sch_tr_ptr->pipe_tr->G.RX_BUFFER +  sch_tr_ptr->pipe_tr->pos;
                    len = sch_tr_ptr->pipe_tr->G.RX_LENGTH -  sch_tr_ptr->pipe_tr->pos;
                }
            }
            break;

        default:
            ASSERT(0);
    }

    return _usb_khci_dispatch_transation(usb_host_ptr, type, sch_tr_ptr->pipe_desc, buf,len);
}

/* validate */
static inline boolean validate_schedule_que(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int que,/* uint32 last_tr_frame,*/ int packet_size)
{
    switch(que)
    {
        case iso_in_tr_queue:
        case iso_out_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_ISO_TR_MAX_BANDWIDTH));

        case intr_in_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_INTRIN_MAX_BANDWIDTH));
        case intr_out_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_INTROUT_MAX_BANDWIDTH));
        case ctrl_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_CTRL_MAX_BANDWIDTH));
        case bulk_in_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_BULKIN_MAX_BANDWIDTH));
        case bulk_out_tr_queue:
            return ((usb_host_ptr->free_bandwidth - packet_size) >= (KHCI_MAX_BANDWIDTH - KHCI_BULKOUT_MAX_BANDWIDTH));
         
    }
    
    return FALSE;
}

// static inline validate_free_bandwidth(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int tr_size)
static inline int get_usb_packet_size(SCHED_TR_STRUCT_PTR sch_tr_ptr,int tr_size)
{
    // todo added header size
    // token_length 5 (34bit) + (packet_length + header 2 + CRC 2 )x7/6 + 3 (18bit)
    return 5 + (tr_size + 4) * 7 /6 + 3;
}

static inline int get_transaction_size(SCHED_TR_STRUCT_PTR sch_tr_ptr/* ,int sched_state*/)
{
    int tr_size = 0;
    if (( sch_tr_ptr->pipe_desc->G.PIPETYPE == USB_CONTROL_PIPE ) &&
           ( sch_tr_ptr->pipe_tr->status == TRF_SETUP_QUEUE)) {
            tr_size = 8;
    }        
    else {
        if( sch_tr_ptr->pipe_desc->G.DIRECTION == USB_SEND ) {
            tr_size = (sch_tr_ptr->pipe_tr->G.TX_LENGTH - sch_tr_ptr->pipe_tr->pos);
        }
        else {
            tr_size = (sch_tr_ptr->pipe_tr->G.RX_LENGTH - sch_tr_ptr->pipe_tr->pos);
        }
    }
    
    tr_size = tr_size > sch_tr_ptr->pipe_desc->G.MAX_PACKET_SIZE ? sch_tr_ptr->pipe_desc->G.MAX_PACKET_SIZE : tr_size;
    
    return tr_size;

}

boolean get_next_schedule_transfer(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr)
{
    int que, tr_size;
    SCHED_TR_STRUCT_PTR sched_tr_ptr = NULL;
    TR_QUEUE_STRUCT_PTR  schdule_queue = NULL;

    ASSERT(usb_host_ptr->schedule_state >= 0);

    SCHED_QUE_LOCK(usb_host_ptr->que_lock);
    for(que = usb_host_ptr->schedule_state; que < shced_queue_size ;que ++) {  /* judge schdule which que ? */
        schdule_queue = usb_host_ptr->schdule_queues[que];
        if ((!schdule_queue) || 
            (( que <= period_queue )  /* interrupt can only send one transfer per farme, and iso pipe send one transaction per frame */
            && (schdule_queue->LFRAME) && (schdule_queue->LFRAME + schdule_queue->INTERVAL >/* use "!= " is too exactly */ usb_host_ptr->frame_num)) /* liutest judging the not equal  maybe is correctly ?  */
           ) {
                if(schdule_queue) {
                    //if( (schdule_queue->LFRAME + schdule_queue->INTERVAL > usb_host_ptr->frame_num))
                    //    printf("error period , last send frame %d, interver %d, current frame %d\n",schdule_queue->LFRAME,schdule_queue->INTERVAL,usb_host_ptr->frame_num);
                }
                
                continue;
            }
        
        while ((!sched_tr_ptr) && (schdule_queue->COUNT > 0)) {
            sched_tr_ptr = get_pending_schdule_tr_que(schdule_queue);  /* get this que first item */
            if(!sched_tr_ptr->pipe_tr) {
                printf("dequeue cancel tr\n");
                dequeue_schdule_tr_que(schdule_queue);   /* dequeue canceled tr ,don't callback complete function */
            }
            else
                break;
        }

        if(sched_tr_ptr) {
            tr_size = get_transaction_size(sched_tr_ptr);
            if(validate_schedule_que(usb_host_ptr,que,get_usb_packet_size(sched_tr_ptr,tr_size)))
                break;
            sched_tr_ptr = NULL;   /* found but is not valid bandwidth */
        }

    }

    if(sched_tr_ptr) { /* find next queue */
        tr_size = get_transaction_size(sched_tr_ptr);
        //ASSERT(tr_size !=0);
        if(HAS_FREE_BANDWIDTH(sched_tr_ptr->pipe_desc->G.DIRECTION,
            get_usb_packet_size(sched_tr_ptr,tr_size),usb_host_ptr->free_bandwidth)) {  // iso max_packet size set to 512
            schdule_queue->LFRAME = usb_host_ptr->frame_num;
            //ASSERT(usb_host_ptr->schedule_tr_ptr);
            usb_host_ptr->schedule_tr_ptr = sched_tr_ptr;
            usb_host_ptr->schedule_state = que;
        }
        SCHED_QUE_UNLOCK(usb_host_ptr->que_lock);
        return TRUE;
    }
    
    SCHED_QUE_UNLOCK(usb_host_ptr->que_lock);

    return   FALSE;
}

static inline boolean update_free_bandwidth(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr)
{
    MQX_TICK_STRUCT cur_tick,diff_tick;
    uint32_t cur_frame_num = _usb_khci_get_total_frame_count((_usb_host_handle)usb_host_ptr);
    //return TRUE;
    
    if( cur_frame_num != usb_host_ptr->frame_num) {
        //usb_host_ptr->free_bandwidth = 0;
        return FALSE;
    }
#ifdef SCHEDULING_CHECK_TIME
    _time_get_elapsed_ticks_fast(&cur_tick);
    _time_diff_ticks(&cur_tick, &usb_host_ptr->sof_start_tick, &diff_tick);

    // ASSERT(diff_tick.TICKS[0] == 0);
    if(diff_tick.TICKS[0] > 0) {
        //printf("elapsed many frames\n");
        return FALSE;
    }
    // ASSERT(usb_host_ptr->one_frame_tick >= diff_tick.HW_TICKS);
    if(usb_host_ptr->one_frame_tick < diff_tick.HW_TICKS) {
        //printf("calirated frame tick %d ,diff hwtick %d\n", usb_host_ptr->one_frame_tick , diff_tick.HW_TICKS);
        // diff_tick.HW_TICKS = diff_tick.HW_TICKS;
        // ASSERT(usb_host_ptr->one_frame_tick >= diff_tick.HW_TICKS);
        return FALSE;
    }

    usb_host_ptr->free_bandwidth = ( usb_host_ptr->one_frame_tick - diff_tick.HW_TICKS)  \
                        * KHCI_MAX_BANDWIDTH /usb_host_ptr->one_frame_tick;
#else
#ifdef SCHEDULING_CHECK_TRANSFERED
    usb_host_ptr->free_bandwidth = KHCI_MAX_BANDWIDTH > usb_host_ptr->this_frame_transfered ? \
                         KHCI_MAX_BANDWIDTH - usb_host_ptr->this_frame_transfered : 0;
#endif
#endif
    //printf("%d-%d\n",usb_host_ptr->free_bandwidth,usb_host_ptr->schedule_state);
    //if(usb_host_ptr->free_bandwidth <= 16)  return FALSE;
    //printf("%d\n",usb_host_ptr->free_bandwidth);
    
    return TRUE;
    
}        

void strat_schdule_this_frame(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr)
{
    // handle the last error 
    //static int testframe = 0;

    //if( usb_host_ptr->schedule_tr_ptr != NULL)
    //    printf("$\n");
    
    // init and start schdule in this frame
    usb_host_ptr->schedule_tr_ptr = NULL;
     usb_host_ptr->max_bandwidth = KHCI_MAX_BANDWIDTH;
     usb_host_ptr->free_bandwidth = KHCI_MAX_BANDWIDTH;
     usb_host_ptr->schedule_state = iso_in_tr_queue;
#ifdef SCHEDULING_CHECK_TRANSFERED
     usb_host_ptr->this_frame_transfered = 0;
#endif
     //_time_get_elapsed_ticks_fast(&usb_host_ptr->sof_start_tick);

     /* get first tr and start to do trasaction */
     // update_free_bandwidth(usb_host_ptr);
     if(get_next_schedule_transfer(usb_host_ptr)) {
        _usb_khci_do_transfer(usb_host_ptr);
     }
     //testframe ++;
}

/* excetp ctrl endpoint */
static inline boolean transfer_is_complete(KHCI_TR_STRUCT_PTR pipe_tr, KHCI_PIPE_STRUCT_PTR pipe_desc,uint_32 total_len ,uint_32 res)
{
    unsigned int  in_out = pipe_desc->G.DIRECTION;
    unsigned int  max_packet_size = pipe_desc->G.MAX_PACKET_SIZE;
    if ((in_out == USB_SEND) &&  ((pipe_tr->G.FLAG & USB_SHORT_TRANSFER) == 0)) {
        if( total_len == pipe_tr->pos )
            return TRUE;
        else
            return FALSE;
    }
    else { /* InPipe transfer with short packet always is valid. (The best is set USB_SHORT_TRANSFER flag at called transfer function.) */
        //if ((pipe_desc->G.PIPETYPE == USB_INTERRUPT_PIPE) && (res == pipe_desc->G.MAX_PACKET_SIZE))
        //	in_out = pipe_desc->G.DIRECTION;
        
        if ( (res < max_packet_size) || ( total_len == pipe_tr->pos ))
            return TRUE;
        else
            return FALSE;
    }
}
    
static inline void handle_ctrl_transaction_done(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int res)
{
    //KHCI_PIPE_STRUCT_PTR pipe_desc;
    uint_32     total_len,pos,tr_size;
    uint_32     state_step = 0;
    uint_32     ctrl_transaction_fail = 0;
   
    KHCI_TR_STRUCT_PTR pipe_tr = usb_host_ptr->schedule_tr_ptr->pipe_tr;    // point to transaction
    KHCI_PIPE_STRUCT_PTR pipe_desc =  usb_host_ptr->schedule_tr_ptr->pipe_desc;

    if(pipe_tr->G.RX_LENGTH > 0)
        total_len = pipe_tr->G.RX_LENGTH;
    else if (pipe_tr->G.TX_LENGTH > 0)
        total_len = pipe_tr->G.TX_LENGTH;
    else
        total_len = 0;
    // else if ((pipe_tr->status != TRF_SETUP_QUEUE) &&  (pipe_tr->status != TRF_SETUP_DTOH_OUT ))
    //    ASSERT(0);

    pos = pipe_tr->pos;

    //if(res <= 0)
        //printf("ctrl transaction fail %d, ctrl tr state %d ,retry %d\n",res, pipe_tr->status,pipe_tr->retry_cnt);

    switch(pipe_tr->status)
    {
        case TRF_SETUP_QUEUE:
            if(res > 0 /*== 8*/) {
                ASSERT(res == 8);
                if(pipe_tr->G.RX_LENGTH > 0)
                    pipe_tr->status = TRF_SETUP_DTOH;
                else if (pipe_tr->G.TX_LENGTH > 0)
                    pipe_tr->status = TRF_SETUP_HTOD;
                else 
                    pipe_tr->status = TRF_SETUP_HTOD_IN;
            }
            else {
                total_len = 8;
                pos = 0;
                ctrl_transaction_fail = 1;
            }
            break;
        case TRF_SETUP_DTOH:  // start ctrl read
        case TRF_SETUP_HTOD:  // start ctrl write
            if( res >= 0 ) {
                pipe_tr->pos += res;
                if(pipe_tr->pos == total_len)
                    state_step = 2;
                else
                    state_step = 1;
                pipe_tr->status += state_step;
            }
            else {
                //pos = pipe_tr->pos;
                ctrl_transaction_fail = 1;
            }
            break;
        case TRF_SETUP_DTOH_IN:  // continue ctrl read
        case TRF_SETUP_HTOD_OUT:  // continue ctrl write
            if( res >= 0 ) {
                pipe_tr->pos += res;
                if(pipe_tr->pos == total_len)
                    pipe_tr->status ++;    /*  ack status */
            }
            else {
                ctrl_transaction_fail = 1;
            }
            break;
        case TRF_SETUP_DTOH_OUT:  // ctrl read ack out
        case TRF_SETUP_HTOD_IN:     // ctrl write ack in
            if( res >=0 ) {                  /* ack  is zero packet */
                pipe_tr->status =  TRF_SETUP_COMPLETE;    /*  ack status */
                /* setup transfer successed !!! */
                // remove ,because _usb_khci_process_tr_complete will set pipe_tr->status to zero
                // _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, total_len - pos, /* res */0);
                // dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
                // usb_host_ptr->schedule_tr_ptr = NULL;
                 }
            else
                ctrl_transaction_fail = 1;
            break;
     }


    if(ctrl_transaction_fail) {
#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,NAK_BANDWIDTH);
#endif

        if ((res != KHCI_ATOM_TR_NAK) && (res != KHCI_ATOM_TR_TO ) && (res != KHCI_ATOM_TR_EOF_ERROR)) {

            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, total_len - pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else {  /* if this transfer nak then retry at next frame !!! */
            if(res == KHCI_ATOM_TR_EOF_ERROR)
                    return;

            if(pipe_tr->retry_cnt) {
                pipe_tr->retry_cnt --;
            }
            else {
                pipe_tr->retry_cnt = usb_host_ptr->schedule_tr_ptr->pipe_desc->G.NAK_COUNT;    /* reset nak retry at next frame */
                if(usb_host_ptr->schedule_state + 1 == shced_queue_size )
                    return;
                else
                    usb_host_ptr->schedule_state += 1;
            }
        }
    }
    else {
        
        // update_free_bandwidth(usb_host_ptr);

#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,res);
#endif
 
        if(pipe_tr->status !=  TRF_SETUP_COMPLETE) {
            // continue this ctrl transfer
            ASSERT(usb_host_ptr->schedule_tr_ptr);
            tr_size = get_transaction_size(usb_host_ptr->schedule_tr_ptr);
            if(update_free_bandwidth(usb_host_ptr)) {
                if(HAS_FREE_BANDWIDTH(usb_host_ptr->schedule_tr_ptr->pipe_desc->G.DIRECTION,
                    get_usb_packet_size(usb_host_ptr->schedule_tr_ptr,tr_size),usb_host_ptr->free_bandwidth))
                    _usb_khci_do_transfer(usb_host_ptr);
            }
            return;  
        }
        else {
            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, total_len - pos, /* res */0);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
    }

    // always judge band width and do next transfer  (setup nak or other error, or setup complete do next transfer !)
    if(update_free_bandwidth(usb_host_ptr)) {
        if(get_next_schedule_transfer(usb_host_ptr))
            _usb_khci_do_transfer(usb_host_ptr);
    }

}

static inline void handle_intr_transaction_done(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int res)
{
    //KHCI_PIPE_STRUCT_PTR pipe_desc;
    uint_32     total_len = 0;
    uint_32      tr_size;      /* transaction size */
   
    KHCI_TR_STRUCT_PTR pipe_tr = usb_host_ptr->schedule_tr_ptr->pipe_tr;    // pointer to transaction
    KHCI_PIPE_STRUCT_PTR pipe_desc =  usb_host_ptr->schedule_tr_ptr->pipe_desc;

    total_len = (pipe_tr->G.TX_LENGTH > 0) ? pipe_tr->G.TX_LENGTH :  ((pipe_tr->G.RX_LENGTH > 0) ? pipe_tr->G.RX_LENGTH : 0);

    if(res >= 0) {
        pipe_tr->pos += res;


#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,res);
#endif

        if (transfer_is_complete(pipe_tr,pipe_desc,total_len,res)) {  /* transfer complete */
            // do complete callback
            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, 
                                                    total_len- pipe_tr->pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else {  // continue this transfer
            // judge band-width
            if (pipe_desc->G.DIRECTION == USB_RECV) {
                pipe_tr->status = TRF_IN;
            }
            else {
                pipe_tr->status = TRF_OUT;
            }
            tr_size = get_transaction_size(usb_host_ptr->schedule_tr_ptr);
            if(update_free_bandwidth(usb_host_ptr)){
                if(HAS_FREE_BANDWIDTH(usb_host_ptr->schedule_tr_ptr->pipe_desc->G.DIRECTION,get_usb_packet_size(usb_host_ptr->schedule_tr_ptr,tr_size),
                                    usb_host_ptr->free_bandwidth))
                    _usb_khci_do_transfer(usb_host_ptr);
             }
            return;
        }
    }
    else {  // handle transaction err
        // pipe_tr->status = res;

#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,NAK_BANDWIDTH);
#endif

        if ((res != KHCI_ATOM_TR_NAK) && (res != KHCI_ATOM_TR_TO ) && (res != KHCI_ATOM_TR_EOF_ERROR)) {
            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len,
                                            total_len- pipe_tr->pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else { /* if the in pipe nak then always retry ,until cancel it */

            if(res == KHCI_ATOM_TR_EOF_ERROR)
                    return;
                        
            if(pipe_desc->G.PIPETYPE == USB_INTERRUPT_PIPE)
                ASSERT(pipe_tr->retry_cnt == 0);

            if(usb_host_ptr->schedule_state + 1 == shced_queue_size )
                return;
            else
                usb_host_ptr->schedule_state += 1;
        }
    }

    // judge band width and do next transfer
    if(update_free_bandwidth(usb_host_ptr)) {
        if(get_next_schedule_transfer(usb_host_ptr))
            _usb_khci_do_transfer(usb_host_ptr);
    }

}

/* only for test */
static inline void handle_bulk_transaction_done(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int res)
{
    //KHCI_PIPE_STRUCT_PTR pipe_desc;
    uint_32     total_len = 0;
    uint_32      tr_size;      /* transaction size */
   
    KHCI_TR_STRUCT_PTR pipe_tr = usb_host_ptr->schedule_tr_ptr->pipe_tr;    // pointer to transaction
    KHCI_PIPE_STRUCT_PTR pipe_desc =  usb_host_ptr->schedule_tr_ptr->pipe_desc;

    total_len = (pipe_tr->G.TX_LENGTH > 0) ? pipe_tr->G.TX_LENGTH :  ((pipe_tr->G.RX_LENGTH > 0) ? pipe_tr->G.RX_LENGTH : 0);

    if(res >= 0) {
#ifdef FIX_KHCI_ALIGN_ISSUE        
        uint_8    * buf;
        if (pipe_desc->G.DIRECTION == USB_RECV) {
            buf = pipe_tr->G.RX_BUFFER +  pipe_tr->pos;
            if((((uint_32)buf) & 0x3) /*&& (pipe_desc->G.PIPETYPE == USB_BULK_PIPE)*/) {
                ASSERT(res <= 64);
                USB_mem_copy(buf_tmp,buf,res);
            }
        }
#endif        

        pipe_tr->pos += res;


#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,res);
#endif

        if (transfer_is_complete(pipe_tr,pipe_desc,total_len,res)) {  /* transfer complete */
            // do complete callback
             _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, 
                                                    total_len- pipe_tr->pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;

            if(pipe_desc->G.DIRECTION == USB_RECV) { 
                pipe_tr->retry_cnt = pipe_desc->G.NAK_COUNT;    /* reset nak retry at next frame */
                if(usb_host_ptr->schedule_state + 1 == shced_queue_size )
                    return;
                else
                    usb_host_ptr->schedule_state += 1;
            }
        }
        else {
            // judge band width
            if (pipe_desc->G.DIRECTION == USB_RECV) {
                pipe_tr->status = TRF_IN;
            }
            else {
                pipe_tr->status = TRF_OUT;
            }
            tr_size = get_transaction_size(usb_host_ptr->schedule_tr_ptr);
            if(update_free_bandwidth(usb_host_ptr)){
                if(HAS_FREE_BANDWIDTH(usb_host_ptr->schedule_tr_ptr->pipe_desc->G.DIRECTION,get_usb_packet_size(usb_host_ptr->schedule_tr_ptr,tr_size),
                        usb_host_ptr->free_bandwidth))
                    _usb_khci_do_transfer(usb_host_ptr);
            }
            return;
        }
    }
    else {  // handle transaction err
        // pipe_tr->status = res;

#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,NAK_BANDWIDTH);
#endif

        if ((res != KHCI_ATOM_TR_NAK) && (res != KHCI_ATOM_TR_TO ) && (res != KHCI_ATOM_TR_EOF_ERROR)) {
            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len,
                                            total_len- pipe_tr->pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else { /* if the in pipe nak or bus timeout then always retry ,until cancel it */
            if(pipe_desc->G.PIPETYPE == USB_INTERRUPT_PIPE) ASSERT(pipe_tr->retry_cnt == 0);

            if(res == KHCI_ATOM_TR_EOF_ERROR)
                    return;

            
            if(pipe_tr->retry_cnt) {
                pipe_tr->retry_cnt --;
            }
            else {
                pipe_tr->retry_cnt = pipe_desc->G.NAK_COUNT;    /* reset nak and retry at next frame */
                if(usb_host_ptr->schedule_state + 1 == shced_queue_size )
                    return;
                else
                    usb_host_ptr->schedule_state += 1;
            }
        }
    }

    // judge band width and do next transfer
    if(update_free_bandwidth(usb_host_ptr)) {
        if(get_next_schedule_transfer(usb_host_ptr))
            _usb_khci_do_transfer(usb_host_ptr);
    }

}

static inline void handle_iso_transaction_done(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr,int res)
{
    // always goto next transfer
    //KHCI_PIPE_STRUCT_PTR pipe_desc;
    uint_32     total_len;
    uint_32      tr_next_size;      /* transaction size */
    /* pipe_tr->status = TRF_QUEUE; */ /* iso always is queue status */
    
    KHCI_TR_STRUCT_PTR pipe_tr = usb_host_ptr->schedule_tr_ptr->pipe_tr;    // pointer to transaction
    KHCI_PIPE_STRUCT_PTR pipe_desc =  usb_host_ptr->schedule_tr_ptr->pipe_desc;

    total_len = (pipe_tr->G.TX_LENGTH > 0) ? pipe_tr->G.TX_LENGTH :  ((pipe_tr->G.RX_LENGTH > 0) ? pipe_tr->G.RX_LENGTH : 0);

    if(res >= 0) {
        pipe_tr->pos = res;


#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,res);
#endif

        if((tr_next_size = _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len,
            total_len- pipe_tr->pos, res))== TR_CALLBACK_COMPLETE) {
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_state ++;
            //usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else {
            if (pipe_desc->G.DIRECTION == USB_SEND) {
                pipe_tr->G.TX_LENGTH = tr_next_size; /* next tr size */
                pipe_tr->G.TX_BUFFER  = pipe_tr->G.TX_BUFFER + res;  /* add res or last TX_LENTGH ? */
            }
            else {
                pipe_tr->G.RX_LENGTH = tr_next_size;
                pipe_tr->G.RX_BUFFER  = pipe_tr->G.RX_BUFFER + res;
            }
        }
        usb_host_ptr->schedule_tr_ptr = NULL;
    }
    else {  // handle transaction err
        // pipe_tr->status = res;

#ifdef SCHEDULING_CHECK_TRANSFERED
        usb_host_ptr->this_frame_transfered +=  get_usb_packet_size(NULL,NAK_BANDWIDTH);
#endif

        if ((res != KHCI_ATOM_TR_NAK) && (res != KHCI_ATOM_TR_TO ) && (res != KHCI_ATOM_TR_EOF_ERROR)) {
            _usb_khci_process_tr_complete(usb_host_ptr->schedule_tr_ptr->pipe_desc, pipe_tr, total_len, total_len- pipe_tr->pos, res);
            dequeue_schdule_tr_que(usb_host_ptr->schdule_queues[usb_host_ptr->schedule_state]);
            usb_host_ptr->schedule_tr_ptr = NULL;
        }
        else  /* recv nak then always retry */

            ASSERT(pipe_tr->retry_cnt == 0);     /* iso does't retry */

            if(res == KHCI_ATOM_TR_EOF_ERROR)
                return;

            if(pipe_tr->retry_cnt) {
                pipe_tr->retry_cnt --;
            }
            else {
                pipe_tr->retry_cnt = pipe_desc->G.NAK_COUNT;    /* reset nak retry at next frame */
                if(usb_host_ptr->schedule_state + 1 == shced_queue_size )
                    return;
                else
                    usb_host_ptr->schedule_state += 1;
            }
            
    }

    if(update_free_bandwidth(usb_host_ptr)) {
        if(get_next_schedule_transfer(usb_host_ptr))
            _usb_khci_do_transfer(usb_host_ptr);
    }

}

void handle_transaction_done(USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr)
{
    USB_MemMapPtr usb_ptr = (USB_MemMapPtr) usb_host_ptr->G.DEV_PTR;
    if(usb_host_ptr->schedule_tr_ptr == NULL) {
        printf("last tokendone in this frame\n");
        return;
    }
    
    KHCI_PIPE_STRUCT_PTR       pipe_desc_ptr = usb_host_ptr->schedule_tr_ptr->pipe_desc;
    uint_32 bd;
    int_32 res;
    
    if (usb_host_ptr->last_bd_ptr != NULL && (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_TOK_DONE)) {
        // transaction finished ,call this clear at other place cuase error !
        _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_TOK_DONE);

        // check result
        bd = LONG_LE_TO_HOST(*(usb_host_ptr->last_bd_ptr));

        if (usb_ptr->ERRSTAT & (
            USB_ERREN_PIDERREN_MASK |
#if defined(KHCICFG_BASIC_SCHEDULING)
            USB_ERREN_CRC5EOFEN_MASK |
#endif
            USB_ERREN_CRC16EN_MASK |
            USB_ERREN_DFN8EN_MASK |
            //                    USB_ERREN_BTO_ERR_EN_MASK | //timeout tested elsewhere
            USB_ERREN_DMAERREN_MASK |
            USB_ERREN_BTSERREN_MASK))  {
#if defined(KHCICFG_BASIC_SCHEDULING)
            if (usb_ptr->ERRSTAT & USB_ERREN_CRC5EOFEN_MASK) {
                // retry = 0;
                //printf("detects End Of Frame (EOF) error conditions.\n");
#ifdef SCHEDULING_CHECK_TIME  // if don't check time then always happen EOF error!
                printf("EOF%d\n",usb_ptr->ERRSTAT);
#endif
            }
#endif                   
            res = -usb_ptr->ERRSTAT;
#ifdef SCHEDULING_CHECK_TIME
            printf("ERRSTAT %d\n",res);
#endif
            // return res;  // error handle by endpoint token done function
        }
        else {
            switch (bd >> 2 & 0xf) {
            case 0x03:  // DATA0
            case 0x0b:  // DATA1
            case 0x02:  // ACK
                //retry = 0;
                res = (bd >> 16) & 0x3ff;
                //if(res ==0 )
                //	printf("liutest transfer res 0 err\n");
                pipe_desc_ptr->G.NEXTDATA01 ^= 1;     // switch data toggle

            break;

            case 0x0e:  // STALL
                res = KHCI_ATOM_TR_STALL;
                printf("STALL error\n");
                //retry = 0;
            break;

            case 0x0a:  // NAK
                res = KHCI_ATOM_TR_NAK;
                //if (retry)
                //    _time_delay(delay_const * (pipe_desc_ptr->G.NAK_COUNT - retry));
            break;

            case 0x00:  // bus timeout
                if((pipe_desc_ptr->G.PIPETYPE != USB_ISOCHRONOUS_PIPE)) {
                    //wait a bit, but not too much, perhaps some error occurs on the bus
                    res = KHCI_ATOM_TR_TO;
                }
                else {
                    //retry = 0;
                    res = (bd >> 16) & 0x3ff;
                    pipe_desc_ptr->G.NEXTDATA01 ^= 1;     /* switch data toggle */
                }
                break;

            case 0x0f:  // data error
                //if this event happens during enumeration, then return means not finished enumeration
                res = KHCI_ATOM_TR_DATA_ERROR;
                printf("data error\n");
                //_time_delay(delay_const * (pipe_desc_ptr->G.NAK_COUNT - retry));
                break;

            default:
                break;
            }
        }

        // caculate new free band width
        // todo !

        // check if already in next frame then dicard handle transaction done.
        // ASSERT(_usb_khci_get_total_frame_count(usb_host_ptr) == usb_host_ptr->frame_num ); // can't assert in step debug !
        
        // handle res ,and complete
        if(pipe_desc_ptr->G.PIPETYPE == USB_ISOCHRONOUS_PIPE)
            handle_iso_transaction_done(usb_host_ptr,res);
        else if(pipe_desc_ptr->G.PIPETYPE == USB_CONTROL_PIPE)
            handle_ctrl_transaction_done(usb_host_ptr,res);
        else if(pipe_desc_ptr->G.PIPETYPE == USB_INTERRUPT_PIPE)
            handle_intr_transaction_done(usb_host_ptr,res);
        else if(pipe_desc_ptr->G.PIPETYPE == USB_BULK_PIPE)
            handle_bulk_transaction_done(usb_host_ptr,res);
        else {
            printf("invalid pipe type\n");
        }
            
    }
}


static void _usb_khci_task(uint_32 dev_inst_ptr) 
{
    USB_KHCI_HOST_STATE_STRUCT_PTR usb_host_ptr = (USB_KHCI_HOST_STATE_STRUCT_PTR) dev_inst_ptr;

    static MQX_TICK_STRUCT last_tick;
    MQX_TICK_STRUCT    cur_tick,diff_tick;
    /*
    _mqx_int res;
    uint_8 *buf; */
        
    while (1) {
        // if (usb_host_ptr->G.DEVICE_LIST_PTR) // not null 
        // else {
        // wait for event if device is not attached
        //    _lwevent_wait_ticks(&usb_host_ptr->khci_event, KHCI_EVENT_MASK, FALSE, 0);
        // }
        _lwevent_wait_ticks(&usb_host_ptr->khci_event, KHCI_EVENT_MASK, FALSE, 0);
        
        if (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_MASK) {
            
            if (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_ATTACH) {
                //printf("a\n");
                _usb_khci_attach(usb_host_ptr);
            }

            if (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_RESET) {
                //printf("r\n");
                _usb_khci_reset(usb_host_ptr);
            }

            if (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_DETACH) {
                //printf("d\n");
                _usb_khci_detach(usb_host_ptr);
            }
            else {
                /* The token-done handle must before the SOF  */       
                if ((usb_host_ptr->khci_event.VALUE & KHCI_EVENT_TOK_DONE)  && (usb_host_ptr->schedule_state >= 0)) {
                    handle_transaction_done(usb_host_ptr);
                    //_lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_TOK_DONE);
                }
                            
                if (usb_host_ptr->khci_event.VALUE & KHCI_EVENT_SOF_TOK) {
                    _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_SOF_TOK 
                        | KHCI_EVENT_TOK_DONE );  /* before start scheduling ,clear tokendone */

#ifdef SCHEDULING_CHECK_TIME                    
                    if(usb_host_ptr->schedule_state == START_GET_FRAME_HWTICK) {
                         _time_get_elapsed_ticks_fast(&last_tick);
                         usb_host_ptr->schedule_state ++;
                    }
                    else  if(usb_host_ptr->schedule_state == SET_FRAME_HWTICK) {
                        _time_get_elapsed_ticks_fast(&cur_tick);
                        _time_diff_ticks(&cur_tick, &last_tick,&diff_tick);
                        ASSERT(diff_tick.TICKS[0] == 0);
                        usb_host_ptr->one_frame_tick = diff_tick.HW_TICKS;
                        usb_host_ptr->one_frame_tick =  usb_host_ptr->one_frame_tick *8/10 /1000 * 1000;  /* %80 insurance */
                        //printf("ref tick %d\n",usb_host_ptr->one_frame_tick);
                        usb_host_ptr->schedule_state ++;
                    }
                    else  
#endif                  
                        if(usb_host_ptr->schedule_state >=0) {
#ifdef SCHEDULING_CHECK_TIME
                        _time_get_elapsed_ticks_fast(&usb_host_ptr->sof_start_tick);
#endif
                        usb_host_ptr ->frame_num =  _usb_khci_get_total_frame_count(usb_host_ptr);
                        strat_schdule_this_frame(usb_host_ptr);
                        // _lwevent_clear(&usb_host_ptr->khci_event, KHCI_EVENT_SOF_TOK);
                    }
                    else {
                        usb_host_ptr->schedule_state ++;
                    }
                }
            }/* else detach */

        }
    }
}

#endif

