/**HEADER********************************************************************
* 
* Copyright (c) 2008 Freescale Semiconductor;
* All Rights Reserved
*
*************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: khci_prv.h$
* $Version : 3.8.18.0$
* $Date    : Jun-22-2012$
*
* Comments:
*
*   This file contains the macros, function prototypes and data structure
*   definitions required by the KHCI USB device driver.
*
*END************************************************************************/
#ifndef __khci_prv_h__
#define __khci_prv_h__

#include <mqx.h>
#include <lwevent.h>
#include <lwmsgq.h>

#include <mutex.h>

#include <bsp.h>

#include "host_cnfg.h"
#include "hostapi.h"    /* needed for USB_SERVICE_STRUCT_PTR */
#include "host_main.h"


//#include "os_utils.h"  // liutest add os adapter
#define __compiler_offsetof(a,b) __builtin_offsetof(a,b)
#define offsetof(TYPE,MEMBER) __compiler_offsetof(TYPE,MEMBER)

#define halt() do { for(;;); } while (0)
#define ASSERT(x) do { if (!(x)) { /*dbgPrintf*/printf("%s:%d ASSERT failed: %s\n", __FILE__, __LINE__, #x); halt(); } } while (0)

#if MQX_USE_UNCACHED_MEM && PSP_HAS_DATA_CACHE
    #define os_mem_alloc_uncached(n)         _mem_alloc_system_uncached(n)
    #define os_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero_uncached(n)
    #define os_mem_alloc_uncached_align(n,a) _mem_alloc_align_uncached(n,a)
#else
    #define os_mem_alloc_uncached(n)         _mem_alloc_system(n)
    #define os_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero(n)
    #define os_mem_alloc_uncached_align(n,a) _mem_alloc_align(n,a)
#endif   /*  PSP_HAS_DATA_CACHE */

#define os_mem_alloc(n)                      _mem_alloc_system(n)
#define os_mem_alloc_zero(n)				 _mem_alloc_system_zero(n)
#define os_mem_free(ptr)                     _mem_free(ptr)
#define os_mem_zero(ptr,n)                   _mem_zero(ptr,n)
#define os_mem_copy(src,dst,n)				 _mem_copy(src,dst,n)

#define os_memcpy		memcpy
#define os_memset		memset


#define  FIX_KHCI_ALIGN_ISSUE     /* khci dma buffer must be aligned with 4Byte */
#define NAK_BANDWIDTH           4   // bytes  , nak and bus timeout occupy band width

#define ISO_TR_QUEUE_SIZE      2
#define INTR_TR_QUEUE_SIZE     2
#define CTRL_TR_QUEUE_SIZE   (4 + 4)
#define BULK_TR_QUEUE_SIZE   (8 + 4/*+ 8*/)

#define  RESERVE_BANDWIDTH                    (64 + 64)    // (60+ 64)  // reserve for void the transaction in corner
#define THEORETICAL_BANDWIDTH              (12000/8)
#define KHCI_MAX_BANDWIDTH                  (THEORETICAL_BANDWIDTH -RESERVE_BANDWIDTH) //  ( 1500 -124)
#define KHCI_ISO_TR_MAX_BANDWIDTH      1024          /* full speed 69% */
//#define KHCI_PERIOD_MAX_BANDWIDTH      ( 1024 + 64 )  /* full speed 90% */
#define KHCI_INTRIN_MAX_BANDWIDTH      ( KHCI_ISO_TR_MAX_BANDWIDTH + 64 )
#define KHCI_INTROUT_MAX_BANDWIDTH    ( KHCI_INTRIN_MAX_BANDWIDTH + 64 )  /* full speed 90% */
#define KHCI_CTRL_MAX_BANDWIDTH          ( KHCI_INTROUT_MAX_BANDWIDTH + 64 )
#define KHCI_BULKIN_MAX_BANDWIDTH      ( KHCI_CTRL_MAX_BANDWIDTH + 64 )
#define KHCI_BULKOUT_MAX_BANDWIDTH    ( KHCI_BULKIN_MAX_BANDWIDTH + 64 )




#ifdef KHCICFG_BASIC_SCHEDULING
#ifdef SCHEDULING_CHECK_TIME  /* usb_send = 1, usb_recv = 0 */
#define HAS_FREE_BANDWIDTH(direct,packet_size, free_bandwidth)  (direct ? ((packet_size < free_bandwidth) ? TRUE : FALSE) : TRUE)
#else
#define HAS_FREE_BANDWIDTH(direct,packet_size, free_bandwidth)     TRUE
#endif
#else
#error not define this scheduling
#endif


#define SCHED_QUE_LOCK(x)    USB_lock()//_mutex_lock((MUTEX_STRUCT_PTR)x)
#define SCHED_QUE_UNLOCK(x)  USB_unlock()// _mutex_unlock((MUTEX_STRUCT_PTR)x)

#define START_STABLE_HWTICK          -8    // -200
#define START_GET_FRAME_HWTICK    -2
#define SET_FRAME_HWTICK              -1

enum {
         iso_in_tr_queue,     /* high prior */ /*  period transfer */
         iso_out_tr_queue,
         //interrupt_tr_queue,
         intr_in_tr_queue,
         intr_out_tr_queue,       
         period_queue = intr_out_tr_queue,
         ctrl_tr_queue,         /* non period transfer */
         bulk_in_tr_queue,
         bulk_out_tr_queue,
         //bulk_tr_queue,        /* low prior */
         shced_queue_size
};

enum {
    TR_CTRL,
    TR_IN,
    TR_OUT,
    TR_OUT_HNDSK,
    TR_IN_HNDSK
};

/* khci transfer status or transaction error state */
typedef enum {
		TRF_SETUP_QUEUE,             /* setup khci transfer status */
          
		TRF_SETUP_DTOH,                
		TRF_SETUP_DTOH_IN,
		TRF_SETUP_DTOH_OUT,
		
		TRF_SETUP_HTOD,
		TRF_SETUP_HTOD_OUT,
		TRF_SETUP_HTOD_IN,
		
		TRF_SETUP_COMPLETE,    /* maybe no use */
};

typedef enum {		
		/*TRF_IN_QUEUE*/TRF_QUEUE,                   /* interrup in,bulk in transfer status  */
             TRF_IN_QUEUE = TRF_QUEUE,
             TRF_OUT_QUEUE = TRF_QUEUE,  // 0
		TRF_IN,                                   // 1
		TRF_OUT,                                // 2
		TRF_COMPLETE,                      // 3
		TRF_IN_COMPLETE = TRF_COMPLETE,
		TRF_OUT_COMPLETE = TRF_COMPLETE,
		
};

typedef struct khci_tr_struct {
   struct tr_struct G;
   uint_16  status;   /* G.STATUS ? liunote use as internal transfer status as above */
   uint_16  pos;       /* current dispatched position ,for iso is what frams, for other pipe is the offset of buf transfer */
   //uint32    period;   /* save the interrupt frame number , 0  no wait */
   uint_32    retry_cnt;  /* for non period pipe ,out retry */
   
   int         tr_error;    /* atom trasaction error , 0 no error */
   //int         next_frame;  /* For USB 1.1, after count NAKs/frame,transaction is deferred to next frame. */
   /*
#define KHCI_ATOM_TR_PID_ERROR  (-1)
#define KHCI_ATOM_TR_EOF_ERROR  (-2)
#define KHCI_ATOM_TR_CRC_ERROR  (-4)
#define KHCI_ATOM_TR_TO         (-16)
#define KHCI_ATOM_TR_DMA_ERROR  (-32)
#define KHCI_ATOM_TR_BTS_ERROR  (-128)
#define KHCI_ATOM_TR_NAK        (-256)
#define KHCI_ATOM_TR_DATA_ERROR (-512)
#define KHCI_ATOM_TR_STALL      (-1024)
#define KHCI_ATOM_TR_RESET      (-2048)
   */
} KHCI_TR_STRUCT, _PTR_ KHCI_TR_STRUCT_PTR;

typedef struct khci_pipe_struct
{
   /* Fields common to both pipe descriptors and the structures used to
    * initialize new pipe instances.
    */
   PIPE_STRUCT         G;
} KHCI_PIPE_STRUCT, _PTR_ KHCI_PIPE_STRUCT_PTR;

/* Transaction type */
typedef enum {
    TR_MSG_UNKNOWN,     // unknow - not used
    TR_MSG_SETUP,       // setup transaction
    TR_MSG_SEND,        // send trnasaction
    TR_MSG_RECV         // receive transaction
} TR_MSG_TYPE;

typedef struct {
    KHCI_PIPE_STRUCT_PTR pipe_desc;         // pointer to pipe descriptor
    KHCI_TR_STRUCT_PTR pipe_tr;             // pointer to transaction
}  SCHED_TR_STRUCT ,* SCHED_TR_STRUCT_PTR;

/* tr_queue at khci */
typedef struct SCHEDULE_TR_QUEUE_STRUCT
{
   uint8_t     MAX_COUNT;  /* max is 256 */
   uint8_t     COUNT;
   uint8_t      FIRST;         /* index of the first TR in the queue */
   uint8_t      LAST;          /* index of the last TR in the queue */
   uint32_t    LFRAME;          /* save the last transfer frame number in this queue  */
   uint_32     INTERVAL;    /* for period transfer, set it at open pipe function  */
   
   SCHED_TR_STRUCT QUE[1];  /* point to the queue memory */
} TR_QUEUE_STRUCT, * TR_QUEUE_STRUCT_PTR;


#define BDT_BASE                ((uint_32*)(bdt))
#define BD_PTR(ep, rxtx, odd)   (((uint_32)BDT_BASE) & 0xfffffe00 | ((ep & 0x0f) << 5) | ((rxtx & 1) << 4) | ((odd & 1) << 3))

#define BD_CTRL(ep,rxtx,odd)    (*((uint_32*)BD_PTR(ep, rxtx, odd)))
#define BD_CTRL_RX(ep, odd)     (*((uint_32*)BD_PTR(ep, 0, odd)))
#define BD_CTRL_TX(ep, odd)     (*((uint_32*)BD_PTR(ep, 1, odd)))

#define BD_ADDR(ep,rxtx,odd)    (*((uint_32*)BD_PTR(ep, rxtx, odd) + 1))
#define BD_ADDR_RX(ep, odd)     (*((uint_32*)BD_PTR(ep, 0, odd) + 1))
#define BD_ADDR_TX(ep, odd)     (*((uint_32*)BD_PTR(ep, 1, odd) + 1))

/* Define USB buffer descriptor definitions in case of their lack */
#ifndef USB_BD_BC
#   define USB_BD_BC(n)                 ((n & 0x3ff) << 16)
#   define USB_BD_OWN                   0x80
#   define USB_BD_DATA01(n)             ((n & 1) << 6)
#   define USB_BD_DATA0                 USB_BD_DATA01(0)
#   define USB_BD_DATA1                 USB_BD_DATA01(1)
#   define USB_BD_KEEP                  0x20
#   define USB_BD_NINC                  0x10
#   define USB_BD_DTS                   0x08
#   define USB_BD_STALL                 0x04
#   define USB_BD_PID(n)                ((n & 0x0f) << 2)
#endif

#ifndef USB_TOKEN_TOKENPID_SETUP
#   define USB_TOKEN_TOKENPID_OUT                   USB_TOKEN_TOKENPID(0x1)
#   define USB_TOKEN_TOKENPID_IN                    USB_TOKEN_TOKENPID(0x9)
#   define USB_TOKEN_TOKENPID_SETUP                 USB_TOKEN_TOKENPID(0xD)
#endif

/* Alignement of buffer for DMA transfer, needed in some cases,
** USB DMA bus could not possibly be intializes properly and 
** first data transfered is the one aligned at 4-byte boundary
*/
#define USB_MEM4_ALIGN(n)               ((n) + (-(n) & 3))
#define USB_DMA_ALIGN(n)                USB_MEM4_ALIGN(n)


typedef struct usb_khci_host_state_struct
{
   USB_HOST_STATE_STRUCT               G; /* generic host state */
   /* Driver-specific fields */
   uint_32                             vector;
   boolean                             endian_swap;
   /* RX/TX buffer descriptor toggle bits */
   uint_8                              rx_bd;
   uint_8                              tx_bd;
   
   /* event from KHCI peripheral */
   LWEVENT_STRUCT                      khci_event;
   
   /* Pipe, that had latest NAK respond, usefull to cut bandwidth for interrupt pipes sending no data */
   //-KHCI_PIPE_STRUCT_PTR                last_to_pipe;
   /* Interrupt transactions */
   //-TR_INT_QUE_ITM_STRUCT               tr_int_que[USBCFG_KHCI_MAX_INT_TR];
   //-_mqx_max_type                       tr_que[(sizeof(LWMSGQ_STRUCT) + USBCFG_KHCI_TR_QUE_MSG_CNT * sizeof(TR_MSG_STRUCT)) / sizeof(_mqx_max_type) + 1];

   /* schedule */
   // KHCI_TR_STRUCT_PTR   dispatched_tr;    /* this tr is waiting for token done  */
   SCHED_TR_STRUCT_PTR    schedule_tr_ptr;     /* current schdule transfer */
   //SCHED_TR_STRUCT_PTR    pending_schedule_tr_ptr;
   int32_t   schedule_state;                          /* -2,-1 ,0--(quesize -1)*/
   uint_32 * last_bd_ptr;                              /**/
   
   /*MQX_TICK_STRUCT*/uint_32  one_frame_tick;  /* get hwtick count at calibration state */
   MQX_TICK_STRUCT  sof_start_tick;     /* get it at sof interrupt */
   uint16_t    max_bandwidth;              /* per frame max band width */
   uint16_t    free_bandwidth;              /* update free_bandwidth at transfer complete */
   
   uint32_t    frame_num;                     /* increase when sof interrupt or get from reg FRMNUMH & FRMNUML */
   uint32_t    this_frame_transfered;
   /*uint32_t    last_intr_in_frame_num;
   uint32_t    last_intr_out_frame_num;
   uint32_t    last_iso_in_frame_num;
   uint32_t    last_iso_out_frame_num;
   */
   MUTEX_STRUCT_PTR  que_lock;  // At present we use usblock close interrupt.
   
   TR_QUEUE_STRUCT_PTR  schdule_queues[shced_queue_size];

} USB_KHCI_HOST_STATE_STRUCT, _PTR_ USB_KHCI_HOST_STATE_STRUCT_PTR;

/* schdule queue */
#define TR_QUE_NEXT(tr_que,index)   (uint_8)( index ==(tr_que->MAX_COUNT - 1) ? 0 : index + 1)

//#define QUEUE_LOCK OS_Mutex_lock

static inline TR_QUEUE_STRUCT_PTR new_schdule_tr_que
   (    
   unsigned char count
    )
{ /* Body */
  
    TR_QUEUE_STRUCT_PTR tr_que_ptr;

    ASSERT(count<=256);

    printf("count %d, queue offset bytes %d, queue size bytes %d\n",count,offsetof(TR_QUEUE_STRUCT, QUE) , count * sizeof(SCHED_TR_STRUCT));
    tr_que_ptr = os_mem_alloc_zero(offsetof(TR_QUEUE_STRUCT, QUE) + count * sizeof(SCHED_TR_STRUCT));
    if(tr_que_ptr)
        tr_que_ptr->MAX_COUNT = count;

    return tr_que_ptr;
  
} /* Endbody */

static inline void delete_schdule_tr_que
   (    
   TR_QUEUE_STRUCT_PTR tr_que_ptr
    )
{ /* Body */
  if(tr_que_ptr)
    os_mem_free(tr_que_ptr);
} /* Endbody */

static inline int_32 enque_schdule_tr_que
   (
      TR_QUEUE_STRUCT_PTR tr_que_ptr,
      KHCI_TR_STRUCT_PTR  hci_tr_ptr,
      KHCI_PIPE_STRUCT_PTR hci_pipe_ptr
   )
{ /* Body */
   int_32                  tmp = -1;

   /*
   ** Insert into queue, update LAST, check if full and return queue position.
   ** If queue is full -1 will be returned
   */
   USB_lock();
   if (tr_que_ptr->COUNT < tr_que_ptr->MAX_COUNT) {
      (tr_que_ptr->QUE[tr_que_ptr->LAST]).pipe_tr = hci_tr_ptr;
      (tr_que_ptr->QUE[tr_que_ptr->LAST]).pipe_desc = hci_pipe_ptr;
      tmp = tr_que_ptr->LAST;
      tr_que_ptr->LAST = TR_QUE_NEXT(tr_que_ptr,tr_que_ptr->LAST);
      tr_que_ptr->COUNT++;
   } /* Endif */
   USB_unlock();

   return tmp;
} /* Endbody */


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : usb_class_mass_deleteq
* Returned Value : None
* Comments       :
*     This routine deletes the pending request in the queue
*END*--------------------------------------------------------------------*/

static inline SCHED_TR_STRUCT_PTR dequeue_schdule_tr_que
   (
      TR_QUEUE_STRUCT_PTR tr_que_ptr
   )
{ /* Body */

    SCHED_TR_STRUCT_PTR  sched_tr_ptr = NULL;
   /* Remove current command and increment FIRST modulo the Q size */
   USB_lock();
   if (tr_que_ptr->COUNT) {
      // sched_tr_ptr = &(tr_que_ptr->QUE[tr_que_ptr->FIRST]);
      tr_que_ptr->QUE[tr_que_ptr->FIRST].pipe_tr = NULL;
      tr_que_ptr->QUE[tr_que_ptr->FIRST].pipe_desc = NULL;
      tr_que_ptr->FIRST = TR_QUE_NEXT(tr_que_ptr,tr_que_ptr->FIRST);
   //   tr_que_ptr->AVAILABLE = TRUE;
      tr_que_ptr->COUNT--;
   }
   USB_unlock();

    return   sched_tr_ptr;
} /* Endbody */

/* in get pending statu can't cancel it ! */
static inline SCHED_TR_STRUCT_PTR get_pending_schdule_tr_que
   (
        TR_QUEUE_STRUCT_PTR tr_que_ptr
   )
{ /* Body */
   SCHED_TR_STRUCT_PTR  sched_tr_ptr = NULL;

   USB_lock();
   if (tr_que_ptr->COUNT) {
      sched_tr_ptr = &(tr_que_ptr->QUE[tr_que_ptr->FIRST]);
   }
  USB_unlock();
   return sched_tr_ptr;
} /* Endbody */

static inline int cancel_schdule_tr_que
   (
      TR_QUEUE_STRUCT_PTR tr_que_ptr,

      KHCI_TR_STRUCT_PTR  hci_tr_ptr
   )
{
   uint_32                 i,index;
   boolean                 result = FALSE;
   
   /* Remove given command - leave q size the same*/
   USB_lock();
   if (tr_que_ptr->COUNT) {
      index = tr_que_ptr->FIRST;
      for (i = 0;i<tr_que_ptr->COUNT;i++) {
         if ((tr_que_ptr->QUE[index]).pipe_tr == hci_tr_ptr) {
            tr_que_ptr->QUE[index].pipe_tr = NULL;
            tr_que_ptr->QUE[index].pipe_desc = NULL;

            result = TRUE;
            //printf("\n q entry cancelled!!!" );
            break;
         }   
         index = TR_QUE_NEXT(tr_que_ptr,index);
      }
   }
   USB_unlock();

   return result;
} 

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif

/* EOF */
