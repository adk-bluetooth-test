
#ifndef _USB_DRV_H_
#define _USB_DRV_H_

#include "usb.h"
#include "usb_prv.h"

#include "host_cnfg.h"
#include "hostapi.h"
/*
#include "host_main.h"
#include "host_snd.h"
#include "host_rcv.h" */
#include "host_ch9.h"

//#define USE_REAL_TRANSFER_HANDLE
/* if defined this flag ,must modify _usb_khci_process_tr_complete
 1. move  " pipe_tr_ptr->G.TR_INDEX = 0;" to close transfer function
 2. set  pipe_tr_ptr->STATUS to callback status ; and set to USB_STATUS_IDLE at close transfer
    add  #define USB_STATUS_TRANSFER_COMPLETE          (8)
          #define USB_STATUS_TRANSFER_FAILED               (9)
 3. modify  usb_hostdev_tr_init  to return  tr_index */

#define  ACL_CLASS_INTF     0
#define  SCO_CLASS_INTF     1

#define  NUM_CLASS_INTF     2

typedef struct _device_struct
{
    /* Class-specific info */
    CLASS_CALL_STRUCT                 class_intf;
    _usb_device_instance_handle      dev_handle;
    _usb_interface_descriptor_handle intf_handle;
}  DEVICE_STRUCT, _PTR_  DEVICE_STRUCT_PTR;

typedef struct _app_device_struct
{
    DEVICE_STRUCT_PTR  pdev_classes[NUM_CLASS_INTF];
} APP_DEVICE_STRUCT,  _PTR_ APP_DEVICE_STRUCT_PTR;

typedef struct _acl_class_device_struct
{
    //DEV_CLASS_INFO                   class_info;
    CLASS_CALL_STRUCT                        class_intf;
    _usb_device_instance_handle              dev_handle;
    _usb_interface_descriptor_handle         intf_handle;
    
    ENDPOINT_DESCRIPTOR_PTR			endp_events;
    ENDPOINT_DESCRIPTOR_PTR			endp_acl_in;
    ENDPOINT_DESCRIPTOR_PTR			endp_acl_out;
} ACL_DEVICE_STRUCT,  _PTR_ ACL_DEVICE_STRUCT_PTR;

#ifdef USE_SCO_TRANSFER
typedef struct _sco_class_device_struct
{
    CLASS_CALL_STRUCT                         class_intf;
    _usb_device_instance_handle              dev_handle;
    _usb_interface_descriptor_handle         intf_handle;
    
    ENDPOINT_DESCRIPTOR_PTR			endp_sco_in[NUM_SCO_ENDPOINTS];
    ENDPOINT_DESCRIPTOR_PTR			endp_sco_out[NUM_SCO_ENDPOINTS];
    INTERFACE_DESCRIPTOR_PTR               interface_sco[NUM_SCO_ALTSETTINGS];

/* move to usb_dev
    uint_8						            endp_sco_alt_setting[NUM_SCO_ENDPOINTS];
    int                                                 sco_in_count, sco_out_count;

    int                                                 sco_pipe_idx;
    unsigned int                                     sco_read_frames;
    unsigned int                                     close_sco_io;

    unsigned int                                in_transfer_lens[MAX_SCO_READ_FRAMES_PER_TRANSFER];
    unsigned int                                out_transfer_lens[SCO_WRITE_FRAMES_PER_PACKET];
    unsigned int                                suggested_sco_alt_setting;
*/    
} SCO_DEVICE_STRUCT,  _PTR_ SCO_DEVICE_STRUCT_PTR;
#endif

typedef struct _bt_interface_struct {
    /* Each class must start with a GENERAL_CLASS struct */
    GENERAL_CLASS                             G;

    /* application handle */
    CLASS_CALL_STRUCT_PTR                  APP;
} BT_INTERFACE_STRUCT, _PTR_ BT_INTERFACE_STRUCT_PTR;
/* ACL_INTERFACE_STRUCT, _PTR_ ACL_INTERFACE_STRUCT_PTR
   SCO_INTERFACE_STRUCT, _PTR_ SCO_INTERFACE_STRUCT_PTR */


#ifdef USE_SCO_TRANSFER
#if 0  // use the same struct
typedef struct _sco_interface_struct {
	/* Each class must start with a GENERAL_CLASS struct */
	GENERAL_CLASS                             G;
    	/* application handle */
	CLASS_CALL_STRUCT_PTR  /* ==  APP_DEVICE_STRUCT_PTR */                  APP;
} SCO_INTERFACE_STRUCT, _PTR_ BT_SCO_INTERFACE_STRUCT_PTR;
#endif
inline SCO_DEVICE_STRUCT_PTR get_sco_pdev(void * usb_dev)
{
    SCO_DEVICE_STRUCT_PTR sco_dev = 
        (SCO_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[SCO_CLASS_INTF]);
    return sco_dev;
}
#endif

inline ACL_DEVICE_STRUCT_PTR get_acl_pdev(void * usb_dev)
{
    ACL_DEVICE_STRUCT_PTR acl_dev = 
        (ACL_DEVICE_STRUCT_PTR)(((APP_DEVICE_STRUCT_PTR)usb_dev)->pdev_classes[ACL_CLASS_INTF]);
    return acl_dev;
}

#endif  /* _USB_DRV_H_ */
