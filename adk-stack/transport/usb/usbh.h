#ifndef _USB_H_H_
#define _USB_H_H_

#define EVENT_TIMEOUT_INFINITE               0

#define NUM_SCO_ENDPOINTS   6
#define NUM_SCO_ALTSETTINGS   /* (1 + 3) */6       /*core4.0 spec P2042  */
#ifdef USE_SCO_TRANSFER
#define NUM_IN_PIPES        3
#define NUM_OUT_PIPES       2
#else
#define NUM_IN_PIPES        2
#define NUM_OUT_PIPES       1
#endif

/*
#define EVENT_ISOCH         0
#define EVENT_BULK          1
#define EVENT_INTERRUPT     2 
*/
#define EVENT_BULK           0
#define EVENT_INTERRUPT   1
#define EVENT_ISOCH         2

#define EVENT_CLOSE          3
#define EVENT_WRITTEN       4

#define EVENT_PACKETS       5

/* move to hci_transport.h
enum HCI_TYPE {
	COMMAND_PACKET  = 1,
	DATA_PACKET_ACL = 2,
	DATA_PACKET_SCO = 3,
	EVENT_PACKET    = 4,
	ETYPE_FINISH    = 5
}; */

/* PIPE_STRUCT_PTR */
typedef void * PIPE_HANDLE;
typedef void * USB_DEV_HANDLE;

typedef struct _tr_user
{
#define TR_BLOCKED      0xffff0001
#define TR_UNBLOCKED  0xffff0000
#define TR_BLOCKED_MASK 0xffff
#define TR_CLEAR_MASK 0xffff0000

    /* 1 - blocked , 0 - unblocked */
    unsigned int tr_blocked;
    /* TR_STRUCT_PTR */
    void *   tr_internal_ptr;
    //unsigned int    tr_status;
    /* Because after tr complete ,it set tr_index to zero,  and maybe already used by other transfer */
    /* must modify khci ,don't set tr_index ,set it at closed transfer ,tr_status used by blocked transfer */

    /* set will transfer size ,then at callback ,check it size is valid . todo */
    unsigned int    transfered;
    void *       tr_private;

    // for ISO
    /* uint32_t */unsigned short  frames;          /* number of frames in transfer ,minus after callback , send event when it is zero */
    unsigned short  lens_num;                         /* current index of lens array  in iso transfer */
    unsigned int * lens_ptr;  /*  array of lengths  (one entry per frame) */
    unsigned char * buffs_ptr;             /* data buffer */
}  tr_user_t;


// Flags for transfer functions 
#define     USB_IN_TRANSFER          0x00000080
#define     USB_OUT_TRANSFER        0x00000000
#define     USB_NO_WAIT                 0x00000100
#define     USB_SHORT_TRANSFER_OK   0x00000200   /* if set this flag ,then tr flag set USB_SHORT_TRANSFER */
#define     USB_START_ISOCH_ASAP    0x00000400
#define     USB_COMPRESS_ISOCH      0x00000800
#define     USB_SEND_TO_DEVICE      0x00001000
#define     USB_SEND_TO_INTERFACE   0x00002000
#define     USB_SEND_TO_ENDPOINT    0x00004000

#endif  /* _USB_H_H_ */

