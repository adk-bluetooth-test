/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef ADK_INTERNAL
#ifndef _ADK_FWK_H_
#define _ADK_FWK_H_

#include <mqx.h>
#include <bsp.h>

#include "os_utils.h"

#include "hci_transport.h"

// todo
/*
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
*/

// #define BT_MODULE_CC256X  // we only test usb bt dongle

void fwkInit(void);
void cpuGetUniqId(uint32_t* dst);	//produce the 128-bit unique ID

uint8_t getVolume(void);
void setVolume(uint8_t);

#endif
#endif


