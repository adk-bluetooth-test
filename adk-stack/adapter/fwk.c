/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define ADK_INTERNAL
#include "fwk.h"

#define TRACE_DEBUG(...)
#define assert(...)

static uint8_t volume = 255;

void cpuGetUniqId(uint32_t* dst){	//produce the 128-bit unique ID
	printf("cpuGetUniqId\n");
}

void fwkInit(void){
	printf("fwkInit\n");
}

uint8_t getVolume(void){

    return volume;
}

void setVolume(uint8_t vol){

    volume = vol;
}

