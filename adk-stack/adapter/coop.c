/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define ADK_INTERNAL
#include "fwk.h"
#include "coop.h"

#define ADK_COOP_TASK_PRIORITY   10 //10  
int coopInit(void){
    return 1;
}

int coopSpawn(CoopTaskF taskF, void* taskData, uint32_t stackSz,uint32_t * task_id){

	// creat read task 
    *task_id = OS_Task_create(taskF, (void*)taskData, (uint32_t)ADK_COOP_TASK_PRIORITY, stackSz, "btTaskF", NULL);
	if (*task_id == 0) 
		return 0;
    return 1;
}

int coopDoom(uint32_t  task_id) {
    return OS_Task_delete(task_id);
}

void coopYield(void){
    OS_Task_yield();
}

void sleep(uint32_t ms)
{
	//uint64_t a = fwkGetUptime();
	//while (fwkGetUptime() - a < ms)
	//	coopYield();
	OS_Time_delay(ms);
}

