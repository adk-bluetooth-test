/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define ADK_INTERNAL
#include "fwk.h"
#include "Audio.h"
#include "coop.h"

#include "msi.h"


#define DEFAULT_AUDIO_SAMPLERATE 44100


void audioInit(void)
{
	msi_snd_init();
}

void audioOn(int source, uint32_t samplerate)
{
    msi_snd_set_format((uint32_t)samplerate, 16, 2);
    // todo add umute
}

void audioOff(int source)
{
        //to do add mute
}

void audioDeinit(void)
{
	msi_snd_deinit();
}

void audioSetSample(int source, uint32_t samplerate)
{
	//msi_snd_set_format((uint32_t)samplerate, 16, 2);
}

void audioAddBuffer(int source, const uint16_t* samples, uint32_t numSamples)  /* call by BT */
{
	msi_snd_write((uint_8 *)samples,numSamples);
}

