
#ifndef OS_UTILS_H
#define OS_UTILS_H
/*
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
*/
#include <mqx.h>
#include <bsp.h>

#include "lwevent.h"
#include "lwmsgq.h"
#include "mutex.h"
#include "lwsem.h"
_mqx_uint _lwmsgq_deinit(pointer location);

#include "dbg.h"

#if _DEBUG == 1
//#define OS_DEBUG
#endif

/* build */
typedef int	ssize_t;
typedef void * HANDLE;
typedef int BOOL;
//gcc4
#define __compiler_offsetof(a,b) __builtin_offsetof(a,b)
#define offsetof(TYPE,MEMBER) __compiler_offsetof(TYPE,MEMBER)
//
//-#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})


#define INT_DISABLE()                           _int_disable()
#define INT_ENABLE()                            _int_enable()

/* building ***************************************************/

/* os function */
#if MQX_USE_UNCACHED_MEM && PSP_HAS_DATA_CACHE
    #define os_mem_alloc_uncached(n)         _mem_alloc_system_uncached(n)
    #define os_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero_uncached(n)
    #define os_mem_alloc_uncached_align(n,a) _mem_alloc_align_uncached(n,a)
#else
    #define os_mem_alloc_uncached(n)         _mem_alloc_system(n)
    #define os_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero(n)
    #define os_mem_alloc_uncached_align(n,a) _mem_alloc_align(n,a)
#endif   /*  PSP_HAS_DATA_CACHE */

#define os_mem_alloc(n)                      _mem_alloc_system(n)
#define os_mem_alloc_zero(n)				 _mem_alloc_system_zero(n)
#define os_mem_free(ptr)                     _mem_free(ptr)
#define os_mem_zero(ptr,n)                   _mem_zero(ptr,n)
#define os_mem_copy(src,dst,n)				 _mem_copy(src,dst,n)

#define os_memcpy		memcpy
#define os_memset		memset

//#define os_malloc		_mem_alloc_system
static inline void * os_malloc(unsigned int y)
{
    int align_len = (y & 3) ? y + (4 - (y & 3)) : y;
    if(y == 0) {
        printf("os_malloc size 0\n");
        //           ???      dbgPrintf("SDP: Failed to allocate flattened result array (%ub)\n", sg_length(resultSoFar));
        //align_len = 4; // align_len
        return NULL;
    }
    return /*_mem_alloc_system*/_mem_alloc_system_zero(align_len);
}

//#define os_free			_mem_free
static inline void os_free(void * x)
{
    if(x == NULL)
        printf("os_free NULL pointer\n");
    else
        _mem_free(x);
}

#define os_zalloc   	os_mem_alloc_zero
static inline void * os_realloc(void * x,  unsigned int y)
{
    int align_len = (y & 3) ? y + (4 - (y & 3)) : y;
    if(x == NULL) {
        printf("os_realloc NULL pointer\n");
        return NULL;
    }
    void * addr = _mem_alloc_system(align_len);
    if(addr)
        memcpy(addr, x, y);
    _mem_free(x);
    return addr;
}

#define  malloc(x)  \
        os_malloc(x)
        
#define  free(x)  \
             os_free(x)
     
#define  realloc(x,y)   \
              os_realloc(x,y)

        
/* misc */


/* debug message */
#define __stringify_1(x...)	#x
#define __stringify(x...)	__stringify_1(x)

#define halt() do { for(;;); } while (0)
#define ASSERT(x) do { if (!(x)) { /*dbgPrintf*/printf("%s:%d ASSERT failed: %s\n", __FILE__, __LINE__, #x); halt(); } } while (0)

#if defined(OS_DEBUG)
#define pr_debug(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warning(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warn pr_warning	
#define os_printf(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#else
#define pr_debug(fmt, ...)
#define pr_warning(fmt, ...)
#define pr_warn	
#define os_printf(fmt, ...)
#endif

#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

#define pr_err(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)

#define USHRT_MAX	((u16)(~0U))
#define SHRT_MAX	((s16)(USHRT_MAX>>1))
#define SHRT_MIN	((s16)(-SHRT_MAX - 1))
#define INT_MAX		((int)(~0U>>1))
#define INT_MIN		(-INT_MAX - 1)
#define UINT_MAX	(~0U)
#define LONG_MAX	((long)(~0UL>>1))
#define LONG_MIN	(-LONG_MAX - 1)
#define ULONG_MAX	(~0UL)
#define LLONG_MAX	((long long)(~0ULL>>1))
#define LLONG_MIN	(-LLONG_MAX - 1)
#define ULLONG_MAX	(~0ULL)

#ifndef min
#define min(x, y)  (x < y ? x : y)
#endif
#ifndef max
#define max(x, y)  (x > y ? x : y)
#endif

#define ULONG_MAX	(~0UL)
#define LONG_MAX	((long)(~0UL>>1))

/* building ***************************************************/
#if 0
static inline uint8_t ffs(unsigned int val)
{
	int i;
	for(i = 0; i < 32; i++) {
		if(val & (1<< i))
			break;
	}

	if(i < 32)
		return i + 1;
	else
		return 0;
}
#endif

static inline int
test_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned int res;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	res = *p;

	return (res & mask) != 0;
}

static inline void set_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	*p |= mask;
}

static inline void clear_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	*p &= ~mask;
}

static inline void change_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	*p ^= mask;
}

static inline int
test_and_set_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned int res;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	res = *p;
	*p = res | mask;

	return (res & mask) != 0;
}

static inline int
test_and_clear_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned int res;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	res = *p;
	*p = res & ~mask;

	return (res & mask) != 0;
}

static inline int
test_and_change_bit(unsigned int bit, volatile unsigned long *p)
{
	//unsigned long flags;
	unsigned int res;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	res = *p;
	*p = res ^ mask;

	return (res & mask) != 0;
}


//#define FIXED_USE_DYNAMIC
//#define DEBUG_FIXED_MEM

#define MAX_FIXED_MEM_BLOCKS       32   /* only support 32 blocks */

typedef struct _fixed_mem {
    unsigned short   mem_blocks;
    unsigned short   mem_bsize;         /* size of blocks for fixed mem , little than 64K */
    // only for debug
#ifdef DEBUG_FIXED_MEM
    unsigned int     allocated_blocks;
#endif
    
    unsigned long    mem_map[MAX_FIXED_MEM_BLOCKS/sizeof(unsigned long)];
    unsigned char    mem_ptr[1];
} fixed_mem_t;


typedef void * fixed_mem_id_t;


#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]) /* + __must_be_array(arr) */)



static inline fixed_mem_id_t os_alloc_fixed_mem(unsigned int blocksize, unsigned int count)
{
    fixed_mem_id_t fmem_id;
    int align_len = blocksize + (4 - (blocksize & 3));
    
    ASSERT((count > 0) && (count <= MAX_FIXED_MEM_BLOCKS));

#ifdef FIXED_USE_DYNAMIC
    fmem_id = (fixed_mem_id_t)os_mem_alloc_zero(offsetof(fixed_mem_t, mem_ptr) + 0);
#else
    fmem_id = (fixed_mem_id_t)os_mem_alloc_zero(offsetof(fixed_mem_t, mem_ptr) + (align_len * count));
#endif
    ((fixed_mem_t *)fmem_id)->mem_blocks = count;
    ((fixed_mem_t *)fmem_id)->mem_bsize =  align_len;
    
#ifdef DEBUG_FIXED_MEM
    ((fixed_mem_t *)fmem_id)->allocated_blocks =  0;
#endif

    return fmem_id;
}

static inline void * os_get_fixed(fixed_mem_id_t fid)
{
    void * result_fixed = NULL;
#ifdef FIXED_USE_DYNAMIC
        result_fixed == os_mem_alloc( ((fixed_mem_t *)fid)->mem_bsize);
#else
    int i,j;
    INT_DISABLE();
    j = ((fixed_mem_t *)fid)->mem_blocks;
    for(i = 0; i < j; i++) {
        if(!test_bit(i, ((fixed_mem_t *)fid)->mem_map)) {
            break;
        }
    }
    
    if(i <  ((fixed_mem_t *)fid)->mem_blocks) {
        set_bit(i, ((fixed_mem_t *)fid)->mem_map);
        result_fixed =  (void *) ((int) (((fixed_mem_t *)fid)->mem_ptr) +  i * ((fixed_mem_t *)fid)->mem_bsize);
    }
    else {
        // todo 
        // if i >= mem_blocks then realloc the fixmem and alloc it.
        result_fixed = NULL;
    }
#endif
#ifdef DEBUG_FIXED_MEM
    printf("get fixed 0x%x, num %d, %d\n",result_fixed, ++(((fixed_mem_t *)fid)->allocated_blocks),((fixed_mem_t *)fid)->mem_bsize);
#endif
    INT_ENABLE();
    return result_fixed;
}

static inline void os_put_fixed (void * data, fixed_mem_id_t fid)
{
#ifdef DEBUG_FIXED_MEM
        printf("put fixed 0x%x num %d\n",(int)data,--(((fixed_mem_t *)fid)->allocated_blocks));
#endif
#ifdef FIXED_USE_DYNAMIC
       os_mem_free(data);
#else
    int i ;
    INT_DISABLE();
    i = ((int) data - (int) (((fixed_mem_t *)fid)->mem_ptr)) / ((fixed_mem_t *)fid)->mem_bsize;

    ASSERT ((i >= 0) &&  (i < MAX_FIXED_MEM_BLOCKS ) &&  test_bit(i, ((fixed_mem_t *)fid)->mem_map));
    clear_bit(i, ((fixed_mem_t *)fid)->mem_map);
    INT_ENABLE();
#endif    
}
static inline void os_free_fixed_mem (fixed_mem_id_t fid)
{
    os_mem_free(fid);
}



/* os ,task, sem,mutex, event */
/**HEADER********************************************************************
* 
* Copyright (c) 2013 Freescale Semiconductor;
* All Rights Reserved
*
*
***************************************************************************  
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: osadapter_mqx.c$
* $Version : 
* $Date    : 
*
* Comments:
*
* @brief  The file includes the implementation of MQX of OS adapter.
*****************************************************************************/
typedef void (* TASK_START)( void *);
typedef void (* OSA_INT_ISR_FPTR)(void *);
typedef void* OS_Event_handle;
typedef void* OS_MsgQ_handle;
typedef void* OS_GPIO_handle;
typedef void* OS_Mutex_handle;
typedef void* OS_Sem_handle;


#define OS_TASK_OK         (0)
#define OS_TASK_ERROR      (-1)
#define OS_EVENT_OK        (0)
#define OS_EVENT_ERROR     (-1)
#define OS_EVENT_TIMEOUT   (-2)
#define OS_MSGQ_OK         (0)
#define OS_MSGQ_ERROR      (-1)
#define OS_GPIO_OK         (0)
#define OS_GPIO_ERROR      (-1)
#define OS_MUTEX_OK        (0)
#define OS_MUTEX_ERROR     (-1)
#define OS_SEM_OK          (0)
#define OS_SEM_ERROR       (-1)
#define OS_SEM_TIMEOUT     (-2)

/* Block the reading task if msgq is empty */
#define OS_MSGQ_RECEIVE_BLOCK_ON_EMPTY       (0x04)
static inline uint32_t OS_Task_create(TASK_START pstart, void* param, uint32_t pri, uint32_t stack_size, char* task_name, void* opt)
{
    _task_id task_id;
    TASK_TEMPLATE_STRUCT task_template;

    task_template.TASK_TEMPLATE_INDEX = 0;
    task_template.TASK_ADDRESS = (TASK_FPTR)pstart;
    task_template.TASK_STACKSIZE = stack_size;
    task_template.TASK_PRIORITY = pri;
    task_template.TASK_NAME = task_name;
    if (opt != NULL)
    {
        task_template.TASK_ATTRIBUTES = *((uint32_t*)opt);
    }
    else
    {
        task_template.TASK_ATTRIBUTES = 0;
    }
    
    task_template.CREATION_PARAMETER = (uint32_t)param;
    task_template.DEFAULT_TIME_SLICE = 0;

    task_id = _task_create_blocked(0, 0, (uint32_t)&task_template);
    
    if (task_id == MQX_NULL_TASK_ID) {
        return (uint32_t)OS_TASK_ERROR;
    }
    
    _task_ready(_task_get_td(task_id));
    return (uint32_t)task_id;
}

static inline uint32_t OS_Task_delete(uint32_t task_id)
{
    uint32_t ret;
    ret = _task_destroy((_task_id)task_id);
    if (ret != MQX_OK)
        return (uint32_t)OS_TASK_ERROR;
    else
        return (uint32_t)OS_TASK_OK;
}

static inline  uint32_t OS_Task_suspend(uint32_t task_id)
{
    task_id = task_id;
    _task_block();
    return (uint32_t)OS_TASK_OK;
}

static inline uint32_t OS_Task_resume(uint32_t task_id)
{
    _task_ready(_task_get_td(task_id));
    return (uint32_t)OS_TASK_OK;
}

static inline void OS_Task_yield()
{
	_sched_yield();
	//_time_delay_ticks(2);
	//_time_delay(1);
}

static inline OS_Event_handle OS_Event_create(uint32_t flag)
{
    LWEVENT_STRUCT *event;
    event = (LWEVENT_STRUCT*)_mem_alloc_system_zero(sizeof(LWEVENT_STRUCT));
    if (event == NULL)
    {
        return NULL;
    }
    
    if (_lwevent_create(event, flag) != MQX_OK)
    {
        _mem_free((void*)event);
        return NULL;
    }
    return (OS_Event_handle)event;
}

static inline uint32_t OS_Event_destroy(OS_Event_handle handle)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_destroy(event) != MQX_OK)
    {
        _mem_free((void*)event);
        return (uint32_t)OS_EVENT_ERROR;
    }
    _mem_free((void*)event);
    return OS_EVENT_OK; 
}

static inline uint32_t OS_Event_check_bit(OS_Event_handle handle, uint32_t bitmask)
{
	LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
	if((event->VALUE & bitmask) != 0)
		return 1;
	else
		return 0;
}

static inline uint32_t OS_Event_clear(OS_Event_handle handle, uint32_t bitmask)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_clear(event, bitmask) != MQX_OK)
    {
        return (uint32_t)OS_EVENT_ERROR;
    }
    return (uint32_t)OS_EVENT_OK;
}

static inline uint32_t OS_Event_set(OS_Event_handle handle, uint32_t bitmask)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_set(event, bitmask) != MQX_OK)
    {
        return (uint32_t)OS_EVENT_ERROR;
    }
    return (uint32_t)OS_EVENT_OK;
}

static inline uint32_t OS_Event_wait(OS_Event_handle handle, uint32_t bitmask, uint32_t flag, uint32_t timeout)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    uint32_t ret;

    ret = _lwevent_wait_ticks(event, bitmask, flag, timeout * _time_get_ticks_per_sec() / 1000);
    //printf("os 0x%x\n", ret);
    if(LWEVENT_WAIT_TIMEOUT == ret)
    {
        return (uint32_t)OS_EVENT_TIMEOUT;
    }
    else if(MQX_OK == ret)
    {
        return (uint32_t)OS_EVENT_OK;
    }
    return (uint32_t)OS_EVENT_ERROR;
}

static inline uint32_t OS_Event_status(OS_Event_handle handle)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    return event->VALUE;
}

static inline OS_MsgQ_handle OS_MsgQ_create(uint32_t max_msg_number, uint32_t msg_size)
{
    void* msgq;
    uint32_t size = sizeof(LWMSGQ_STRUCT) + max_msg_number * msg_size * 4;
    
    msgq = _mem_alloc_system_zero(size);
    if (msgq == NULL)
    {
        return NULL;
    }

    if (_lwmsgq_init(msgq, max_msg_number, msg_size) != MQX_OK)
    {
        _mem_free(msgq);
        return NULL;
    }
       
    return (OS_MsgQ_handle)msgq;
}

static inline uint32_t OS_MsgQ_send(OS_MsgQ_handle msgq, void* msg, uint32_t flag)
{
    if (MQX_OK != _lwmsgq_send(msgq, (uint32_t *) msg, flag))
    {
        return (uint32_t)OS_MSGQ_ERROR;
    }
    return (uint32_t)OS_MSGQ_OK;
}

static inline uint32_t OS_MsgQ_recv(OS_MsgQ_handle msgq, void* msg, uint32_t flag, uint32_t timeout)
{
    if (MQX_OK != _lwmsgq_receive(msgq, (uint32_t *) msg, flag, timeout, NULL))
    {
        return (uint32_t)OS_MSGQ_ERROR;
    }
    return (uint32_t)OS_MSGQ_OK;
}

static inline uint32_t OS_MsgQ_Is_Empty(OS_MsgQ_handle msgq, void* msg)
{
	uint32_t ret;
	ret = LWMSGQ_IS_EMPTY(msgq);
	if(!ret)
	{
		if (MQX_OK != _lwmsgq_receive(msgq, (uint32_t *) msg, OS_MSGQ_RECEIVE_BLOCK_ON_EMPTY, 1, NULL))
		{
			return (uint32_t)OS_MSGQ_ERROR;
		}
	}
	return ret;
}

static inline uint32_t OS_MsgQ_destroy(OS_MsgQ_handle msgq)
{
    _lwmsgq_deinit(msgq);
    _mem_free(msgq);
    return (uint32_t)OS_MSGQ_OK;
}

static inline OS_Mutex_handle OS_Mutex_create()
{
	MUTEX_STRUCT_PTR mutex = NULL;
	mutex = _mem_alloc_system_zero(sizeof(MUTEX_STRUCT));
	if (mutex == NULL)
	{
	    return NULL;
	}
	if (_mutex_init(mutex, NULL) != MQX_OK)
	{
		_mem_free(mutex);
		return NULL;
	}
	return (OS_Mutex_handle)mutex;
}

static inline uint32_t OS_Mutex_lock(OS_Mutex_handle mutex)
{
	if (_mutex_lock((MUTEX_STRUCT_PTR)mutex) != MQX_OK)
	    return (uint32_t)OS_MUTEX_ERROR;
	else
		return (uint32_t)OS_MUTEX_OK;
}

static inline uint32_t OS_Mutex_unlock(OS_Mutex_handle mutex)
{
	if (_mutex_unlock((MUTEX_STRUCT_PTR)mutex) != MQX_OK)
		return (uint32_t)OS_MUTEX_ERROR;
    else
	    return (uint32_t)OS_MUTEX_OK;
}

static inline  uint32_t OS_Mutex_destroy(OS_Mutex_handle mutex)
{
    _mutex_destroy((MUTEX_STRUCT_PTR)mutex);
	_mem_free(mutex);
	return OS_MUTEX_OK;
}

static inline OS_Sem_handle OS_Sem_create(uint32_t initial_number)
{
	LWSEM_STRUCT_PTR sem = NULL;
	sem = (LWSEM_STRUCT_PTR)_mem_alloc_system_zero(sizeof(LWSEM_STRUCT));
    if (sem == NULL)
    {
        return NULL;
    }
    if (_lwsem_create(sem, initial_number) != MQX_OK)
    {
        _mem_free(sem);
        return NULL;
    }
    return (OS_Sem_handle)sem;
}

static inline uint32_t OS_Sem_wait(OS_Sem_handle sem, uint32_t timeout)
{
    uint32_t result = _lwsem_wait_ticks((LWSEM_STRUCT_PTR)sem, timeout);
    if (result == MQX_LWSEM_WAIT_TIMEOUT)
    {
        return (uint32_t)OS_SEM_TIMEOUT;
    }
    else if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

static inline uint32_t OS_Sem_post(OS_Sem_handle sem)
{
    uint32_t result = _lwsem_post((LWSEM_STRUCT_PTR)sem);
    if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

static inline uint32_t OS_Sem_destroy(OS_Sem_handle sem)
{
    uint32_t result = _lwsem_destroy((LWSEM_STRUCT_PTR)sem);
    _mem_free(sem);
    if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

void OS_Time_delay(register uint32_t milliseconds)
{
    _time_delay(milliseconds);
    return ;
}

#endif /* OS_UTILS_H */
