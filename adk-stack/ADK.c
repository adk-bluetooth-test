/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define ADK_INTERNAL

#include "fwk.h"

#include "Audio.h"
#include "dbg.h"
#include "coop.h"
#include "BT.h"
#include "HCI.h"
#include "btL2CAP.h"
#include "btRFCOMM.h"
#include "btSDP.h"
#include "btA2DP.h"

#include "sgBuf.h"

#include "ADK.h"

static adkBtConnectionRequestF bt_crF = 0;
static adkBtLinkKeyRequestF bt_krF = 0;
static adkBtLinkKeyCreatedF bt_kcF = 0;
static adkBtPinRequestF bt_prF = 0;
static adkBtDiscoveryResultF bt_drF = 0;
static adkBtSspDisplayF bt_pdF = 0;


static char btLinkKeyRequest(void* userData, const uint8_t* mac, uint8_t* buf){
/*  adkBtLinkKeyRequest bt_krF, adkBtLinkKeyCreated bt_kcF , adkBtPinRequest bt_drF */
    if(bt_krF) return bt_krF(mac, buf);

    dbgPrintf("BT Link key request from %02x:%02x:%02x:%02x:%02x:%02x -> denied due to lack of handler", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
    return 0;
}
static uint8_t btPinRequestF(void* userData, const uint8_t* mac, uint8_t* buf){	//fill buff with PIN code, return num bytes used (16 max) return 0 to decline
/*  adkBtPinRequest bt_drF */

    if(bt_prF) return bt_prF(mac, buf);

    dbgPrintf("BT PIN request from %02x:%02x:%02x:%02x:%02x:%02x -> '0000' due to lack of handler", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);

    buf[0] = buf[1] = buf[2] = buf[3] = '0';
    return 4;
}

static void btLinkKeyCreated(void* userData, const uint8_t* mac, const uint8_t* buf){
/*  adkBtLinkKeyCreated bt_kcF , adkBtPinRequest bt_drF */
    if(bt_kcF) bt_kcF(mac, buf);
	/* adkBtLinkKeyRequest bt_krF, adkBtLinkKeyCreated bt_kcF , adkBtPinRequest bt_drF */
}

static char btConnReqF(void* userData, const uint8_t* mac, uint32_t devClass, uint8_t linkType){	//return 1 to accept

    if(bt_crF) return bt_crF(mac, devClass, linkType);
	/* adkBtConnectionRequest bt_crF, adkBtLinkKeyRequest bt_krF, adkBtLinkKeyCreated bt_kcF , adkBtPinRequest bt_drF */

    dbgPrintf("BT connection request: %s connection from %02x:%02x:%02x:%02x:%02x:%02x (class %06X) -> accepted due to lack of handler\n", linkType ? "ACL" : "SCO", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0], devClass);
    return 1;
}

static void btConnStartF(void* userData, uint16_t conn, const uint8_t* mac, uint8_t linkType, uint8_t encrMode){

    dbgPrintf("BT %s connection up with handle %d to %02x:%02x:%02x:%02x:%02x:%02x encryption type %d\n",
    linkType ? "ACL" : "SCO", conn, mac[5], mac[4], mac[3], mac[2], mac[1], mac[0], encrMode);
    l2capAclLinkUp(conn);  /* btConnStartF => l2capAclLinkUp (printf   linktype,handle,and bt mac ) */
}

static void btConnEndF(void* userData, uint16_t conn, uint8_t reason) {

    dbgPrintf("BT connection with handle %d down for reason %d\n", conn, reason);
    l2capAclLinkDown(conn);   /* btConnEndF => l2capAclLinkDown  */
}

static void btAclDataRxF(void* userData, uint16_t conn, char first, uint8_t bcastType, const uint8_t* data, uint16_t sz){ /*  BtAclDataRxF */
    //printf("ACL %d\n",conn);
    //printf("2\n");
    l2capAclLinkDataRx(conn, first, data, sz);
}

static void btSspShowF(void* userData, const uint8_t* mac, uint32_t val){

    if(val == BT_SSP_DONE_VAL) val = ADK_BT_SSP_DONE_VAL;

    if(bt_pdF) bt_pdF(mac, val);   /* adkBtSspF */
}

static char btVerboseScanCbkF(void* userData, BtDiscoveryResult* dr){

    if(bt_drF) return bt_drF(dr->mac, dr->PSRM, dr->PSPM, dr->PSM, dr->co, dr->dc);

    dbgPrintf("BT: no callback for scan makes the scan useless, no?");
    return 0;
}

extern char btName[64];
void ADK_adkInit(void){

    //board init
    fwkInit();
    coopInit();
    /* test
   for(int i=0; i<10; i++){
    audioInit();
    audioDeinit();
    }*/

    audioInit();

    //bt init
    static const BtFuncs myBtFuncs = {NULL, btVerboseScanCbkF, btConnReqF, btConnStartF, btConnEndF, btPinRequestF, btLinkKeyRequest, btLinkKeyCreated, btAclDataRxF, btSspShowF};
    // todo judge btinit failed
    btInit(&myBtFuncs);              //BT UART & HCI driver
    
    btSdpRegisterL2capService();     //SDP daemon
    btRfcommRegisterL2capService();  //RFCOMM framework

    btA2dpRegister();                //A2DP profile

    uint8_t mac[BT_MAC_SIZE];

    if(btLocalMac(mac)) dbgPrintf("BT MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
    snprintf(btName, sizeof( btName), "bt-test-%02x-%02x", mac[1], mac[0]);
    printf("device :%s\n",btName);

}

void ADK_adkDeinit(void) 
{
    /* liutest add adversed to ADK_adkInit */
    //l2capServiceUnregister(L2CAP_PSM_AVDTP);
    //l2capServiceUnregister(L2CAP_PSM_RFCOMM);
    //l2capServiceUnregister(L2CAP_PSM_SDP);
    btA2dpUnregister(0);
    btRfcommUnregisterL2capService(0);
    btSdpUnregisterL2capService(0);

    btDeinit();
    audioDeinit();
}

void ADK_adkEventProcess(void){
    coopYield();
}

void ADK_getUniqueId(uint32_t* ID){

    cpuGetUniqId(ID);
}

void ADK_audioOn(int source, uint32_t samplerate){

    audioOn(source, samplerate);
}

void ADK_audioOff(int source){

    audioOff(source);
}

void ADK_audioAddBuffer(int source, const uint16_t* samples, uint32_t numSamples){

    audioAddBuffer(source, samples, numSamples);
}


void ADK_btEnable(adkBtConnectionRequestF crF, adkBtLinkKeyRequestF krF, adkBtLinkKeyCreatedF kcF, adkBtPinRequestF prF, adkBtDiscoveryResultF drF){
/* adkBtConnectionRequest bt_crF, adkBtLinkKeyRequest bt_krF, adkBtLinkKeyCreated bt_kcF , adkBtPinRequest bt_drF */
    bt_crF = crF;
    bt_krF = krF;
    bt_kcF = kcF;
    bt_prF = prF;
    bt_drF = drF;
}

void ADK_btSetSspCallback(adkBtSspDisplayF pdF){

    bt_pdF = pdF;
}

char ADK_btSetLocalName(const char* name){

    return btSetLocalName(name);
}

char ADK_btGetRemoteName(const uint8_t* mac, uint8_t PSRM, uint8_t PSM, uint16_t co, char* nameBuf){

    return btGetRemoteName(mac, PSRM, PSM, co, nameBuf);
}

void ADK_btScan(void){

    btScan();
}

char ADK_btDiscoverable(char on){

    return btDiscoverable(on);
}

char ADK_btConnectable(char on){

    return btConnectable(on);
}

char ADK_btSetDeviceClass(uint32_t cls){

    return btSetDeviceClass(cls);
}

void ADK_setVolume(uint8_t vol){

    setVolume(vol);
}

uint8_t ADK_getVolume(void){

    return getVolume();
}















void ADK_l2capServiceTx(uint16_t conn, uint16_t remChan, const uint8_t* data, uint32_t size){ //send data over L2CAP
    
    sg_buf* buf = sg_alloc();

    if(!buf) return;
    if(sg_add_back(buf, data, size, SG_FLAG_MAKE_A_COPY)) l2capServiceTx(conn, remChan, buf);
    else sg_free(buf);
}

void ADK_l2capServiceCloseConn(uint16_t conn, uint16_t chan){

    l2capServiceCloseConn(conn, chan);
}

char ADK_l2capServiceRegister(uint16_t PSM, const L2capService* svcData){

    return l2capServiceRegister(PSM, svcData);
}

char ADK_l2capServiceUnregister(uint16_t PSM){
    // default sendDiscPacket = 1
    return l2capServiceUnregister(PSM,1);
}

void ADK_btSdpServiceDescriptorAdd(const uint8_t* descriptor, uint16_t descrLen){

    btSdpServiceDescriptorAdd(descriptor, descrLen);
}

void ADK_btSdpServiceDescriptorDel(const uint8_t* descriptor){

    btSdpServiceDescriptorDel(descriptor);
}

void ADK_btRfcommRegisterPort(uint8_t dlci, BtRfcommPortOpenF oF, BtRfcommPortCloseF cF, BtRfcommPortRxF rF){

    btRfcommRegisterPort(dlci, oF, cF, rF);
}

void ADK_btRfcommPortTx(void* port, uint8_t dlci, const uint8_t* data, uint16_t size){

    btRfcommPortTx(port, dlci, data, size);
}

uint8_t ADK_btRfcommReserveDlci(uint8_t preference){

    return btRfcommReserveDlci(preference);
}

void ADK_btRfcommReleaseDlci(uint8_t dlci){

    btRfcommReleaseDlci(dlci);
}


